@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Nhân Viên
                <small>Thêm</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
           @if(count($errors)>0)
           <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/nhan-vien/add" method="post">
        {{ csrf_field() }}

        <div class="form-group">
            <label>Tên Nhân Viên</label>
            <input class="form-control" name="Ten" placeholder="Nhập tên nhân viên" />
        </div>
        <div class="form-group">
                <label>Mật Khẩu</label>
                <input class="form-control" type="password" name="matkhau" placeholder="Nhập mật khẩu" />
            </div>
        <div class="form-group">
            <label>Email</label>
            <input class="form-control" type="email" name="email" placeholder="Nhập email." />
        </div>
        <div class="form-group">
            <label>Số Điện Thoại</label>
            <input class="form-control" name="SDT" placeholder="Nhập số điện thoại" />
        </div>
        <div class="form-group">
                <label>Ngày Sinh</label>
                <input class="form-control" type="date" name="ngaysinh" placeholder="Nhập ngày sinh" />
            </div>
        <div class="form-group">
                <label>Địa Chỉ</label>
                <input class="form-control" name="diachi" placeholder="Nhập địa chỉ" />
            </div>
        <div class="form-group">
                <label>Chức Vụ</label>
                <select class="form-control" name="chucvu" id="chucvu">
                    <option >Chọn chức vụ</option>
                    <option value="qtv">Quản Trị Viên Bán Hàng</option>
                    <option value="qtvtk">Quản Trị Viên Tài Khoản</option>
                </select>
            </div>
        <button type="submit" class="btn btn-default btn-info">Thêm</button>
        <button type="reset" class="btn btn-default btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')