@extends('mainAdmin')
@section('AdMain')
<style>
.hinhtin img{
    height: 250px; width:250px ;
}
.titleTin{
        text-transform: uppercase; text-align: center;
        font-weight: bold;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Nhân Viên
                <small>Danh Sách</small>
                @if(session('ThongBao'))
        <div class="btn btn-info">
            {{session('ThongBao')}}
        </div>
        @endif
            </h1>
        </div>
        

        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>Email</th>
                    <th>Thông Tin Nhân Viên</th>
                    <th>Trạng Thái</th>
                    <th>Sửa</th>
                </tr>
            </thead> 
            <tbody>
                @foreach($NhanVien as $nhanvien)
                <tr class="odd gradeX" align="center">
                    <td>{{$nhanvien->email}}</td>
                    <td>
                        <div>Tên Nv: {{$nhanvien->TenNV}}</div>
                        <div>SĐT: {{$nhanvien->SDT}}</div>
                        <div>Địa chỉ: {{$nhanvien->DiaChi}}</div>
                    </td>
                    <td>
                            <a href="admin/nhan-vien/xuly/{{$nhanvien->id}}" class="btn 
                                @if($nhanvien->TrangThai==0)
                                        {{"btn-danger"}}
                                        @elseif($nhanvien->TrangThai==1)
                                        {{"btn-info"}}
                                        @endif">
                                @if($nhanvien->TrangThai==0)
                                {{"Active"}}
                                @elseif($nhanvien->TrangThai==1)
                                {{"Đã Active"}}
                                @endif
                                </a>
                    </td>
                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/nhan-vien/update/{{$nhanvien->id}}">Edit</a></td>
                </tr>
               @endforeach
            </tbody>
</table>
</div>
<!-- /.row -->
</div>
@endsection('AdMain')