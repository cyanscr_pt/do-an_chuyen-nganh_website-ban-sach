@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Nhà Xuất Bản
                <small>Update</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
         @if(count($errors)>0)
         <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/nha-xuat-ban/update/{{$nxb->id}}" method="post" enctype="multipart/form-data">
           {{ csrf_field() }}
           <div class="form-group">
            <label>Tên NXB</label>
            <input class="form-control" name="TenNxb" value="{{$nxb->TenNXB}}" />
        </div>
        <div class="form-group">
            <label>Tên NXB Không Dấu</label>
            <input class="form-control" name="TenNxb_KhongDau"  value="{{$nxb->TenNXB_KhongDau}}"/>
        </div>
         <div class="form-group">
            <label>Địa Chỉ</label>
            <input class="form-control" name="diachi" value="{{$nxb->DiaChi}}" />
        </div>
         <div class="form-group">
            <label>Số Điện Thoại</label>
            <input class="form-control" name="phone"  value="{{$nxb->SDT}}"/>
        </div>
        <div  class="form-group">
            <label>Hình Bìa</label>
            <input class="form-control" type="file" name="image" placeholder="Chọn hình"/> 
        </div>
        <div class="col-md-3"><img height="100px" width="100px" src="upload/logoNXB/{{$nxb->logo}}" alt=""></div><br/>

        <button type="submit" class="btn btn-default btn-info">Update</button>
        <button type="reset" class="btn btn-default btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')