<div class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li class="sidebar-search">
            <div class="input-group custom-search-form">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
            <!-- /input-group -->
        </li>
        <li>
            <a href="admin/thong-ke"><i class="fa fa-dashboard fa-fw"></i> Thống Kê</a>
        </li>
        <li 
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif
        >
            <a href="admin/don-hang/danh-sach"><i class="fa fa-dashboard fa-fw"></i> Đơn Hàng</a>
        </li>
        <!-- Sách -->
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="#"><i class="fa fa-cube fa-fw"></i>Sách<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/sach/danh-sach">Danh Sách Sách</a>
                </li>
                <li>
                    <a href="admin/sach/add">Thêm Sách</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <!-- Sách -->

        <!-- Loai -->
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="#"><i class="fa fa-cube fa-fw"></i>Loại Sách<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/the-loai/danh-sach">Danh Sách Loại</a>
                </li>
                <li>
                    <a href="admin/the-loai/add">Thêm Loại</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <!-- Loai -->

        <!-- Tác Giả -->
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="#"><i class="fa fa-cube fa-fw"></i> Tác Giả<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/tac-gia/danh-sach">Danh Sách Tác Giả</a>
                </li>
                <li>
                    <a href="admin/tac-gia/add">Thêm Tác Giả</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <!-- Tác Giả -->
        <!-- Nhà Xuất Bản -->
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="admin/nha-xuat-ban/danh-sach"><i class="fa fa-cube fa-fw"></i> Nhà Xuất Bản<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/nha-xuat-ban/danh-sach">Danh Sách NXB</a>
                </li>
                <li>
                    <a href="admin/nha-xuat-ban/add">Thêm NXB</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <!-- Nhà Xuất Bản -->
         <li
         @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="admin/nha-xuat-ban/danh-sach"><i class="fa fa-cube fa-fw"></i>Khuyến Mãi<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/khuyen-mai/danh-sach">Danh Sách Khuyến Mãi</a>
                </li>
                <li>
                    <a href="admin/khuyen-mai/add">Thêm Khuyến Mãi</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <!-- Khuyến Mãi -->
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="admin/binh-luan/danh-sach"><i class="fa fa-cube fa-fw"></i> Bình Luận<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/binh-luan/danh-sach">Danh Sách Bình Luận</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>

        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qtv")
        style="display: none"
        @endif>
            <a href="#"><i class="fa fa-users fa-fw"></i> Tin Tức<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/tin/danh-sach">List Tin Tức</a>
                </li>
                <li>
                    <a href="admin/tin/add">AddTin Tức</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qttk")
        style="display: none"
        @endif>
            <a href="#"><i class="fa fa-users fa-fw"></i> User<span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
                <li>
                    <a href="admin/user/getdsuser">User Danh Sách</a>
                </li>
            </ul>
            <!-- /.nav-second-level -->
        </li>
        <li
        @if(Auth::guard('QuanTri')->user()->ChucVu != "qttk")
        style="display: none"
        @endif>
                <a href="#"><i class="fa fa-users fa-fw"></i> Nhân Viên<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="admin/nhan-vien/danh-sach">Danh Sách Nhân Viên</a>
                    </li>
                    <li>
                        <a href="admin/nhan-vien/add">Thêm Nhân Viên</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
    </ul>
</div>
                <!-- /.sidebar-collapse -->