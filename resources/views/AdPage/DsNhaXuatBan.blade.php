@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Nhà Xuất Bản
                <small>Danh Sách</small>
                @if(session('ThongBao'))
                <div class="btn btn-info">
                    {{session('ThongBao')}}
                </div>
                @endif
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_NXB</th>
                    <th>Tên Nhà Xuất Bản</th>
                    <th>LoGo</th>
                    <th>Đia Chỉ</th>
                    <th>SĐT</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($NXB as $nxb)
                <tr class="odd gradeX" align="center">
                    <td>{{$nxb->id}}</td>
                    <td class="center">
                       {{$nxb->TenNXB}}
                    </td>
                    <td><div class="col-md-3"><img height="300px" width="300" src="upload/logoNXB/{{$nxb->logo}}" alt=""></div></td>
                    <td>{{$nxb->DiaChi}}</td>
                    <td>{{$nxb->SDT}}</td>
                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/nha-xuat-ban/delete/{{$nxb->id}}"> Delete</a></td>
                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/nha-xuat-ban/update/{{$nxb->id}}">Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.row -->
</div>
@endsection('AdMain')