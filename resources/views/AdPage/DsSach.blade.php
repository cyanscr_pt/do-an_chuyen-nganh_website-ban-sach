@extends('mainAdmin')
@section('AdMain')
<style type="text/css">
    .img-sach img{ height: 250px; width:250px ;}
    .tua{
        text-transform: uppercase; text-align: center;
        font-weight: bold;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sách
                <small>Danh sách</small>
                @if(session('ThongBao'))
                <div class="btn btn-info">
                    {{session('ThongBao')}}
                </div>
                @endif
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_Sách</th>
                    <th>Thông tin sách</th>
                    <th>Bìa</th>
                    <th>Trạng thái</th>
                    <th>Giá</th>
                    <th>Ngôn ngữ</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($Sach as $sach)
                <tr class="odd gradeX" align="center">
                    <td>{{$sach->id}}</td>
                    <td>
                        <div class="img-sach"><img src="upload/biasach/{{$sach->urlHinh}}"></div>
                        <br/>
                        <div class="tua" ><a class="btn btn-xs btn-infor">{{$sach->TenSach}}</a></div>
                    </td>
                    <td>
                        <div class="thong-tin-sach">

                            <div>Dịch giả: {{$sach->DichGia}}</div>
                            <div>{{$sach->NhaXuatBan->TenNXB}}</div>
                            <div>Trang: {{$sach->SoTrang}}</div>
                            <div>Loại bìa: {{$sach->LoaiBia}}</div>
                            <div>SKU: {{$sach->SKU}}</div>
                            <div>
                                Tác giả: @foreach($sach->tacgia as $tacgia)
                                {{$tacgia->TenTG}} <br/>
                                @endforeach
                            </div>
                            <div> Thể loại: @foreach($sach->theloai as $loai)
                                {{$loai->TenTL}}<br/>
                                @endforeach
                            </div>
                        </div>
                    </td>
                    
                    <td >
                        <a class="btn btn-xs 
                        @if($sach->TrangThai==1)
                        btn-info
                        @else
                        btn-danger
                        @endif
                        ">
                        @if($sach->TrangThai==1)
                        Hiện
                        @else
                        Ẩn
                        @endif
                    </a>
                </td>
                <td >
                    <a class="btn btn-xs btn-danger">
                        {{number_format($sach->Gia,0, ",",".")}} VND
                    </a>
                </td>
                <td>
                 @if($sach->lang=='vi')
                 Tiếng việt
                 @else
                 Tiếng anh
                 @endif
             </td>
             <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/sach/delete/{{$sach->id}}"> Delete</a></td>
             <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/sach/update/{{$sach->id}}">Edit</a></td>
         </tr>
         @endforeach
     </tbody>
 </table>
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')

