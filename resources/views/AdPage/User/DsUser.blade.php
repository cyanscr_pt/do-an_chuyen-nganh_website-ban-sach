@extends('mainAdmin')
@section('AdMain')
<style>
.hinhtin img{
    height: 250px; width:250px ;
}
.titleTin{
        text-transform: uppercase; text-align: center;
        font-weight: bold;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">User
                <small>Danh Sách</small>
                @if(session('ThongBao'))
        <div class="btn btn-info">
            {{session('ThongBao')}}
        </div>
        @endif
            </h1>
        </div>
        

        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>Email</th>
                    <th>Mật Khẩu</th>
                    <th>Thông Tin Khách Hàng</th>
                    <th>Trạng Thái</th>
                </tr>
            </thead> 
            <tbody>
               @foreach($User as $user)
                <tr class="odd gradeX" align="center">
                    <td>{{$user->email}}</td>
                    <td >{!! Str::words( $user->password, 1,'.... &raquo') !!}</td>
                    <td>
                        <div>Tên KH: {{$user->tenUser}}</div>
                        <div>SĐT: {{$user->SDT}}</div>
                        <div>Ngày Sinh: {{$user->NgaySinh}}</div>
                    </td>
                    <td>
                    <a href="admin/user/xulyUser/{{$user->id}}" class="btn 
                        @if($user->active==0)
                                {{"btn-danger"}}
                                @elseif($user->active==1)
                                {{"btn-info"}}
                                @endif">
                        @if($user->active==0)
                        {{"Active"}}
                        @elseif($user->active==1)
                        {{"Đã Active"}}
                        @endif
                        </a>
                    </td>
                </tr>
                @endforeach
            </tbody>
</table>
</div>
<!-- /.row -->
</div>
@endsection('AdMain')