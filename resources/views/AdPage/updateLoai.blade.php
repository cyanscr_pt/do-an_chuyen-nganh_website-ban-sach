@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Loại Sách
                <small>Update</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
           @if(count($errors)>0)
           <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/the-loai/update/{{$Loai->id}}" method="post">
         {{ csrf_field() }}
         <div class="form-group">
            <label>Tên Loại</label>
            <input class="form-control" name="TenLoai" value="{{$Loai->TenTL}}" /> 
        </div>
        <div class="form-group">
            <label>Tên Loại Không Dấu</label>
            <input class="form-control" name="TenLoai_KhongDau" value="{{$Loai->TenTL_KhongDau}}" /> 
        </div>
        <button type="submit" class="btn btn-default btn-info" >Update</button>
        <button type="reset" class="btn btn-default btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')