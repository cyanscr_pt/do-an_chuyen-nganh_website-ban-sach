@extends('mainAdmin')
@section('AdMain')



<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Khuyến Mãi
                <small>Danh Sách</small>
            </h1>
        </div>
        @if(session('ThongBao'))
        <div class="btn btn-info">
            {{session('ThongBao')}}
        </div>
        @endif

        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_KM</th>
                    <th>Tên Khuyến Mãi</th>
                    <th>Thời Gian KM</th>
                    <th>Trạng Thái</th>
                    <th>Chi Tiết KM</th>
                    <th>Edit</th>
                </tr>
            </thead> 
            <tbody>
                @foreach($KM as $km)
                <tr class="odd gradeX" align="center">
                    <td>{{$km->id}}</td>
                    <td>{{$km->Ten_KM}}</td>
                    <td>{{$km->NgayBatDau}} - {{$km->NgayKetThuc}}</td>
                    <td>
                        @if($km->TrangThai ==1)
                        {{"Hiện"}}
                        @else
                        {{Ẩn}}
                        @endif
                    </td>
                    <td class="center">
                        <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Chi Tiết</button>

                        <!-- Modal -->
                        <div class="modal fade" id="myModal" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title">Chi Tiết Khuyến Mãi<small> Mã KM:{{$km->id}} </small></h4>
                              </div>
                              <div class="modal-body">
                                  <table class="table table-striped table-bordered table-hover">
                                  
                                    <thead>
                                      <tr align="center">
                                        <th>ID Khuyến Mãi</th>
                                        <th>Thông Tin Sách</th>
                                        <th>Số Lượng KM</th>
                                        <th>Số Lượng KM Còn Lại</th>
                                        <th>Phần Trăm KM</th>
                                        <th>Điều Kiện</th>
                                    </tr>
                                </thead>
                                <tbody>
                        
                               @foreach($km->Sach as $value)
                              
                                <tr class="odd gradeX" align="center">
                                    <td>{{$km->id}}</td>
                                    <td>{{$value->TenSach}}</td>
                                    <td>{{$km->pivot_SoLuongKM}}</td>
                                    <td>{{$km->pivot_SoLuongConLai}}</td>
                                    <td>{{$value->pivot->PhanTramKM}}%</td>
                                    <td>{!!$value->pivot->DieuKien!!}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  </div>
              </div>
          </td>
          <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/khuyen-mai/update/{{$km->id}}">Edit</a></td>
      </tr>
      @endforeach
  </tbody>
</table>
</div>
<!-- /.row -->
</div>
@endsection('AdMain')