@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Khuyến Mãi
                <small>Thêm</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
           @if(count($errors)>0)
           <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/khuyen-mai/add" method="post">
         {{ csrf_field() }}
         <div class="row">
            <div class="form-group col-md-9">
                <label>Tên Khuyến Mãi</label>
                <input class="form-control" name="tenKM" placeholder="Nhập tên chương trình KM" />
            </div>
            <div class="col-md-3">
            <div  class="form-group">
                <label>Phần Trăm Khuyến mãi</label>
                <input class="form-control"  type="number" name="phantramKm" min="1" max="100" placeholder="Nhập phần trăm khuyen mai" />
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-3">
            <label>Trạng Thái</label>
            <select class="form-control" name= "trangthai">
                <option value="1">Hiện</option>
                <option value="0">Ẩn</option>
            </select>
        </div>
        <!-- col-md-3 -->
        <div class="col-md-3">
            <div  class="form-group">
                <label>Số Lượng Khuyến Mãi</label>
                <input class="form-control"  type="number" name="soluongKM" placeholder="Nhập số lượng khuyến mãi" />
            </div>
        </div>
        <!-- col-md-3 -->
        <div class="col-md-3">
            <div  class="form-group">
                <label>Ngày Bắt Đầu</label>
                <input class="form-control"  type="date" name="ngaybatdauKM" placeholder="Chọn ngày bắt đầu" />
            </div>
        </div>
        <!-- col-md-3 -->
        <div class="col-md-3">
            <div  class="form-group">
                <label>Ngày Kết Thúc KM</label>
                <input class="form-control"  type="date" name="ngayketthucKM" placeholder="Chọn ngày Kết Thúc" />
            </div>
        </div>
        <!-- col-md-3 -->
    </div> 
    <!-- div row -->
    <div  class="form-group">
        <label>Điều Kiện - Ghi Chú Khuyến Mãi</label>
        <textarea id="ghichu" name="ghichu" class="ckeditor form-control"></textarea>
    </div>
    <div class="form-group row">
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th class="col-md-3">Chọn</th>
                    <th class="col-md-9">Sách</th>
                </tr>
            </thead>
            <tbody>
                @foreach($Sach as $value)
                <tr class="odd gradeX" align="center">
                    <td class="col-md-3">
                        <input type="checkbox" name="sach[]" value="{{$value->id}}">
                    </td>
                    <td class="col-md-9">
                        <div>{{$value->id}}-{{$value->TenSach}}</div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div  hidden="hidden" class="form-group">
        <label >Nhân Viên</label>
        <input class="form-control" name="id_NV" value="{{ Auth::guard('QuanTri')->user()->id}}" />
    </div>
    <br/>
    <button type="submit" class="btn btn-default btn-info">Thêm</button>
    <button type="reset" class="btn btn-default btn-danger">Reset</button>
    <form>
    </div>
</div>
<!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')