@extends('mainAdmin')
@section('AdMain')
<style>
.hinhtin img{
    height: 250px; width:250px ;
}
.titleTin{
        text-transform: uppercase; text-align: center;
        font-weight: bold;
    }
</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tin Tức
                <small>Danh Sách</small>
                @if(session('ThongBao'))
        <div class="btn btn-info">
            {{session('ThongBao')}}
        </div>
        @endif
            </h1>
        </div>
        

        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_Tin</th>
                    <th>Tên Tin</th>
                    <th>Nội dung</th>
                    <th>Trạng Thái</th>
                    <th>Nhân Viên Đăng Tin</th>
                    <th>Edit</th>
                </tr>
            </thead> 
            <tbody>
               @foreach($Tin as $tin)
                <tr class="odd gradeX" align="center">
                    <td>{{$tin->id}}</td>
                    <td>
                        <div class="titleTin">
                            {{$tin->TenTin}}
                        </div>
                        <div class="hinhtin">
                            <img  src="upload/image_tin/{{$tin->urlHinh}}">
                        </div>
                    </td>
                    <td>{!! Str::words( $tin->NoiDungTin, 100,'.... &raquo') !!} </td>
                    <td>
                        <a href="admin/tin/xuly/{{$tin->id}}" class="btn 
                            @if($tin->TrangThai==1)
                            {{"btn-info"}}
                            @elseif($tin->TrangThai==0)
                            {{"btn-danger"}}
                            @endif
                            ">
                        @if($tin->TrangThai==1)
                        {{"Hiện"}}
                        @else
                        {{"Ẩn"}}
                        @endif
                        </a>
                    </td>
                    <td>{{$tin->id_NV}}</td>
                <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/tin/update/{{$tin->id}}">Edit</a></td>
                </tr>
     @endforeach
            </tbody>
</table>
</div>
<!-- /.row -->
</div>
@endsection('AdMain')