@extends('mainAdmin')
@section('AdMain')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Khuyến Mãi
                <small>Update</small>
            </h1>
            @foreach($khuyenMai->Sach as $value)
            <?php 
            $phantram=  $value->pivot->PhanTramKM;
            $soluong =$value->pivot->SoLuongKM;
            $dieukien=$value->pivot->DieuKien;
            ?>
            @endforeach

        </div>

        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
            @if(count($errors)>0)
            <div class="btn btn-info">
                @foreach($errors->all() as $err)
                {{$err}}
                @endforeach
            </div>
            @endif
            <form action="admin/khuyen-mai/update/{{$khuyenMai->id}}" method="post">
                {{ csrf_field() }}
                <div class="row">
                    <div class="form-group col-md-9">
                        <label>Tên Khuyến Mãi</label>
                        <input class="form-control" name="tenKM" value="{{$khuyenMai->Ten_KM}}" />
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Phần Trăm Khuyến mãi</label>
                            <input class="form-control" type="number" name="phantramKm" min="1" max="100"
                                value="{{$phantram}}" />
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label>Trạng Thái</label>
                        <select class="form-control" name="trangthai">
                            <option value="1" @if($khuyenMai->TrangThai==1) {{"checked"}} @endif >Hiện</option>
                            <option value="0" @if($khuyenMai->TrangThai==0) {{"checked"}} @endif >Ẩn</option>
                        </select>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Số Lượng Khuyến Mãi</label>
                            <input class="form-control " type="number" name="soluongKM" value="{{$soluong}}" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Ngày Bắt Đầu</label>
                            <input type="date" class="form-control datepicker" name="ngaybatdauKM"
                                value="{{$khuyenMai->NgayBatDau}}" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Ngày Kết Thúc KM</label>
                            <input type="date" class="form-control datepicker" name="ngayketthucKM"
                                value="{{$khuyenMai->NgayBatDau}}" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                </div>
                <!-- div row -->
                <div class="form-group">
                    <label>Điều Kiện - Ghi Chú Khuyến Mãi</label>
                    <textarea id="ghichu" name="ghichu" class="ckeditor form-control">{!!$dieukien!!}</textarea>
                </div>
                <div class="form-group row">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr align="center">
                                <th class="col-md-3">Chọn</th>
                                <th class="col-md-9">Sách</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($Sach as $value)
                            <tr class="odd gradeX" align="center">
                                <td class="col-md-3">
                                    
                                    <input type="checkbox"  name="sach[]" 
                                    @foreach($khuyenMai->Sach as $val)
                                    @if($val->id ==$value->id)
                                    {{"checked"}}
                                    @endif
                                    @endforeach
                                    value="{{$value->id}}">
                                    
                                </td>
                                <td class="col-md-9">
                                    <div>{{$value->id}}-{{$value->TenSach}}</div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div hidden="hidden" class="form-group">
                    <label>Nhân Viên</label>
                    <input class="form-control" name="id_NV" value="{{ Auth::guard('QuanTri')->user()->id}}" />
                </div>
                <br />
                <button type="submit" class="btn btn-default btn-info">Lưu</button>
                <button type="reset" class="btn btn-default btn-danger">Reset</button>
                <form>
        </div>
    </div>
    <!-- /.row -->
</div>

<!-- /.container-fluid -->
@endsection('AdMain')
@section('script')
<script>
    $(document).ready(function (e) {
        $('.datepicker').bootstrapMaterialDatePicker({
            format: 'Y/M/D',
            weekStart: 1,
            time: false
        });
    });

</script>
@endsection
