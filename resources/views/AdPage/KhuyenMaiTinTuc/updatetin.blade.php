@extends('mainAdmin')
@section('AdMain')
<style type="text/css">
    #spands{
        font-weight: bold;
    }
</style>
<!-- upload Hình -->

<!-- upload Hình -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tin
                <small>Update</small>
                @if(count($errors)>0)
         <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
         
        <form action="admin/tin/update/{{$tin->id}}"  method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Tiêu Đề Tin Tức</label>
                <input class="form-control" name="title" placeholder="Nhập tiêu đề tin" value="{{$tin->TenTin}}"/>
            </div>
            <div class="form-group">
                <label>Tiêu Đề không dấu</label>
                <input class="form-control" name="Title_KhongDau" placeholder="Nhập tiêu đề không dấu" value="{{$tin->TenTin_KhongDau}}"/>
            </div>
            <div class="form-group">
                <label>Khuyến Mãi Được Liên Kết</label>
                <select class="form-control" name="id_KM">
                    <option >Chọn khuyến mãi liên kết</option>
                    @foreach($KhuyenMai as $km)
                    <option value="{{$km->id}}" 
                        @if($tin->id_KM == $km->id)
                            {{"selected" }}
                        @endif
                        >{{$km->Ten_KM}} </option>
                    @endforeach
                </select>
            </div>
            <div  class="form-group">
                    <label>Hình</label>
                    <input class="form-control" type="file" name="image" placeholder="Chọn hình" />
                </div>
        <br/>
        <div  class="form-group">
            <label>Nội Dung Tin</label>
        <textarea id="gioithieusach" name="NoiDung" class="ckeditor form-control">{{$tin->NoiDungTin}}</textarea>
        </div>
        <br/>
        <button type="submit" class="btn btn-info">Lưu</button>
        <button type="reset" class="btn btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')