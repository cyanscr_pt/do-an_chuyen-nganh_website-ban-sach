@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Bình Luận
                <small>Danh Sách</small>
            </h1>
        </div>
        @if(session('ThongBao'))
        <div class="btn btn-info">
            {{session('ThongBao')}}
        </div>
        @endif
        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_BL</th>
                    <th>Người bình luận</th>
                    <th>Sách bình luận</th>
                    <th>Nhân viên duyệt</th>
                    <th>Nội dung</th>
                    <th>Ngày</th>
                    <th>Trạng thái</th>
                </tr>
            </thead> 
            <tbody>
                @foreach($BL as $bl)
                <tr class="odd gradeX" align="center">
                    <td>{{$bl->id}}</td>
                    <td>{{$bl->user->tenUser}}</td>
                    <td>{{$bl->Sach->TenSach}}</td>
                    <td>{{$bl->NhanVien->TenNV}}</td>
                    <td>{{$bl->NoiDung}}</td>
                    <td>{{$bl->Ngay}}</td>
                    <td class="btn btn 
                        @if($bl->TrangThai)==1
                        {{'btn-info'}}
                        @else
                        {{'btn-danger'}}
                        @endif
                    ">
                       <a href="admin/binh-luan/xu-ly/{{$bl->id}}"> @if($bl->TrangThai==1)
                            {{'Hiển Thị'}}
                        @else
                            {{'Ẩn'}}
                        @endif
                        </a>
                    </td>
                    
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.row -->
</div>
@endsection('AdMain')