@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Nhà Xuất Bản
                <small>Thêm</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
         @if(count($errors)>0)
         <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/nha-xuat-ban/add" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
           <div class="form-group">
            <label>Tên NXB</label>
            <input class="form-control" name="TenNxb" placeholder="Nhập tên NXB" />
        </div>
        <div class="form-group">
            <label>Tên NXB Không Dấu</label>
            <input class="form-control" name="TenNxb_KhongDau" placeholder="Nhập tên NXB không dấu" />
        </div>
         <div class="form-group">
            <label>Địa Chỉ</label>
            <input class="form-control" name="diachi" placeholder="Địa chỉ NXb" />
        </div>
         <div class="form-group">
            <label>Số Điện Thoại</label>
            <input class="form-control" name="phone" placeholder="Số điện thoại nhà xuất bản" />
        </div>
        <div  class="form-group">
            <label>logo</label>
            <input class="form-control" type="file" name="image" placeholder="Chọn hình" />
        </div>
        <button type="submit" class="btn btn-default btn-info">Thêm</button>
        <button type="reset" class="btn btn-default btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')