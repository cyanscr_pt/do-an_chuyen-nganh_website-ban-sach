@extends('mainAdmin')
@section('AdMain')
 <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tác Giả
                <small>Danh Sách</small>
                 @if(session('ThongBao'))
                <div class="btn btn-info">
                    {{session('ThongBao')}}
                </div>
                @endif
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_TacGia</th>
                    <th>Tên tác giả</th>
                    <th>TenTG_KhongDau</th>
                    <th>Giới thiệu</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                @foreach($TacGia as $tg)
                <tr class="odd gradeX" align="center">
                    <td>{{$tg->id}}</td>
                    <td>{{$tg->TenTG}}</td>
                    <td>{{$tg->TenTG_KhongDau}}</td>
                    <td>{{$tg->MoTa}}</td>
                    <td class="center"><i class="fa fa-trash-o  fa-fw btn btn btn-dark"></i><a href="admin/tac-gia/delete/{{$tg->id}}"> Delete</a></td>
                    <td class="center btn btn-dark"><i class="fa fa-pencil fa-fw "></i> <a href="admin/tac-gia/update/{{$tg->id}}">Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.row -->
</div>
@endsection('AdMain')