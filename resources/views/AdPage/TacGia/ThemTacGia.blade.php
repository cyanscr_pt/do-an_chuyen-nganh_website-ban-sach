@extends('mainAdmin')
@section('AdMain')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Tác giả
                <small>Thêm</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
           @if(count($errors)>0)
           <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/tac-gia/add" method="post">
        {{ csrf_field() }}

        <div class="form-group">
            <label>Tên Tác Giả</label>
            <input class="form-control" name="TenTG" placeholder="Nhập tên tác giả" />
        </div>
        <div class="form-group">
            <label>Tên Tác Giả Không Dấu</label>
            <input class="form-control" name="TenTG_KhongDau" placeholder="Nhập tên tác giả không dấu" />
        </div>
        <div class="form-group">
            <label>Giới Thiệu</label>
            <textarea id="gioithieu" name="gioithieu" class="ckeditor form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-default btn-info">Thêm</button>
        <button type="reset" class="btn btn-default btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')