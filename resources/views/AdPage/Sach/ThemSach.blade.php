@extends('mainAdmin')
@section('AdMain')
<style type="text/css">
    #spands {
        font-weight: bold;
    }

</style>
<!-- upload Hình -->
<style>
    #tacgianew {
        display: none;
    }

    #theloainew {
        display: none;

    }
    #nxbnew{
        display: none;
    }

</style>
<!-- upload Hình -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sách
                <small>Thêm</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
            @if(count($errors)>0)
            <div class="btn btn-info">
                @foreach($errors->all() as $err)
                {{$err}}
                @endforeach
            </div>
            @endif
            <form action="admin/sach/add" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label>Tựa sách</label>
                    <input class="form-control" name="TenSach" placeholder="Nhập tên sách" />
                </div>
                <div class="form-group">
                    <label>Tên sách không dấu</label>
                    <input class="form-control" name="TenSach_KhongDau" placeholder="Nhập tên sách không dấu" />
                </div>
                <div class="form-group">
                    <label>Dịch giả</label>
                    <input class="form-control" name="dichgia" placeholder="Tên dịch giả" />
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <a class="btn btn-info" id="btn-tacGiaNew">Tác Giả Mới</a>
                        <div id="tacgianew">
                            <label>Tên Tác giả: </label><br>
                            <input type="text" class="form-group" name="tentacgianew" id="tentacgianew"
                                placeholder="Tên tác giả"><br>
                            <a class="btn btn-danger" id="HuyTg">Hủy</a>
                            <a class="btn btn-info" id="themtg">Thêm</a>
                        </div>
                    </div>
                    {{-- col-md-4 --}}
                    <div class="col-md-4">
                        <a class="btn btn-info" id="btn-TLNew">Thể Loại Mới</a>
                        <div id="theloainew">
                            <label>Tên Thể Loại: </label><br>
                            <input type="text" class="form-group" name="tentheloainew" id="tentheloainew"><br>
                            <a class="btn btn-danger" id="HuyTl">Hủy</a>
                            <a class="btn btn-info" id="themtl">Thêm</a>
                        </div>

                    </div>
                    {{-- col-md-4 --}}
                    <div class="col-md-4">
                        <a class="btn btn-info" id="btn-NXBNew">Nhà Xuất Bản Mới</a>
                        <div id="nxbnew">
                            <label>Tên Thể Loại: </label><br>
                            <input type="text" class="form-group" name="tennxbnew" id="tennxbnew"><br>
                            <a class="btn btn-danger" id="Huynxb">Hủy</a>
                            <a class="btn btn-info" id="themnxb">Thêm</a>
                        </div>

                    </div>
                    {{-- col-md-4 --}}

                </div>
                <br/>
                <div class="row">
                    <div class="col-md-6">
                        <div>
                            <label>Tác giả: </label><br>
                            @foreach($TacGia as $tg)
                            <input type="checkbox" class="form-group" name="tacgia[]" value="{{$tg->id}}"><span
                                id="spands"> {{$tg->TenTG}}</span><br>
                            @endforeach

                        </div>
                    </div>
                    {{-- col-md-3 --}}

                    <div class="col-md-6">
                        <div>
                            <label>Thể Loại: </label><br>
                            @foreach($TL as $tl)
                            <input type="checkbox" class="form-group" name="theloai[]" value="{{$tl->id}}"><span
                                id="spands"> {{$tl->TenTL}}</span><br>
                            @endforeach
                        </div>
                    </div>
                </div>
                <br />
                
                <br/>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Nhà Xuất Bản</label>
                            <select class="form-control" name="NXB">
                                <option>Chọn nhà xuất bản</option>
                                @foreach($NXB as $nxb)
                                <option value="{{$nxb->id}}">{{$nxb->TenNXB}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Ngôn ngữ</label>
                            <select class="form-control" name="lang">
                                <option>Chọn ngôn ngữ</option>
                                <option value="vi">Tiếng Việt</option>
                                <option value="en">Tiếng Anh</option>
                            </select>
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Loai bìa</label>
                            <select class="form-control" name="bia">
                                <option>Chọn loại bìa</option>
                                <option value="cung">Cứng</option>
                                <option value="mem">Mềm</option>
                            </select>
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <label>Trạng Thái</label>
                        <select class="form-control" name="trangthai">
                            <option value="1">Hiện</option>
                            <option value="0">Ẩn</option>
                        </select>
                    </div>
                    <!-- col-md-3 -->
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Kích Thước</label>
                            <input class="form-control" name="kichthuoc" placeholder="Nhập kích thước" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Số Lượng</label>
                            <input class="form-control" type="number" name="soluong" placeholder="Nhập số lượng sách" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Mã SKU</label>
                            <input class="form-control" name="sku" placeholder="Nhập mã sku" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Giá Bán</label>
                            <input class="form-control" type="number" name="gia" placeholder="Nhập giá bán" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Số trang</label>
                            <input class="form-control" type="number" name="sotrang" placeholder="Nhập số trang" />
                        </div>
                    </div>
                    <!-- col-md-3 -->
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Hình Bìa</label>
                            <input class="form-control" type="file" name="image" placeholder="Chọn hình" />
                        </div>
                    </div>
                    <!-- col-md-3 -->

                </div>
                <div class="form-group">
                    <label>giới thiệu</label>
                    <textarea id="gioithieusach" name="gioithieusach" class="ckeditor form-control"></textarea>
                </div>
                <br />
                <button type="submit" class="btn btn-info">Thêm</button>
                <button type="reset" class="btn btn-danger">Reset</button>
                <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<script language="javascript">
    document.getElementById("HuyTg").onclick = function () {
        document.getElementById("tacgianew").style.display = 'none';
    };

    document.getElementById("btn-tacGiaNew").onclick = function () {
        document.getElementById("tacgianew").style.display = 'block';
    };
    //Tác giả
    document.getElementById("HuyTl").onclick = function () {
        document.getElementById("theloainew").style.display = 'none';
    };

    document.getElementById("btn-TLNew").onclick = function () {
        document.getElementById("theloainew").style.display = 'block';
    };
    //Thể loại
    document.getElementById("Huynxb").onclick = function () {
        document.getElementById("nxbnew").style.display = 'none';
    };

    document.getElementById("btn-NXBNew").onclick = function () {
        document.getElementById("nxbnew").style.display = 'block';
    };
</script>
<!-- /.container-fluid -->
@endsection('AdMain')
