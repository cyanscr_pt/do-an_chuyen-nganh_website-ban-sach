@extends('mainAdmin')
@section('AdMain')
<style type="text/css">
    #spands{
        font-weight: bold;
    }
</style>
<!-- upload Hình -->

<!-- upload Hình -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Sách
                <small>Update</small>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
        <div class="col-lg-7" style="padding-bottom:120px">
         @if(count($errors)>0)
         <div class="btn btn-info">
            @foreach($errors->all() as $err)
            {{$err}}
            @endforeach
        </div>
        @endif
        <form action="admin/sach/update/{{$Sach->id}}"  method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Tựa sách</label>
                <input class="form-control" name="TenSach" placeholder="Nhập tên sách" value="{{$Sach->TenSach}}" />
            </div>
            <div class="form-group">
                <label>Tên sách không dấu</label>
                <input class="form-control" name="TenSach_KhongDau" placeholder="Nhập tên sách không dấu" value="{{$Sach->TenSach_KhongDau}}"  />
            </div>
            <div class="form-group">
                <label>Dịch giả</label>
                <input class="form-control" name="dichgia" placeholder="Tên dịch giả" value="{{$Sach->DichGia}}"  />
            </div>
            <div class="row">
               <div class="col-md-6">
                <div >
                    <label >Tác giả: </label><br>
                    @foreach($TacGia as $tg)
                    <input type="checkbox" class="form-group" name="tacgia[]"  value="{{$tg->id}}"
                        @foreach($Sach->tacgia as $value)
                            @if($value->id == $tg->id )
                                {{"checked='checked'"}}
                            @endif
                        @endforeach
                    ><span id="spands"> {{$tg->TenTG}}</span><br>
                    @endforeach

                </div>
            </div> 
            <div class="col-md-6">
                <div >
                    <label >Thể Loại: </label><br>
                    @foreach($TL as $tl)
                    <input type="checkbox" class="form-group"   name="theloai[]" value="{{$tl->id}}" 
                        @foreach($Sach->theloai as $value)
                                @if($value->id == $tl->id )
                                    {{"checked='checked'"}}
                                @endif
                        @endforeach
                    ><span id="spands"> {{$tl->TenTL}}</span><br>
                    @endforeach

                </div>
            </div> 
        </div>
        <br/>
        <div class="row">
             <div class="col-md-3">
                <div  class="form-group">
                    <label>Nhà Xuất Bản</label>
                    <select class="form-control" name= "NXB">
                        <option>Chọn nhà xuất bản</option>
                        @foreach($NXB as $nxb)
                        <option value="{{$nxb->id}}" @if($Sach->id_NXB == $nxb->id) {{'selected'}} @endif > {{$nxb->TenNXB}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div class="form-group">
                    <label>Ngôn ngữ</label>
                    <select class="form-control" name= "lang">
                        <option>Chọn ngôn ngữ</option>
                        
                        <option value="vi" @if($Sach->lang == 'vi') {{'selected'}} @endif >Tiếng Việt</option>
                        
                        
                        <option value="en"  @if($Sach->lang=='en') {{'selected'}} @endif >Tiếng Anh</option>
                        
                    </select>
                </div>
            </div> 
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Loai bìa</label>
                    <select class="form-control" name= "bia">
                        <option>Chọn loại bìa</option>
                        
                        <option value="cung"  @if($Sach->LoaiBia=='cung') {{'selected'}} @endif >Cứng</option>
                        
                        
                        <option value="mem" @if($Sach->LoaiBia=='mem') {{'selected'}} @endif>Mềm</option>
                        
                    </select>
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <label>Trạng Thái</label>
                <select class="form-control" name= "trangthai">
                    <option value="1" @if($Sach->TrangThai=='1') {{'selected'}} @endif>Hiện</option>
                    <option value="0" @if($Sach->TrangThai=='0') {{'selected'}} @endif>Ẩn</option>
                </select>
            </div>
            <!-- col-md-3 -->
            </div>
            <div class="row">
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Kích Thước</label>
                    <input class="form-control"  name="kichthuoc" placeholder="Nhập kích thước" value="{{$Sach->KichThuoc}}"/>
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Số Lượng</label>
                    <input class="form-control"  type="number" name="soluong" placeholder="Nhập số lượng sách" value="{{$Sach->SoLuong}}" />
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Mã SKU</label>
                    <input class="form-control" name="sku" placeholder="Nhập mã sku" value="{{$Sach->SKU}}" />
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Giá Bán</label>
                    <input class="form-control" type="number" name="gia" placeholder="Nhập giá bán" value="{{$Sach->Gia}}"/>
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Số trang</label>
                    <input class="form-control" type="number" name="sotrang" placeholder="Nhập số trang" value="{{$Sach->SoTrang}}"/>
                </div>
            </div>
            <!-- col-md-3 -->
            <div class="col-md-3">
                <div  class="form-group">
                    <label>Hình Bìa</label>
                    <input class="form-control" type="file" name="image" placeholder="Chọn hình"/> 
                </div>
            </div>
            <div class="col-md-3"><img height="100px" width="100px" src="upload/biasach/{{$Sach->urlHinh}}" alt=""></div>
            <!-- col-md-3 -->

        </div>
        <div  class="form-group">
            <label>giới thiệu</label>
            <textarea id="gioithieusach" name="gioithieusach" class="ckeditor form-control">{{$Sach->GioiThieu}}</textarea>
        </div>
        <br/>
        <button type="submit" class="btn btn-info">Lưu</button>
        <button type="reset" class="btn btn-danger">Reset</button>
        <form>
        </div>
    </div>
    <!-- /.row -->
</div>
<!-- /.container-fluid -->
@endsection('AdMain')