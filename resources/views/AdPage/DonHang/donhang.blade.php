@extends('mainAdmin')
@section('AdMain')
<style type="text/css">
    .tt {
        text-decoration: none;
        text-transform: uppercase;
        font-size: large;
    }

    .tt a {
        color: white;
    }

</style>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Đơn Hàng
                <small>Danh Sách</small>
                @if(session('ThongBao'))
                <div class="btn btn-info">
                    {{session('ThongBao')}}
                </div>
                @endif
            </h1>
        </div>
        <?php $idnv =Auth::guard('QuanTri')->user()->id;?>
        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>Đơn Hàng</th>
                    <th>Thông Tin Người Nhận</th>
                    <th>Ngày Lập</th>
                    <th>Tổng Tiền</th>
                    <th>Ghi chú</th>
                    <th>Chi Tiết Đơn Hàng</th>
                    <th>Trạng thái</th>
                </tr>
            </thead>
            <tbody>

                @foreach($DH as $dh)

                <tr class="odd gradeX" align="center">
                    <td>
                        <div>ID: {{$dh->id}}</div>
                        <div>Người đặt: {{$dh->user->tenUser}}</div>
                    </td>
                    <td>
                        <div>Tên người nhận: {{$dh->TenNguoiNhan}}</div><br />
                        <div>SĐT: {{$dh->SDT}}</div><br />
                        <div>Địa chỉ: {{$dh->DiaChiNhanHang}}</div>
                    </td>
                    <td>
                        <div>{{$dh->NgayLap}} </div>
                        <div>
                             {{-- @if($dh->TrangThai!=0)
                            {{$dh->nhanvien->id}}-{{$dh->nhanvien->TenNV}}
                            @else
                            {{""}}
                            @endif --}}
                        </div>
                    </td>
                    <td>{{number_format($dh->TongTien,0, ",",".")}} vnđ</td>
                    <td>{!!$dh->GhiChu!!}
                    </td>
                    <td>
                        @foreach($dh->sach as $value)
                        <div>{{$value->TenSach}} - SL-{{$value->pivot->SoLuong}} -
                            {{number_format($value->pivot->Gia,0, ",",".")}} vnđ</div><br />
                        @endforeach
                    </td>
                    <td>
                        <div class="btn btn-info tt">
                            <a href="admin/don-hang/xu-ly/{{$dh->id}}/{{$idnv}}" @if($dh->TrangThai==4 ||
                                $dh->TrangThai==5 )
                                {{"hidden='hidden'"}}
                                @endif
                                >
                                @if($dh->TrangThai==0)
                                Chờ Xử Lý
                                @elseif($dh->TrangThai==1)
                                Đã Tiếp Nhận
                                @elseif($dh->TrangThai==2)
                                Đang Giao Hàng
                                @elseif($dh->TrangThai==3)
                                Hoàn Thành Đơn Hàng
                                @endif
                            </a>
                        </div>
                        <div class="btn btn-danger tt">
                            <a href="admin/don-hang/xu-ly-huy/{{$dh->id}}" @if($dh->TrangThai==3)
                                {{"hidden='hidden'"}}
                                @endif
                                >
                                @if($dh->TrangThai==4 ||$dh->TrangThai==5)
                                Đã Hủy
                                @else
                                Hủy
                                @endif
                            </a></div>
                        <br />
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.row -->
</div>
@endsection('AdMain')
