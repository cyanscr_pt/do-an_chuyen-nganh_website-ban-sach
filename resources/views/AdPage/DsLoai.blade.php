@extends('mainAdmin')
@section('AdMain')
 <div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Loại Sách
                <small>Danh Sách</small>
            </h1>
        </div>
        @if(session('ThongBao'))
            <div class="btn btn-info">
                {{session('ThongBao')}}
            </div>
        @endif
        <!-- /.col-lg-12 -->
        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
            <thead>
                <tr align="center">
                    <th>ID_Loai</th>
                    <th>Tên Loại</th>
                    <th>TenLoai-KhongDau</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead> 
            <tbody>
                @foreach($Loai as $loai)
                <tr class="odd gradeX" align="center">
                    <td>{{$loai->id}}</td>
                    <td>{{$loai->TenTL}}</td>
                    <td>{{$loai->TenTL_KhongDau}}</td>
                    <td class="center"><i class="fa fa-trash-o  fa-fw"></i><a href="admin/the-loai/delete/{{$loai->id}}"> Delete</a></td>
                    <td class="center"><i class="fa fa-pencil fa-fw"></i> <a href="admin/the-loai/update/{{$loai->id}}">Edit</a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.row -->
</div>
@endsection('AdMain')