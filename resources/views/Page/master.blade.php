
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>BookStore</title>
    <base href="{{asset('')}}">

    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="apple-touch-icon" href="frontend/images/icons/favicon.ico">
    <!-- Place favicon.ico in the root directory -->

    <!-- All css files are included here. -->
    <!-- Bootstrap fremwork main css -->
    <link rel="stylesheet" href="frontend/css/bootstrap.min.css">
    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> --}}
    <!-- This core.css file contents all plugings css file. -->
    <link rel="stylesheet" href="frontend/css/core.css">
    <!-- Theme shortcodes/elements style -->
    <link rel="stylesheet" href="frontend/css/shortcode/shortcodes.css">
    <!-- Theme main style -->
    <link rel="stylesheet" href="frontend/style.css">
    <!-- Responsive css -->
    <link rel="stylesheet" href="frontend/css/responsive.css">
    <!-- User style -->
    <link rel="stylesheet" href="frontend/css/custom.css">  <link rel="stylesheet" href="frontend/css/color/skin-default.css">

    
    <!-- Modernizr JS -->
    <script src="frontend/js/vendor/modernizr-2.8.3.min.js"></script>

</head>

<body>
    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->  

    <!-- Body main wrapper start -->
    <div class="wrapper home-one">
       
        <!-- header.blade.php header -->
        @include('Page.Subpage.header')

        <!-- trang-chu.blade.php-->
        @yield('content-container')

        <!-- footer.blade.php -->
        @include('Page.Subpage.footer')
    </div> 
    <!-- Body main wrapper end -->

                    <!-- Placed js at the end of the document so the pages load faster -->

    <!-- jquery latest version -->
     <script src="frontend/js/vendor/jquery-1.12.0.min.js"></script>
    <!-- Bootstrap framework js -->
    <script src="frontend/js/bootstrap.min.js"></script>
    {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> --}}
   
    <!-- Slider js -->
    <script src="frontend/js/slider/jquery.nivo.slider.pack.js"></script>
    <script src="frontend/js/slider/nivo-active.js"></script>
    <!-- counterUp-->
    <script src="frontend/js/jquery.countdown.min.js"></script>
    <!-- All js plugins included in this file. -->
    <script src="frontend/js/plugins.js"></script>
    <!-- Main js file that contents all jQuery plugins activation. -->
    <script src="frontend/js/main.js"></script>

    @yield('script')

</body>

</html>