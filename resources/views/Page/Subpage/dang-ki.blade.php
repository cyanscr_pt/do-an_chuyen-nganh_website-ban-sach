@extends('Page.master')
@section('content-container')

    <!-- Account Area Start -->
    <div class="account-area ptb-80">
        <div class="container">
            <div class="row" align="centre">
               <div class="col-sm-6 col-xs-12 lr2">
                    @if(session('ThongBao'))
                        <div class="forgotten forg">
                            {{session('ThongBao')}}
                        </div>
                        @endif
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{$error}}
                                    {!!'<br>'!!}
                                @endforeach
                        </div>
                        @endif
                    <form action="dang-ki" method="POST">
                        {{csrf_field()}}
                        <div class="login-reg">
                            <h3>Đăng ký</h3>                   
                            <div class="input-box mb-20">
                                    <label class="control-label">Tài Khoản</label>
                                    <input type="email" placeholder="E-Mail" value="" name="email" class="info">
                                </div>
                                <div class="input-box mb-20">
                                        <label class="control-label">Tên người dùng</label>
                                        <input type="text" placeholder="Tên người dùng" value="" name="tenUser" class="info">
                                    </div>
                                <div class="input-box mb-20">
                                    <label class="control-label">Mật Khẩu</label>
                                    <input type="password" placeholder="Password" value="" name="password" class="info">
                                </div>
                                <div class="input-box mb-20">
                                        <label class="control-label">Nhập lại Mật Khẩu</label>
                                        <input type="password" placeholder="nhập lại Password" value="" name="repassword" class="info">
                                </div>
                                <div class="input-box mb-20">
                                        <label class="control-label">Số điện thoại</label>
                                        <input type="text" placeholder="Số điện thoại" value="" name="SDT" class="info">
                                </div>
                                <div class="input-box mb-20">
                                        <label class="control-label">Địa chỉ</label>
                                        <input type="text" placeholder="Địa chỉ" value="" name="DiaChi" class="info">
                                </div>
                                <div class="input-box mb-20">
                                        <label class="control-label">Ngày sinh</label>
                                        <input type="date" placeholder="Ngày sinh" value="" name="NgaySinh" class="info">
                                </div>
                                <span>
                                    <input class="remr" type="checkbox" name="agree"> Đồng ý với điều khoản
                                </span>
                        </div>
                        <div class="frm-action">
                                <div class="input-box tci-box">
                                        {{-- <a href="" class="btn-def btn2">Đăng Nhập</a> --}}
                                        <button  class="btn btn-info" type="submit">Đăng ký</button>
                                </div>
                        </div>
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Account Area End -->
    
@endsection