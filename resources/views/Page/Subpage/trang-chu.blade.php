@extends('Page.master')
@section('content-container')
<!--slider area start-->
<div class="slider-area pos-rltv carousel slide carosule-pagi cp-line" data-ride="carousel">
    <div class="active-slider"> 
        @foreach($Banner as $banner)
        <div class="single-slider pos-rltv">
            <div class="slider-img"><a href="/tin/{{$banner->id}}"><img src="upload/image_tin/{{$banner->urlHinh}}" alt=""></a></div>
        </div>
        @endforeach
    </div>
</div>
<!--slider area start-->
<!--branding-section-area start-->
{{-- <div class="branding-section-area">
    <div class="container">
        <div class="row">
            <div class="active-slider pos-rltv carosule-pagi cp-line pagi-02">
                @foreach($KhuyenMai as $khuyenmai)
                @foreach($khuyenmai->Tin as $tin)
                <div class="single-slider">
                    <div class="col-lg-7 col-md-6 col-sm-6 col-xs-12"> 
                        <div class="brand-img text-center">
                        <img src="upload/image_tin/{{$tin->urlHinh}}" alt="">
</div>
</div>
<div class="col-lg-5 col-md-6 col-sm-6 col-xs-12">
    <div class="brand-content ptb-55">
        <div class="brand-text color-lightgrey">
            <h6>Khuyến Mãi</h6>
            <h2 class="uppercase montserrat">{{$tin->TenTin}}</h2>
            <div class="social-icon-wraper mt-35">
                <div class="social-icon">
                    <ul class="btn btn-danger">
                        <a href="/">Chi Tiết</a>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endforeach
@endforeach

</div>
</div>
</div>
</div> --}}
<!--branding-section-area end-->

<!--Sách yeu thích-->
<div class="new-arrival-area pt-70" @if(!Auth::check()) {{"hidden='hidden'"}} @endif>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="heading-title heading-style pos-rltv mb-50 text-center">
                    <h5 class="uppercase">Sách Bạn Thích</h5>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="total-new-arrival new-arrival-slider-active carsoule-btn">

                @foreach($Sach as $sach)
                <?php 
                if(!Auth::check()){
                    $id_u= -1;
                }else{
                    $id_u =Auth::user()->id;
                 } ?>
                @foreach($Like as $like)
                @if($sach->id == $like->id_Sach && $like->id_User == $id_u)
                <div class="col-md-3">
                    <!-- single product start-->
                    <div class="single-product">
                        <div class="product-img">
                            <div class="single-prodcut-img  product-overlay pos-rltv">
                                <a href="chi-tiet/{{$sach->id}}"> <img alt="" src="upload/biasach/{{$sach->urlHinh}}"
                                        class="primary-image"> <img alt="" src="upload/biasach/{{$sach->urlHinh}}"
                                        class="secondary-image"> </a>
                            </div>
                            <div class="product-icon socile-icon-tooltip text-center">
                                <ul>
                                    <li><a href="#" data-tooltip="Add To Cart" class="add-cart" data-placement="left"><i
                                                class="fa fa-cart-plus"></i></a></li>
                                    <li><a href="
                                    @if(!Auth::check())
                                        login
                                    @else
                                        xu-ly-thich/{{$sach->id}}
                                    @endif" data-tooltip="Thích" class="w-list"><i class="fa fa-heart-o"></i></a></li>
                                    <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                class="fa fa-refresh"></i></a></li>
                                    <li><a href="#" data-tooltip="Quick View" class="q-view" data-toggle="modal"
                                            data-target=".modal"><i class="fa fa-eye"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="product-text">
                            @if ($sach->GiaSale > 0)
                                    <div class="product-label">
                                            <div class="new">Sale</div>
                                    </div>
                            @endif
                            <div class="prodcut-name"> <a href="chi-tiet/{{$sach->id}}">{{$sach->TenSach}}</a> </div>
                            <div class="prodcut-ratting-price">
                                <div class="prodcut-price">
                                    @if ($sach->GiaSale > 0)
                                    <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                    </div>
                                    <div class="old-price">
                                        <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                    @else
                                    <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- single product end-->
                </div>
                @endif
                @endforeach
                @endforeach
            </div>
        </div>
    </div>
</div>
<!--Sách yeu thic-->

<!--discunt-featured-onsale-area start -->
<div class="discunt-featured-onsale-area pt-60">
    <div class="container">
        <div class="row">
            <div class="product-area tab-cars-style">
                <div class="title-tab-product-category">
                    <div class="col-md-12 text-center">
                        <ul class="nav mb-40 heading-style-2" role="tablist">
                            <li role="presentation" class="active"><a href="#newarrival" aria-controls="newarrival"
                                    role="tab" data-toggle="tab">Sách Mới</a></li>
                            {{-- <li role="presentation"><a href="#bestsellr" aria-controls="bestsellr" role="tab" data-toggle="tab">Best Seller</a></li>
                            <li role="presentation"><a href="#specialoffer" aria-controls="specialoffer" role="tab" data-toggle="tab">Special Offer</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="content-tab-product-category">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="newarrival">
                            <div class="total-new-arrival new-arrival-slider-active carsoule-btn">
                                @foreach($SachNew as $sachnew)
                                <div class="col-md-3">
                                    <!-- single product start-->
                                    <div class="single-product">
                                        <div class="product-img">
                                            <div class="product-label">
                                                <div class="new">New</div>
                                            </div>
                                            <div class="single-prodcut-img  product-overlay pos-rltv">
                                                <a href="chi-tiet/{{$sachnew->id}}"> <img alt=""
                                                        src="upload/biasach/{{$sachnew->urlHinh}}"
                                                        class="primary-image"> <img alt=""
                                                        src="upload/biasach/{{$sachnew->urlHinh}}"
                                                        class="secondary-image"> </a>
                                            </div>
                                            <div class="product-icon socile-icon-tooltip text-center">
                                                <ul>
                                                    <li><a href="{{route('themvaogiohang',$sachnew->id)}}"
                                                            data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                            data-placement="left"><i class="fa fa-cart-plus"></i></a>
                                                    </li>
                                                    <li><a href="
                                                        @if(!Auth::check())
                                                            login
                                                        @else
                                                            xu-ly-thich/{{$sachnew->id}}
                                                        @endif
                                                        " data-tooltip="Thích" class="w-list"><i
                                                                class="fa fa-heart-o"></i></a></li>
                                                    <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                                class="fa fa-refresh"></i></a></li>
                                                    <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                            data-toggle="modal" data-target=".modal"><i
                                                                class="fa fa-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <div class="prodcut-name"> <a
                                                    href="chi-tiet/{{$sach->id}}">{{$sachnew->TenSach}}</a> </div>
                                            <div class="prodcut-ratting-price">
                                                <div class="prodcut-price">
                                                    @if ($sach->GiaSale > 0)
                                                    <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                                    </div>
                                                    <div class="old-price">
                                                        <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                                    @else
                                                    <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single product end-->
                                </div>
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--discunt-featured-onsale-area end-->

{{-- sach Kinh tế--}}
<div class="discunt-featured-onsale-area pt-60">
    <div class="container">
        <div class="row">
            <div class="product-area tab-cars-style">
                <div class="title-tab-product-category">
                    <div class="col-md-12 text-center">
                        <ul class="nav mb-40 heading-style-2" role="tablist">
                            <li role="presentation" class="active"><a href="#newarrival" aria-controls="newarrival"
                                    role="tab" data-toggle="tab">Kinh tế</a></li>
                            {{-- <li role="presentation"><a href="#bestsellr" aria-controls="bestsellr" role="tab" data-toggle="tab">Best Seller</a></li>
                            <li role="presentation"><a href="#specialoffer" aria-controls="specialoffer" role="tab" data-toggle="tab">Special Offer</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="content-tab-product-category">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="newarrival">
                            <div class="total-new-arrival new-arrival-slider-active carsoule-btn">
                                @foreach($Sach as $sach)
                                @foreach($sach->TheLoai as $theloai)
                                @if($theloai->id == 2)
                                <div class="col-md-3">
                                    <!-- single product start-->
                                    <div class="single-product">
                                        <div class="product-img">
                                            {{-- <div class="product-label">
                                                <div class="new">New</div>
                                            </div> --}}
                                            <div class="single-prodcut-img  product-overlay pos-rltv">
                                                <a href="chi-tiet/{{$sach->id}}"> <img alt=""
                                                        src="upload/biasach/{{$sach->urlHinh}}" class="primary-image">
                                                </a>
                                            </div>
                                            <div class="product-icon socile-icon-tooltip text-center">
                                                <ul>
                                                    <li><a href="{{route('themvaogiohang',$sach->id)}}"
                                                            data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                            data-placement="left"><i class="fa fa-cart-plus"></i></a>
                                                    </li>
                                                    <li><a href="
                                                        @if(!Auth::check())
                                                            login
                                                        @else
                                                            xu-ly-thich/{{$sach->id}}
                                                        @endif
                                                        " data-tooltip="Thích" class="w-list"><i
                                                                class="fa fa-heart-o"></i></a></li>
                                                    <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                                class="fa fa-refresh"></i></a></li>
                                                    <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                            data-toggle="modal" data-target=".modal"><i
                                                                class="fa fa-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <div class="prodcut-name"> <a
                                                    href="chi-tiet/{{$sach->id}}">{{$sach->TenSach}}</a> </div>
                                            <div class="prodcut-ratting-price">
                                                <div class="prodcut-price">
                                                    @if ($sach->GiaSale > 0)
                                                    <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                                    </div>
                                                    <div class="old-price">
                                                        <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                                    @else
                                                    <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single product end-->
                                </div>
                                @endif
                                @endforeach
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- sach Kinh tế --}}

{{-- sach Van hoc --}}
<div class="discunt-featured-onsale-area pt-60">
    <div class="container">
        <div class="row">
            <div class="product-area tab-cars-style">
                <div class="title-tab-product-category">
                    <div class="col-md-12 text-center">
                        <ul class="nav mb-40 heading-style-2" role="tablist">
                            <li role="presentation" class="active"><a href="#newarrival" aria-controls="newarrival"
                                    role="tab" data-toggle="tab">Văn Học</a></li>
                            {{-- <li role="presentation"><a href="#bestsellr" aria-controls="bestsellr" role="tab" data-toggle="tab">Best Seller</a></li>
                            <li role="presentation"><a href="#specialoffer" aria-controls="specialoffer" role="tab" data-toggle="tab">Special Offer</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="content-tab-product-category">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="newarrival">
                            <div class="total-new-arrival new-arrival-slider-active carsoule-btn">
                                @foreach($Sach as $sach)
                                @foreach($sach->TheLoai as $theloai)
                                @if($theloai->id ==3)
                                <div class="col-md-3">
                                    <!-- single product start-->
                                    <div class="single-product">
                                        <div class="product-img">
                                            {{-- <div class="product-label">
                                                <div class="new">New</div>
                                            </div> --}}
                                            <div class="single-prodcut-img  product-overlay pos-rltv">
                                                <a href="chi-tiet/{{$sach->id}}"> <img alt=""
                                                        src="upload/biasach/{{$sach->urlHinh}}" class="primary-image">
                                                </a>
                                            </div>
                                            <div class="product-icon socile-icon-tooltip text-center">
                                                <ul>
                                                    <li><a href="{{route('themvaogiohang',$sach->id)}}"
                                                            data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                            data-placement="left"><i class="fa fa-cart-plus"></i></a>
                                                    </li>
                                                    <li><a href="
                                                        @if(!Auth::check())
                                                            login
                                                        @else
                                                            xu-ly-thich/{{$sach->id}}
                                                        @endif
                                                        " data-tooltip="Thích" class="w-list"><i
                                                                class="fa fa-heart-o"></i></a></li>
                                                    <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                                class="fa fa-refresh"></i></a></li>
                                                    <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                            data-toggle="modal" data-target=".modal"><i
                                                                class="fa fa-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <div class="prodcut-name"> <a
                                                    href="chi-tiet/{{$sach->id}}">{{$sach->TenSach}}</a> </div>
                                            <div class="prodcut-ratting-price">
                                                <div class="prodcut-price">
                                                    @if ($sach->GiaSale > 0)
                                                    <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                                    </div>
                                                    <div class="old-price">
                                                        <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                                    @else
                                                    <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single product end-->
                                </div>
                                @endif
                                @endforeach
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- sach Van hoc --}}

{{-- sach Tiểu sử hồi ký --}}
<div class="discunt-featured-onsale-area pt-60">
    <div class="container">
        <div class="row">
            <div class="product-area tab-cars-style">
                <div class="title-tab-product-category">
                    <div class="col-md-12 text-center">
                        <ul class="nav mb-40 heading-style-2" role="tablist">
                            <li role="presentation" class="active"><a href="#newarrival" aria-controls="newarrival"
                                    role="tab" data-toggle="tab">Tiểu sử hồi ký</a></li>
                            {{-- <li role="presentation"><a href="#bestsellr" aria-controls="bestsellr" role="tab" data-toggle="tab">Best Seller</a></li>
                            <li role="presentation"><a href="#specialoffer" aria-controls="specialoffer" role="tab" data-toggle="tab">Special Offer</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="content-tab-product-category">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="newarrival">
                            <div class="total-new-arrival new-arrival-slider-active carsoule-btn">
                                @foreach($Sach as $sach)
                                @foreach($sach->TheLoai as $theloai)
                                @if($theloai->id ==5)
                                <div class="col-md-3">
                                    <!-- single product start-->
                                    <div class="single-product">
                                        <div class="product-img">
                                            {{-- <div class="product-label">
                                                <div class="new">New</div>
                                            </div> --}}
                                            <div class="single-prodcut-img  product-overlay pos-rltv">
                                                <a href="chi-tiet/{{$sach->id}}"> <img alt=""
                                                        src="upload/biasach/{{$sach->urlHinh}}" class="primary-image">
                                                </a>
                                            </div>
                                            <div class="product-icon socile-icon-tooltip text-center">
                                                <ul>
                                                    <li><a href="{{route('themvaogiohang',$sach->id)}}"
                                                            data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                            data-placement="left"><i class="fa fa-cart-plus"></i></a>
                                                    </li>
                                                    <li><a href="
                                                        @if(!Auth::check())
                                                            login
                                                        @else
                                                            xu-ly-thich/{{$sach->id}}
                                                        @endif
                                                        " data-tooltip="Thích" class="w-list"><i
                                                                class="fa fa-heart-o"></i></a></li>
                                                    <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                                class="fa fa-refresh"></i></a></li>
                                                    <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                            data-toggle="modal" data-target=".modal"><i
                                                                class="fa fa-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <div class="prodcut-name"> <a
                                                    href="chi-tiet/{{$sach->id}}">{{$sach->TenSach}}</a> </div>
                                            <div class="prodcut-ratting-price">
                                                <div class="prodcut-price">
                                                    @if ($sach->GiaSale > 0)
                                                    <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                                    </div>
                                                    <div class="old-price">
                                                        <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                                    @else
                                                    <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single product end-->
                                </div>
                                @endif
                                @endforeach
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- sach Tiểu sử hồi ký --}}

{{-- sach Lịch sử --}}
<div class="discunt-featured-onsale-area pt-60">
    <div class="container">
        <div class="row">
            <div class="product-area tab-cars-style">
                <div class="title-tab-product-category">
                    <div class="col-md-12 text-center">
                        <ul class="nav mb-40 heading-style-2" role="tablist">
                            <li role="presentation" class="active"><a href="#newarrival" aria-controls="newarrival"
                                    role="tab" data-toggle="tab">Lịch sử</a></li>
                            {{-- <li role="presentation"><a href="#bestsellr" aria-controls="bestsellr" role="tab" data-toggle="tab">Best Seller</a></li>
                            <li role="presentation"><a href="#specialoffer" aria-controls="specialoffer" role="tab" data-toggle="tab">Special Offer</a></li> --}}
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="content-tab-product-category">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="newarrival">
                            <div class="total-new-arrival new-arrival-slider-active carsoule-btn">
                                @foreach($Sach as $sach)
                                @foreach($sach->TheLoai as $theloai)
                                @if($theloai->id ==13)
                                <div class="col-md-3">
                                    <!-- single product start-->
                                    <div class="single-product">
                                        <div class="product-img">
                                            {{-- <div class="product-label">
                                                <div class="new">New</div>
                                            </div> --}}
                                            <div class="single-prodcut-img  product-overlay pos-rltv">
                                                <a href="chi-tiet/{{$sach->id}}"> <img alt=""
                                                        src="upload/biasach/{{$sach->urlHinh}}" class="primary-image">
                                                </a>
                                            </div>
                                            <div class="product-icon socile-icon-tooltip text-center">
                                                <ul>
                                                    <li><a href="{{route('themvaogiohang',$sach->id)}}"
                                                            data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                            data-placement="left"><i class="fa fa-cart-plus"></i></a>
                                                    </li>
                                                    <li><a href="
                                                        @if(!Auth::check())
                                                            login
                                                        @else
                                                            xu-ly-thich/{{$sach->id}}
                                                        @endif
                                                        " data-tooltip="Thích" class="w-list"><i
                                                                class="fa fa-heart-o"></i></a></li>
                                                    <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                                class="fa fa-refresh"></i></a></li>
                                                    <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                            data-toggle="modal" data-target=".modal"><i
                                                                class="fa fa-eye"></i></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="product-text">
                                            <div class="prodcut-name"> <a
                                                    href="chi-tiet/{{$sach->id}}">{{$sach->TenSach}}</a> </div>
                                            <div class="prodcut-ratting-price">
                                                <div class="prodcut-price">
                                                    @if ($sach->GiaSale > 0)
                                                    <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                                    </div>
                                                    <div class="old-price">
                                                        <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                                    @else
                                                    <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                                    </div>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- single product end-->
                                </div>
                                @endif
                                @endforeach
                                @endforeach
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
{{-- sach Lịch sử --}}

<!--new-arrival on-sale Top-ratted area start-->

<!--new-arrival on-sale Top-ratted area end-->

<!--brand area are start-->
<div class="brand-area ptb-60">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="heading-title heading-style pos-rltv mb-50 text-center">
                    <h5 class="uppercase">Nhà Xuất Bản</h5>
                </div>
            </div>
            <div class="total-brand">
                @foreach($NXB as $nxb)
                <div class="col-md-3">
                    <div class="single-brand shadow-box"><a href="view-NXB/{{$nxb->id}}"><img
                                src="upload/logoNXB/{{$nxb->logo}}" alt=""></a></div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!--brand area are start-->

{{-- <!--blog area are start-->
<div class="blog-area pb-70">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="heading-title heading-style pos-rltv mb-50 text-center">
                    <h5 class="uppercase">Blog</h5>
                </div>
            </div>
            <div class="total-blog">
                <div class="col-md-4">
                    {{--TODO Làm blog--}}
                    {{-- <div class="single-blog">
                        <div class="blog-img pos-rltv product-overlay">
                            <a href="#"><img src="images/blog/01.jpg" alt=""></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-title">
                                <h5 class="uppercase font-bold"><a href="#">New fashion collection 2016</a></h5>
                                <div class="like-comments-date">
                                    <ul>
                                        <li><a href="#"><i class="zmdi zmdi-favorite-outline"></i>13 Like</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comment-outline"></i>03 Comments</a></li>
                                        <li class="blog-date"><a href="#"><i class="zmdi zmdi-calendar-alt"></i>16 jun
                                                2016</a></li>
                                    </ul>
                                </div>
                                <div class="blog-text">
                                    <p>It is a long established fact that a reader will be distracted by the readable
                                        content of a page when looking at its layout. The point of using.</p>
                                </div>
                                <a class="read-more montserrat" href="#">Read More</a>
                            </div>
                        </div>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!--blog area are end--> --}}


<!--delivery service start-->
<div class="delivery-service-area ptb-30">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 text-center">
                <div class="heading-title heading-style pos-rltv mb-50 text-center">
                    <h5 class="uppercase">dịch vụ</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-service shadow-box text-center">
                    <img src="frontend/images/icons/garantee.png" alt="">
                    <h5>Money Back Guarantee</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-service shadow-box text-center">
                    <img src="frontend/images/icons/coupon.png" alt="">
                    <h5>Gift Coupon</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-service shadow-box text-center">
                    <img src="frontend/images/icons/delivery.png" alt="">
                    <h5>Free Shipping</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="single-service shadow-box text-center">
                    <img src="frontend/images/icons/support.png" alt="">
                    <h5>24x7 Support</h5>
                </div>
            </div>
        </div>
    </div>
</div>
<!--delivery service start-->

<!-- QUICKVIEW PRODUCT -->
<div id="quickview-wrapper">
    <!-- Modal -->
    <div class="modal fade" id="productModal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="modal-product">
                        <div class="product-images">
                            <!--modal tab start-->
                            <div class="portfolio-thumbnil-area-2">
                                <div class="tab-content active-portfolio-area-2">
                                    <div role="tabpanel" class="tab-pane active" id="view1">
                                        <div class="product-img">
                                            <a href="#"><img src="images/product/01.jpg" alt="Single portfolio" /></a>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="view2">
                                        <div class="product-img">
                                            <a href="#"><img src="images/product/02.jpg" alt="Single portfolio" /></a>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="view3">
                                        <div class="product-img">
                                            <a href="#"><img src="images/product/03.jpg" alt="Single portfolio" /></a>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="view4">
                                        <div class="product-img">
                                            <a href="#"><img src="images/product/04.jpg" alt="Single portfolio" /></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="product-more-views-2">
                                    <div class="thumbnail-carousel-modal-2" data-tabs="tabs">
                                        <a href="#view1" aria-controls="view1" data-toggle="tab"><img
                                                src="images/product/01.jpg" alt="" /></a>
                                        <a href="#view2" aria-controls="view2" data-toggle="tab"><img
                                                src="images/product/02.jpg" alt="" /></a>
                                        <a href="#view3" aria-controls="view3" data-toggle="tab"><img
                                                src="images/product/03.jpg" alt="" /></a>
                                        <a href="#view4" aria-controls="view4" data-toggle="tab"><img
                                                src="images/product/04.jpg" alt="" /></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--modal tab end-->
                        <!-- .product-images -->
                        <div class="product-info">
                            <h1>Aenean eu tristique</h1>
                            <div class="price-box-3">
                                <div class="s-price-box"> <span class="new-price">$160.00</span> <span
                                        class="old-price">$190.00</span> </div>
                            </div> <a href="shop.html" class="see-all">See all features</a>
                            <div class="quick-add-to-cart">
                                <form method="post" class="cart">
                                    <div class="numbers-row">
                                        <input type="number" id="french-hens" value="3" min="1"> </div>
                                    <button class="single_add_to_cart_button" type="submit">Add to cart</button>
                                </form>
                            </div>
                            <div class="quick-desc"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam
                                fringilla augue nec est tristique auctor. Donec non est at libero.Lorem ipsum dolor sit
                                amet, consectetur adipiscing elit. Nam fringilla augue nec est tristique auctor. Donec
                                non est at libero.Nam fringilla tristique auctor. </div>
                            <div class="social-sharing-modal">
                                <div class="widget widget_socialsharing_widget">
                                    <h3 class="widget-title-modal">Share this product</h3>
                                    <ul class="social-icons-modal">
                                        <li><a target="_blank" title="Facebook" href="#"
                                                class="facebook m-single-icon"><i class="fa fa-facebook"></i></a></li>
                                        <li><a target="_blank" title="Twitter" href="#" class="twitter m-single-icon"><i
                                                    class="fa fa-twitter"></i></a></li>
                                        <li><a target="_blank" title="Pinterest" href="#"
                                                class="pinterest m-single-icon"><i class="fa fa-pinterest"></i></a></li>
                                        <li><a target="_blank" title="Google +" href="#" class="gplus m-single-icon"><i
                                                    class="fa fa-google-plus"></i></a></li>
                                        <li><a target="_blank" title="LinkedIn" href="#"
                                                class="linkedin m-single-icon"><i class="fa fa-linkedin"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- .product-info -->
                    </div>
                    <!-- .modal-product -->
                </div>
                <!-- .modal-body -->
            </div>
            <!-- .modal-content -->
        </div>
        <!-- .modal-dialog -->
    </div>
    <!-- END Modal -->
</div>
<!-- END QUICKVIEW PRODUCT -->
@endsection

@section('script')
<script>
    $('.carousel').carousel({
  interval: 1000
})
    </script>
@endsection