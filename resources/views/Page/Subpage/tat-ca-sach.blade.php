@extends('Page.master')
@section('content-container')
<!--shop main area are start-->
<div class="shop-main-area ptb-70">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="shop-area-top">
                    <div class="row">
                        <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                            <div class="sort product-show">
                                <label>View</label>
                                <select id="input-amount">
                                    <option value="volvo">10</option>
                                    <option value="saab">15</option>
                                    <option value="vw">20</option>
                                    <option value="audi" selected>25</option>
                                </select>
                            </div>
                            <div class="sort product-type">
                                <label>Sort By</label>
                                <select id="input-sort">
                                    <option value="#" selected>Default</option>
                                    <option value="#">Name (A - Z)</option>
                                    <option value="#">Name (Z - A)</option>
                                    <option value="#">Price (Low &gt; High)</option>
                                    <option value="#">Price (High &gt; Low)</option>
                                    <option value="#">Rating (Highest)</option>
                                    <option value="#">Rating (Lowest)</option>
                                    <option value="#">Model (A - Z)</option>
                                    <option value="#">Model (Z - A)</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-3 col-sm-2 col-xs-12">
                            <div class="list-grid-view text-center">
                                <ul class="nav" role="tablist">
                                    <li role="presentation"><a href="#grid" aria-controls="grid" role="tab"
                                            data-toggle="tab"><i class="zmdi zmdi-widgets"></i></a></li>
                                    <li role="presentation" class="active"><a href="#list" aria-controls="list"
                                            role="tab" data-toggle="tab"><i class="zmdi zmdi-view-list-alt"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 hidden-xs">
                            <div class="total-showing text-right">
                                Showing - <span>10</span> to <span>18</span> Of Total <span>36</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="shop-total-product-area clearfix mt-35">
                <!-- Tab panes -->
                <div class="tab-content">
                    <!--tab grid are start-->
                    <div role="tabpanel" class="tab-pane fade in active" id="grid">
                        <div class="total-shop-product-grid">
                            @foreach($Sach as $sach)
                            @if($sach->TrangThai == 1)
                            <div class="col-md-3 col-sm-4 col-xs-12 item">
                                <!-- single product start-->
                                <div class="single-product">
                                    <div class="product-img">

                                        <!-- TODO: xử lý if mới -->
                                        {{-- <div class="product-label red">
                                                            <div class="new">New</div>
                                                        </div> --}}
                                        @if ($sach->GiaSale > 0)
                                        <div class="product-label">
                                            <div class="new">Sale</div>
                                        </div>
                                        @endif

                                        <div class="single-prodcut-img  product-overlay pos-rltv">
                                            <a href="{{route('chi-tiet',$sach->id)}}">
                                                <img alt="" src="upload/biasach/{{$sach->urlHinh}}"
                                                    class="primary-image">
                                                {{-- <img alt="" src="images/product/02.jpg" class="secondary-image">  --}}
                                            </a>
                                        </div>
                                        <div class="product-icon socile-icon-tooltip text-center">
                                            <ul>
                                                <li><a href="{{route('themvaogiohang',$sach->id)}}"
                                                        data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                        data-placement="left"><i class="fa fa-cart-plus"></i></a></li>
                                                <li><a href="
                                                                    @if(!Auth::check())
                                                                    login
                                                                @else
                                                                    xu-ly-thich/{{$sach->id}}
                                                                @endif
                                                                    " data-tooltip="Thích" class="w-list"><i
                                                            class="fa fa-heart-o"></i></a></li>
                                                <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                            class="fa fa-refresh"></i></a></li>
                                                <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                        data-toggle="modal" data-target=".modal"><i
                                                            class="fa fa-eye"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="product-text">
                                        <div class="prodcut-name"> <a
                                                href="{{route('chi-tiet',$sach->id)}}">{{$sach->TenSach}}</a> </div>
                                        <div class="prodcut-ratting-price">
                                            <div class="prodcut-price">
                                                @if ($sach->GiaSale > 0)
                                                <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ
                                                </div>
                                                <div class="old-price">
                                                    <del>{{number_format($sach->Gia,0, ",",".")}} vnđ</del> </div>
                                                @else
                                                <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- single product end-->
                            </div>
                            @endif
                            @endforeach

                        </div>
                    </div>
                    <!--shop product list end-->

                    <!--pagination start-->
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="pagination-btn text-center">
                            {{$Sach->links()}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--shop main area are end-->

@endsection
