@extends('Page.master')
@section('content-container')

    <!-- Account Area Start -->
    <div class="account-area ptb-80">
        <div class="container">
            <div class="row" align="centre">
                <div class="col-sm-6 col-xs-12">
                    
                        @if(session('ThongBao'))
                        <div class="forgotten forg">
                            {{session('ThongBao')}}
                        </div>
                        @endif
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    {{$error}}
                                    {!!'<br>'!!}
                                @endforeach
                        </div>
                        @endif

                <form action="login" method="POST" class="login-side">
                        {{csrf_field()}}
                        <div class="login-reg">
                            <h3>Đăng Nhập</h3>
                            
                            <div class="input-box mb-20">
                                <label class="control-label">Tài Khoản</label>
                                <input type="email" placeholder="E-Mail" value="" name="email" class="info">
                            </div>
                            <div class="input-box">
                                <label class="control-label">Mật Khẩu</label>
                                <input type="password" placeholder="Password" value="" name="password" class="info">
                            </div>
                        </div>
                        <div class="frm-action">
                            <div class="input-box tci-box">
                                {{-- <a href="" class="btn-def btn2">Đăng Nhập</a> --}}
                                <button  class="btn btn-info" type="submit">Đăng Nhập</button>
                            </div>
                            <span>
                         <input class="remr" type="checkbox" name="remember"> Nhớ mật khẩu
                     </span>
                            <a href="#" class="forgotten forg">Quên mật khẩu?</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>
    <!-- Account Area End -->
    
@endsection