<!-- cart are start-->
<div class="cart-page-area">
    <form method="post" action="#">
        <div class="table-responsive mb-20">
            <table class="shop_table-2 cart table">
                <thead>
                    <tr>
                        <th class="product-thumbnail ">Hình</th>
                        <th class="product-name ">Tên Sách</th>
                        <th class="product-price ">Giá</th>
                        <th class="product-quantity">Số Lượng</th>
                        <th class="product-subtotal ">Tổng Giá</th>
                        <th class="product-remove">Cập nhật</th>
                        <th class="product-remove">Xóa</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $tongtien=0 ?>

                    @if(Session::has('cart'))
                    @foreach ($cart->items as $item)
                    <?php $tongtien+= $item['price'] ?>
                    <tr class="cart_item">
                        <td class="item-img">
                            <a href="#"><img src="/upload/biasach/{{$item['item']['urlHinh']}}" alt=""> </a>
                        </td>
                        <td class="item-title"><a
                                href="{{route('chi-tiet',$item['item']['id'])}}">{{$item['item']['TenSach']}}</a></td>
                        <td class="item-price"> {{number_format($item['item']['Gia'],0, ",",".")}} vnđ</td>
                        <td class="item-qty">
                            <div class="cart-quantity">
                                <div class="product-qty">
                                    <div class="cart-quantity">
                                        <div class="cart-plus-minus">
                                            <div class="dec qtybutton">-</div>
                                            <input value="{{$item['Qty']}}" name="qtybutton"
                                                class="cart-plus-minus-box itemQty" type="text">
                                            <div class="inc qtybutton">+</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td class="total-price"><strong>{{number_format($item['price'],0, ",",".")}} vnđ</strong></td>
                        <td>
                            <div class="update-continue-btn text-right pb-20">
                                <a class="btn-def btn1 update-to-cart" data-key="{{$item['item']['id']}}">Cập nhật</a>
                            </div>
                        </td>
                        <td class="remove-item"><a href="{{route('xoakhoigiohang',$item['item']['id'])}}"><i
                                    class="fa fa-trash-o"></i></a></td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
            </table>
        </div>
    </form>
    
    <div class="cart-bottom-area">
        <div class="row">
            <div class="col-md-8 col-sm-7 col-xs-12">
                <div class="update-coupne-area">
                    <div class="update-continue-btn text-right pb-20">
                        <a href="/" class="btn-def btn2">Tiếp Tục Mua Hàng</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="table-responsive mb-20">
        <div class="cart-total-area tien">
            <div class="catagory-title cat-tit-5 mb-20 text-right">
                <h3>Đơn Hàng</h3>
            </div>
            <div class="sub-shipping">
                <p>Tổng Tiền <span>{{number_format($tongtien,0, ",",".")}} vnđ</span></p>
                <p>Phí giao hàng do bên giao hàng.</p>
            </div>
            <hr />
            <div class="process-cart-total">
                <p>Tổng <span>{{number_format($tongtien,0, ",",".")}} vnđ</span></p>
            </div>
    </div>
</div>
</div>
<!-- cart are end-->

@section('script')
<script>
    $(document).ready(function () {
        $(".update-to-cart").click(function () {
            var qty = $(this).parents("tr").find(".itemQty").val();
            var idsp = $(this).attr("data-key");

            $.get("update-to-cart/" + idsp + "/" + qty + "/", function (data) {
                $(".tien").append(data);

                if (data == 1) {
                    alert("update thành công");
                    location.reload();
                } else {
                    alert("update thất bại");
                    location.reload();
                }
            });
        });
    });

</script>
@endsection
