<!-- Start of header area -->
<header class="header-area header-wrapper">

            <div class="header-top-bar black-bg clearfix">
                <div class="container">
                    <div class="row">
                        @if(session('ThongBao'))
                            <div class="alert alert-success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{session('ThongBao')}}
                            </div>
                        @endif
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="login-register-area">
                                <ul>
                                @if(Auth::check())
                                <li><a href="{{route('tai-khoan')}}">Xin chào: {{Auth::user()->tenUser}}</a></li>
                                    <li><a href="logout">Thoát</a></li>
                                @else
                                    <li><a href="login">Đăng nhập</a></li>
                                    <li><a href="dang-ki">Đăng kí</a></li>
                                @endif
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 hidden-xs">
                            <div class="social-search-area text-center">
                                <div class="social-icon socile-icon-style-2">
                                    <ul>
                                        <li><a href="#" title="facebook"><i class="fa fa-facebook"></i></a> </li>
                                        <li><a href="#" title="twitter"><i class="fa fa-twitter"></i></a> </li>
                                        <li> <a href="#" title="dribble"><i class="fa fa-dribbble"></i></a></li>
                                        <li> <a href="#" title="behance"><i class="fa fa-behance"></i></a> </li>
                                        <li> <a href="#" title="rss"><i class="fa fa-rss"></i></a> </li>
                                    </ul>
                                 </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-6">
                            <div class="cart-currency-area login-register-area text-right">
                                <ul>
                                    <li>
                                        <div class="header-cart">
                                            <div class="cart-icon"><a href="{{route('gio-hang')}}">Cart<i class="zmdi zmdi-shopping-cart"></i></a> 
                                                <span>
                                                    @if(Session::has('cart'))
                                                        {{Session('cart')->totalQty}}
                                                        @else
                                                            0
                                                    @endif
                                                </span> </div>
                                            <div class="cart-content-wraper">
                                                @if(Session::has('cart'))
                                                @foreach ($product_cart as $product)
                                                <div class="cart-single-wraper">
                                                    <div class="cart-img">
                                                    <a href="#"><img src="upload/biasach/{{$product['item']['urlHinh']}}" alt=""></a>
                                                    </div>
                                                    <div class="cart-content">
                                                        <div class="cart-name"> <a href="#">{{$product['item']['TenSach']}}</a> </div>
                                                        <div class="cart-price">{{number_format($product['item']['Gia'],0, ",",".")}} vn₫</div>
                                                    <div class="cart-qty">số lượng: <span>{{$product['Qty']}}</span> </div>
                                                    </div>
                                                    <div class="remove"> <a href="{{route('borakhoigiohang',$product['item']['id'])}}"><i class="zmdi zmdi-close"></i></a> </div>
                                                </div>
                                                @endforeach
                                                <div class="cart-subtotal"> Tổng tiền: <span>{{number_format(Session('cart')->totalPrice,0, ",",".")}}</span> </div>
                                                <div class="cart-check-btn">
                                                    <div class="view-cart"> <a class="btn-def" href="{{route('gio-hang')}}">Giỏ hàng</a> </div>
                                                    <div class="check-btn"> <a class="btn-def" href="{{route('checkout')}}">Thanh toán</a> </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header"  class="header-middle-area">
                <div class="container">
                    <div class="full-width-mega-dropdown">
                        <div class="row">
                            <div class="col-md-2 col-sm-2">
                                <div class="logo ptb-20"><a href="/"> 
									<img src="frontend/images/logo/logo.png" alt="main logo"></a>
								</div>
                            </div>
                            <div class="col-md-7 col-sm-10 hidden-xs">
                                <nav id="primary-menu">
                                    <ul class="main-menu">
                                        <li ><a href="/">Home</a></li>
                                        <li><a href="/tin-tuc">Tin tức</a></li>
                                        <li class="mega-parent"><a href="tat-ca-sach">Sách</a>
                                            <div class="mega-menu-area mma-970">
                                                <ul class="single-mega-item">
                                                   <li class="menu-title uppercase">Sách</li>
                                                   <li><a href="tat-ca-sach">Tất cả sách</a></li>
                                                   <li><a href="" target="_blank">Sách mới</a></li>
                                                </ul>
                                                <ul class="single-mega-item">
                                                   <li class="menu-title uppercase">Thể loại</li>
                                                    @foreach ($TheLoai   as $theloai)
                                                <li><a href="the-loai/{{$theloai->id}}" target="_blank">{{$theloai->TenTL}} 
                                                    ({{($theloai["soluong"])}})
                                                </a></li>
                                                    @endforeach
                                                </ul>
                                               
                                            </div>
                                        </li>
                                        <li><a href="https://drive.google.com/open?id=1-dbcUSmb39cHB17NH3Ln7hhjOMd166HN" target="_blank">SOURCE</a></li>
                                    </ul>
                                </nav>
                            </div>
                            <div class="col-md-3 hidden-sm hidden-xs">
                                <div class="search-box global-table">
                                    <div class="global-row">
                                        <div class="global-cell">
                                            <form action="search" method="GET">
                                                <div class="input-box">
                                                    <input class="single-input" placeholder="Search anything" type="text" name="key"
                                                    value="{{Session::get('key')}}">
                                                    <button type="submit" class="src-btn" ><i class="fa fa-search"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                        
                            <!-- Mobile -->
                            {{-- @include('Page.Subpage.header_mobile') --}}

                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- End of header area -->