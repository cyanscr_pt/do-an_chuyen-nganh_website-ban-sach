@extends('Page.master')
@section('content-container')
<!--cart-checkout-area start -->
<div class="cart-checkout-area  pt-30">
    <div class="container">
        <div class="row">
            <div class="product-area">
                <div class="title-tab-product-category">
                    <div class="col-md-12 text-center pb-60">
                        <ul class="nav heading-style-3" role="tablist">
                            <li role="presentation" class="shadow-box"><a href="{{route('gio-hang')}}" aria-controls="cart"
                                    role="tab" data-toggle="tab"><span>01</span> Giỏ Hàng</a></li>
                            <li role="presentation" class="active shadow-box"><a saria-controls="checkout"
                                    role="tab" data-toggle="tab"><span>02</span>Đặt Mua</a></li>
                            <li role="presentation" class="shadow-box"><a href=""
                                    aria-controls="complete-order" role="tab" data-toggle="tab"><span>03</span> Xác
                                    Nhận</a></li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-sm-12">
                    <div class="content-tab-product-category pb-70">
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane  fade in active" id="checkout">
                                <!-- Checkout are start-->
                                <div class="checkout-area">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <div class="coupne-customer-area mb50">
                                                    <div class="panel-group" id="accordion" role="tablist"
                                                        aria-multiselectable="true">
                                                        <div class="panel panel-checkout">

                                                            <div class="panel panel-checkout">
                                                                <div class="panel-heading" role="tab" id="headingThree">
                                                                    <h4 class="panel-title">
                                                                        <img src="images/icons/acc.jpg" alt="">
                                                                        Bạn có mã giảm giá?
                                                                        <a class="collapsed" role="button"
                                                                            data-toggle="collapse"
                                                                            data-parent="#accordion"
                                                                            href="#collapseThree" aria-expanded="false"
                                                                            aria-controls="collapseThree">
                                                                            Hãy nhập mã giảm giá ở đây
                                                                        </a>
                                                                    </h4>
                                                                </div>
                                                                <div id="collapseThree" class="panel-collapse collapse"
                                                                    role="tabpanel" aria-labelledby="headingThree">
                                                                    <div class="panel-body coupon-body">

                                                                        <div class="first-last-area">
                                                                            <div class="input-box mtb-20">
                                                                                <input type="text"
                                                                                    placeholder="Coupon Code"
                                                                                    class="info" name="code">
                                                                            </div>
                                                                            <div class="frm-action">
                                                                                <div class="input-box tci-box">
                                                                                    <a href="#" class="btn-def btn2">Áp
                                                                                        dụng giảm giá</a>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-xs-12">
                                                            <div class="billing-details">
                                                                <div class="contact-text right-side">
                                                                    <h2>Chi tiết người nhận</h2>
                                                                    <form action="checkout" method="POST" id="form-thong-tin">
                                                                        {{csrf_field()}}
                                                                        <div class="row">
                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="input-box mb-20">
                                                                                    <label>Tên người
                                                                                        nhận<em>*</em></label>
                                                                                    <input type="text" name="name"
                                                                                        class="info"
                                                                                        placeholder="Tên người nhận" value="{{$user->tenUser}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="input-box mb-20">
                                                                                    <label>Số điện thoại người
                                                                                        nhận<em>*</em></label>
                                                                                    <input type="text" name="sdt"
                                                                                        class="info"
                                                                                    placeholder="Số điện thoại người nhận" value="{{$user->SDT}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="input-box mb-20">
                                                                                    <label>Địa chỉ nhận
                                                                                        hàng<em>*</em></label>
                                                                                    <input type="text" name="diachi"
                                                                                        class="info"
                                                                                        placeholder="Địa chỉ nhận hàng" value="{{$user->DiaChi}}">
                                                                                </div>
                                                                            </div>

                                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <div class="input-box mb-20">
                                                                                    <label>Ghi chú<em>*</em></label>
                                                                                    <input type="text" name="ghichu"
                                                                                        class="info"
                                                                                        placeholder="Ghi chú">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="cart-bottom-area">
                                                                            <div class="row">
                                                                                <div class="col-md-8 col-sm-7 col-xs-12">
                                                                                    <div class="update-coupne-area">
                                                                                        <div class="update-continue-btn text-right pb-20">
                                                                                            <a href="{{route('gio-hang')}}" class="btn-def btn2">Quay lại</a>
                                                                                            <a class="btn-def btn2" onclick="document.getElementById('form-thong-tin').submit()">Thanh toán</a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                        </div>
                                                                    </form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- Checkout are end-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--cart-checkout-area end-->
@endsection
