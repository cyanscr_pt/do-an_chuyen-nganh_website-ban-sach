@extends('Page.master')
@section('content-container')
        <!--cart-checkout-area start -->
        <div class="cart-checkout-area  pt-30">
                <div class="container">
                    <div class="row">
                        <div class="product-area">
                            <div class="title-tab-product-category">
                                <div class="col-md-12 text-center pb-60">
                                    <ul class="nav heading-style-3" role="tablist">
                                        <li role="presentation" class="shadow-box"><a aria-controls="cart" role="tab" data-toggle="tab"><span>01</span> Giỏ Hàng</a></li>
                                        <li role="presentation" class="shadow-box"><a aria-controls="checkout" role="tab" data-toggle="tab"><span>02</span>Đặt Mua</a></li>
                                        <li role="presentation" class="shadow-box active"><a aria-controls="complete-order" role="tab" data-toggle="tab"><span>03</span> Xác Nhận</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-sm-12">
                                <div class="content-tab-product-category pb-70">
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane  fade in active">
                                        <!-- payment are start -->
                                        <div class="row">
                                            <div class="col-xs-12">
                                            <div class="checkout-payment-area">
                                                <div class="checkout-total mt20">
                                                   <h3>Đơn hàng</h3>
                                               <form action="complete-order" method="post" id="form-complete-order">
                                                   {{ csrf_field() }}
                                               <div class="table-responsive">
                                                    <table class="checkout-area table">
                                                   <thead>
                                                    <tr class="cart_item check-heading">
                                                        <td class="ctg-type"> Sản phẩm</td>
                                                        <td class="cgt-des"> tổng cộng</td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                    
                                                        @foreach ($cart->items as $item)
                                                        <tr class="cart_item check-item prd-name">
                                                                <td class="ctg-type"> {{$item['item']['TenSach']}} × <span> {{$item['Qty']}}</span></td>
                                                                <td class="cgt-des">  {{number_format($item['price'],0, ",",".")}} vnđ</td>
                                                            </tr>
                                                        @endforeach
                                    
                                                        <tr class="cart_item">
                                                            <td class="ctg-type"> Tạm tính</td>
                                                            <td class="cgt-des">{{number_format($totalPrice,0, ",",".")}} vnđ</td>
                                                        </tr>
                                                        <tr class="cart_item">
                                                            <td class="ctg-type">Vận chuyển</td>
                                                                <td class="cgt-des ship-opt">
                                                                    <div class="shipp">
                                                                        <input type="radio" id="pay-toggle2" name="ship">
                                                                        <label for="pay-toggle2">Vận chuyển miễn phí</label>
                                                                    </div>
                                                             </td>
                                                        </tr>
                                                        <tr class="cart_item">
                                                            <td class="ctg-type crt-total">Tổng cộng</td>
                                                            <td class="cgt-des prc-total">{{number_format($totalPrice,0, ",",".")}} vnđ</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                               </div>
                                            </form>
                                            </div>
                                                <div class="payment-section mt-20 clearfix">
                                                    <div class="pay-toggle">
                                                           <div class="pay-type-total">
                                                            <div class="pay-type">
                                                                <input type="radio" id="pay-toggle01" name="pay">
                                                                <label for="pay-toggle01">Chuyển khoản</label>
                                                            </div>
                                                            <div class="pay-type">
                                                                <input type="radio" id="pay-toggle03" name="pay">
                                                                <label for="pay-toggle03">COD - Trả tiền khi nhận hàng</label>
                                                            </div>
                                    
                                                            </div>
                                                            <div class="input-box mt-20">
                                                                <a class="btn-def btn2" onclick="document.getElementById('form-complete-order').submit()">Đặt hàng</a>
                                                            </div>
                                                        </form>
                                                    </div>
                                    
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
            <!--cart-checkout-area end-->     
@endsection