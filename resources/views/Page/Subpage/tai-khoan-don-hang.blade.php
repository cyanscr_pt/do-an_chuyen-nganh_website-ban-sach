<div role="tabpanel" class="tab-pane fade in" id="devlopment">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr align="center">
                <th>ID</th>
                <th>Ngày mua</th>
                <th>Sản phẩm</th>
                <th>Tổng Tiền</th>
                <th>Trạng thái</th>
                <th>Xem chi tiết</th>
            </tr>
        </thead>
        <tbody class="text-center">
            @foreach ($lichSuMuaHang as $donhang)
            <tr>
                <td >{{$donhang->id}}</td>
                <td>{{$donhang->created_at}}</td>
                <td>
                    @foreach ($donhang['chitiet'] as $item)
                    {{$item['sach']['TenSach']}}
                    @endforeach
                </td>
                <td> {{number_format($donhang->TongTien,0, ",",".")}} vnđ</td>
                <td>
                    @if ($donhang->TrangThai == 0 || $donhang->TrangThai ==1)
                        <div class="text-center">
                            <div class="alert alert-warning" role="alert">Đang chờ xử lý</div>
                        </div>
                    @endif
                    @if ($donhang['TrangThai'] == 2)
                        <div class="text-center">
                            <div class="alert alert-info" role="alert">Đang giao hàng</div>
                        </div>
                    @endif
                    @if ($donhang['TrangThai'] == 3)
                        <div class="text-center">
                            <div class="alert alert-success" role="alert">Đơn hàng hoàn tất</div>
                        </div>
                    @endif
                    @if ($donhang['TrangThai'] == 4 ||$donhang['TrangThai'] == 5)
                        <div class="text-center">
                            <div class="alert alert-danger" role="alert">Đơn hàng bị huỷ</div>
                        </div>
                    @endif
                </td>
                <td align="center"> <a class="btn-def btn2 info"
                    href="/chi-tiet-don-hang/{{$donhang->id}}"> Chi tiết</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
