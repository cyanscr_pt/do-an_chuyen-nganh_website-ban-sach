@extends('Page.master')
@section('content-container')

<!--single blog main area are start-->
<div class="shop-main-area pt-70 pb-40">
    <div class="container">
        <div class="row">
            <!--main-shop-product start-->
            <div class="col-md-12 col-sm-8 col-xs-12">
                <div class="single-blog-body">
                    <div class="single-blog sb-2 mb-30">
                        <div class="blog-img pos-rltv product-overlay">
                            <a href=""><img src="/upload/image_tin/{{$blog->urlHinh}}" alt=""></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-title">
                                <h5 class="uppercase font-bold">{{$blog->TieuDeTin}}</h5>
                                <div>{!!$blog->NoiDungTin!!}</div>
                                
                            </div>
                                <div class="like-comments-date ">
                                    <ul>
                                        <li><a href="#"><i class="zmdi zmdi-favorite-outline"></i>13 Like</a></li>
                                        <li><a href="#"><i class="zmdi zmdi-comment-outline"></i>03 Comments</a>
                                        </li>
                                        <li class="blog-date"><a href=""><i class="zmdi zmdi-calendar-alt"></i>16
                                                jun
                                                2016</a></li>
                                    </ul>
                                </div>
                            <br>
                        </div>
                        @if (count($DanhSachSPKhuyenMai) >0)
                        <h5 class="uppercase">Các sản phẩm thuộc khuyến mãi này: </h5>
                        <br>
                        <div>
                            <!--tab grid are start-->
                            <div role="tabpanel" class="tab-pane fade in active" id="grid">
                                <div class="total-shop-product-grid">
                                    @foreach($DanhSachSPKhuyenMai as $sach)
                                    @if($sach['0']->TrangThai = 1)
                                    <div class="col-md-3 col-sm-4 col-xs-12 item">
                                        <!-- single product start-->
                                        <div class="single-product">
                                            <div class="product-img">

                                                <!-- TODO: xử lý if mới -->
                                                {{-- <div class="product-label red">
                                                        <div class="new">New</div>
                                                    </div> --}}
                                                @if ($sach['0']->GiaSale > 0)
                                                <div class="product-label">
                                                    <div class="new">Sale</div>
                                                </div>
                                                @endif

                                                <div class="single-prodcut-img  product-overlay pos-rltv">
                                                    <a href="chi-tiet/{{$sach['0']->id}}">
                                                        <img alt="" src="upload/biasach/{{$sach['0']->urlHinh}}"
                                                            class="primary-image">
                                                        {{-- <img alt="" src="images/product/02.jpg" class="secondary-image">  --}}
                                                    </a>
                                                </div>
                                                <div class="product-icon socile-icon-tooltip text-center">
                                                    <ul>
                                                        <li><a href="{{route('themvaogiohang',$sach['0']->id)}}"
                                                                data-tooltip="Thêm giỏ hàng" class="add-cart"
                                                                data-placement="left"><i
                                                                    class="fa fa-cart-plus"></i></a></li>
                                                        <li><a href="
                                                                @if(!Auth::check())
                                                                login
                                                            @else
                                                                xu-ly-thich/{{$sach['0']->id}}
                                                            @endif
                                                                " data-tooltip="Thích" class="w-list"><i
                                                                    class="fa fa-heart-o"></i></a></li>
                                                        <li><a href="#" data-tooltip="Compare" class="cpare"><i
                                                                    class="fa fa-refresh"></i></a></li>
                                                        <li><a href="#" data-tooltip="Quick View" class="q-view"
                                                                data-toggle="modal" data-target=".modal"><i
                                                                    class="fa fa-eye"></i></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="product-text">
                                                <div class="prodcut-name"> <a
                                                        href="{{route('chi-tiet',$sach['0']->id)}}">{{$sach['0']->TenSach}}</a>
                                                </div>
                                                <div class="prodcut-ratting-price">
                                                    <div class="prodcut-price">
                                                        @if ($sach['0']->GiaSale > 0)
                                                        <div class="new-price">
                                                            {{number_format($sach['0']->GiaSale,0, ",",".")}} vnđ
                                                        </div>
                                                        <div class="old-price">
                                                            <del>{{number_format($sach['0']->Gia,0, ",",".")}}</del>
                                                        </div>
                                                        @else
                                                        <div class="new-price">
                                                            {{number_format($sach['0']->Gia,0, ",",".")}} vnđ
                                                        </div>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- single product end-->
                                    </div>
                                    @endif
                                    @endforeach

                                </div>
                            </div>
                            <!--shop product list end-->
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!--main-shop-product start-->
        
        <div style="background: #fafafa; padding: 10px">
            <div class="comments-area fix mt-20">
                <h5 class="uppercase sb-title">Comments Area</h5>
                 <div class="comments-body">
                     <ul>
                         <li class="signle-comments">
                             <img src="images/blog/author.jpg" alt="">
                             <div class="commets-text">
                                 <h5>JOHN NGUYEN</h5>
                                 <span>July 15 , 2018</span> <span>, at 2:39 am</span>
                                 <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed.</p>
                             </div>
                             <div class="replay"><a href="#">Replay</a></div>
                         </li>
                         <li class="signle-comments">
                             <img src="images/blog/demo.jpg" alt="">
                             <div class="commets-text">
                                 <h5>JOHN NGUYEN</h5>
                                 <span>July 15 , 2018</span> <span>, at 2:39 am</span>
                                 <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed.</p>
                             </div>
                             <div class="replay"><a href="#">Replay</a></div>
                         </li>
                         <li class="signle-comments">
                             <img src="images/blog/author.jpg" alt="">
                             <div class="commets-text">
                                 <h5>JOHN NGUYEN</h5>
                                 <span>July 15 , 2018</span> <span>, at 2:39 am</span>
                                 <p>Exercitation photo booth stumptown tote bag Banksy, elit small batch freegan sed.</p>
                             </div>
                             <div class="replay"><a href="#">Replay</a></div>
                         </li>
                     </ul>
                 </div>
             </div>

             <div class="leave-through-area clearfix mt-40">
                <h5 class="uppercase sb-title">Leave your thought</h5>                                
                <div class="row">
                     <form action="mail.php" method="post">
                          <div class="col-md-6">
                               <div class="input-box mb-20">
                                   <input name="name" class="info" placeholder="Name*" type="text"> 
                               </div>
                           </div>
                          <div class="col-md-6">
                               <div class="input-box mb-20">
                                   <input name="name" class="info" placeholder="Email" type="email"> 
                               </div>
                           </div>
                          <div class="col-md-6">
                               <div class="input-box mb-20">
                                   <input name="name" class="info" placeholder="Phone" type="text"> 
                               </div>
                           </div>
                          <div class="col-md-6">
                               <div class="input-box mb-20">
                                   <input name="name" class="info" placeholder="Country" type="text"> 
                               </div>
                           </div>
                            <div class="col-md-12">
                                <div class="input-box mb-20">
                                    <textarea name="message" class="area-tex" placeholder="Your Message*"></textarea>
                                </div>
                            </div>
                          <div class="col-xs-12">
                               <div class="input-box">
                                   <input name="submit" class="sbumit-btn" value="Post Commetns" type="submit"> 
                               </div>
                           </div>

                       </form>
                </div>
            </div>
        </div>

    </div>
</div>
<!--single blog main area are end-->
@endsection
