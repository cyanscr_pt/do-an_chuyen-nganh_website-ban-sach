@if(count($errors) > 0)
<div class="alert alert-danger">
    @foreach ($errors->all() as $error)
    {{$error}}
    {!!'<br>'!!}
    @endforeach
</div>
@endif

<div role="tabpanel" class="tab-pane fade in active" id="creativity">
    <form action="updateThongTin" method="POST" id="form-thong-tin">
        {{csrf_field()}}
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="input-box mb-20">
                <label>Họ tên: <em>*</em></label>
                <input type="text" name="tenUser" class="info" placeholder="{{$user->tenUser}}" value="{{$user->tenUser}}">
            </div>
        </div>

        {{-- Không cho đổi email --}}
        {{-- <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="input-box mb-20">
                <label>email:<em>*</em></label>
                <input type="email" name="email" class="info" placeholder="{{$user->email}}" value="">
            </div>
        </div> --}}

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="input-box mb-20">
                <label>SDT<em>*</em></label>
                <input type="text" name="SDT" class="info" placeholder="{{$user->SDT}}" value="{{$user->SDT}}">
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="input-box mb-20">
                    <label class="control-label">Ngày sinh {{$user->NgaySinh}}</label>
                    <input type="date" placeholder="Ngày sinh" value="{{$user->NgaySinh}}" name="NgaySinh" class="info">
                </div>
            </div>
    

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="input-box mb-20">
                <label>Địa chỉ <em>*</em></label>
                <input type="text" name="DiaChi" class="info mb-10" placeholder="{{$user->DiaChi}}" value="{{$user->DiaChi}}">
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h5>Đổi mật khẩu</h5>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="input-box mb-20">
                <label>Mật khẩu<em>*</em></label>
                <input type="password" name="old_password" class="info" placeholder="mật khẩu hiện tại của bạn">
            </div>
        </div>

        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="input-box mb-20">
                <label>Mật khẩu mới<em>*</em></label>
                <input type="password" name="new_password" class="info" placeholder="Mật khẩu mới">
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
            <div class="input-box mb-20">
                <label>Xác nhận Mật khẩu mới<em>*</em></label>
                <input type="password" name="confirm_new_password" class="info" placeholder="Xác nhận Mật khẩu mới">
            </div>
        </div>
    </form>

    <div class="col-md-12 col-sm-12 col-xs-12 text-right">
        <a class="btn-def btn2" onclick="document.getElementById('form-thong-tin').submit()">Lưu thay đổi</a>
    </div>
   
</div>
