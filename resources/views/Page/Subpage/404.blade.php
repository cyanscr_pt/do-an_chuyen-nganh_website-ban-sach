@extends('Page.master')
@section('content-container')
            <!--404 area start-->
            <div class="area-404 ptb-80">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                               <div class="content-404 text-center">
                                <div class="text-404">
                                    <h1>Oops ! that page can't be found.</h1>
                                    <img src="upload/AR Rabbit.png" alt="">
                                    <p>Can't find what you need? Take a moment and do a search below!</p>
                                </div>
                                <div class="search-box serch-404">
                                    <form action="#">
                                        <div class="input-box">
                                            <input type="text" class="single-input" placeholder="Search....">
                                            <button class="src-btn src-btn-blog"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               </div>
            <!--404 area end-->
@endsection