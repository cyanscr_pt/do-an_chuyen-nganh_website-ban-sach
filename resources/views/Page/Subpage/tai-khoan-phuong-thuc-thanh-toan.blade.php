<div role="tabpanel" class="tab-pane fade in" id="markenting">
        <div class="col-xs-12">
         <div class="input-box mb-20">
             <label>Card Type <em>*</em></label>
             <select class="selectpicker select-custom" data-live-search="true">
               <option data-tokens="paypal">Paypal</option>
               <option data-tokens="visa">visa</option>
               <option data-tokens="master-card">master-card</option>
               <option data-tokens="discover">discover</option>
               <option data-tokens="payneor">payneor</option>
               <option data-tokens="skrill">skrill</option>
             </select>
         </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
             <div class="input-box mb-20">
                 <label>Card Number<em>*</em></label>
                 <input type="text" name="email" class="info" placeholder="Card Number">
             </div>
             </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
             <div class="input-box mb-20">
                 <label>Security Code<em>*</em></label>
                 <input type="text" name="phone" class="info" placeholder="Security Code">
             </div>
        </div>
        
           
        <div class="col-md-6 col-sm-6 col-xs-12">
         <div class="input-box mb-20">
             <label>Month <em>*</em></label>
             <select class="selectpicker select-custom" data-live-search="true">
               <option data-tokens="Januray">Januray</option>
               <option data-tokens="February">February</option>
               <option data-tokens="March">March</option>
               <option data-tokens="April">April</option>
               <option data-tokens="May">May</option>
               <option data-tokens="June">June</option>
               <option data-tokens="July">July</option>
               <option data-tokens="August">August</option>
               <option data-tokens="September">September</option>
               <option data-tokens="Ocotober">Ocotober</option>
               <option data-tokens="November">November</option>
               <option data-tokens="December">December</option>
             </select>
         </div>
        </div>
        
        <div class="col-md-6 col-sm-6 col-xs-12">
         <div class="input-box mb-20">
             <label>Year<em>*</em></label>
             <select class="selectpicker select-custom" data-live-search="true">
               <option data-tokens="2016">2016</option>
               <option data-tokens="2017">2017</option>
               <option data-tokens="2018">2018</option>
               <option data-tokens="2019">2019</option>
               <option data-tokens="2020">2020</option>
               <option data-tokens="2021">2021</option>
               <option data-tokens="2022">2022</option>
               <option data-tokens="2023">2023</option>
               <option data-tokens="2024">2024</option>
               <option data-tokens="2025">2025</option>
             </select>
         </div>
        </div>
        <div class="payment-btn-area mt-20">
         <div class="col-md-4 col-sm-4 col-xs-12 text-left">
             <a class="btn-def btn2" href="#">Pay Now</a>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12 text-center">
             <a class="btn-def btn2" href="#">Cancel Order</a>
         </div>
         <div class="col-md-4 col-sm-4 col-xs-12 text-right">
             <a class="btn-def btn2" href="#">Continue</a>
         </div>
 </div>