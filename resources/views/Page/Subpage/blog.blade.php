@extends('Page.master')
@section('content-container')
    
        <!--blog main area are start-->
        <div class="shop-main-area pt-70 pb-40">
                <div class="container">
                    <div class="row">                    
                        <!--main-shop-product start-->
                        <div class="col-md-9 col-sm-8 col-xs-12">
                           <div class="row">

                            @foreach ($Blogs as $blog)
                            <!-- a Blog preview start -->
                            <div class="blog-wraper">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                    <div class="single-blog sb-2 mb-30">
                                        <div class="blog-img pos-rltv product-overlay">
                                            <a href="{{route('tin',$blog->id)}}"><img src="/upload/image_tin/{{$blog->urlHinh}}" alt=""></a>
                                        </div>

                                        <div class="blog-content">
                                            <div class="blog-title">
                                                <h5 class="uppercase font-bold"><a href="{{route('tin',$blog->id)}}">{{$blog->TenTin}}</a></h5>
                                                <div class="like-comments-date">
                                                    <ul>
                                                        <li><a href="#"><i class="zmdi zmdi-favorite-outline"></i>13 Like</a></li>
                                                        <li><a href="#"><i class="zmdi zmdi-comment-outline"></i>03 Comments</a></li>
                                                        <li class="blog-date"><a href="#"><i class="zmdi zmdi-calendar-alt"></i>{{$blog->created_at}}</a></li>
                                                    </ul>
                                                </div>

                                                <div class="blog-text">
                                                    <p>{!! Str::words($blog->NoiDungTin, 50,'.... &raquo') !!}</p>
                                                </div> <a class="read-more montserrat" href="{{route('tin',$blog->id)}}">đọc thêm</a>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- a Blog preview end -->
                            @endforeach 

                            </div>
                        </div>
                        <!--main-shop-product start-->
                        
                    </div>
                </div>
            </div>
            <!--blog main area are end-->
@endsection