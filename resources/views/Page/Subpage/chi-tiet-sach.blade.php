@extends('Page.master')
@section('content-container')
    
        <!--single-protfolio-area are start-->
        <div class="single-protfolio-area ptb-70">
                <div class="container">
                    <div class="row">
                          <div class="col-md-7 col-sm-12 col-xs-12">
                             <div class="portfolio-thumbnil-area">
                              {{-- <div class="product-more-views">
                                  <div class="tab_thumbnail" data-tabs="tabs">
                                        <div class="thumbnail-carousel">
                                          <ul>
                                             <li class="active">
                                              <a href="#view11" class="shadow-box" aria-controls="view11" data-toggle="tab"><img src="images/product/01.jpg" alt="" /></a></li>
                                             <li>
                                              <a href="#view22" class="shadow-box" aria-controls="view22" data-toggle="tab"><img src="images/product/02.jpg" alt="" /></a></li>
                                             <li>
                                              <a href="#view33" class="shadow-box" aria-controls="view33" data-toggle="tab"><img src="images/product/03.jpg" alt="" /></a></li>
                                             <li>
                                              <a href="#view44" class="shadow-box" aria-controls="view44" data-toggle="tab"><img src="images/product/04.jpg" alt="" /></a></li>
                                          </ul>
                                      </div>
                                  </div>
                              </div> --}}
                              <div class="tab-content active-portfolio-area pos-rltv">
                                  <div role="tabpanel" class="tab-pane active" id="view11">
                                      <div class="product-img">
                                          <a class="fancybox" data-fancybox-group="group" href="upload/biasach/{{$sach->urlHinh}}"><img src="upload/biasach/{{$sach->urlHinh}}" alt="Single portfolio" /></a>
                                      </div>
                                  </div>
                                  <div role="tabpanel" class="tab-pane" id="view22">
                                      <div class="product-img">
                                          <a class="fancybox" data-fancybox-group="group" href="images/product/02.jpg"><img src="images/product/02.jpg" alt="Single portfolio" /></a>
                                      </div>
                                  </div>
                                  <div role="tabpanel" class="tab-pane" id="view33">
                                      <div class="product-img">
                                          <a class="fancybox" data-fancybox-group="group" href="images/product/03.jpg"><img src="images/product/03.jpg" alt="Single portfolio" /></a>
                                      </div>
                                  </div>
                                  <div role="tabpanel" class="tab-pane" id="view44">
                                      <div class="product-img">
                                          <a class="fancybox" data-fancybox-group="group" href="images/product/04.jpg"><img src="images/product/04.jpg" alt="Single portfolio" /></a>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </div>
                          <div class="col-md-5 col-sm-12 col-xs-12">
                              <div class="single-product-description">
                                 <div class="sp-top-des">
                                 <h3>{{$sach->TenSach}}<span>
                                     @foreach ($sach->TacGia as $tacgia)
                                         {{$tacgia->tenTG}}
                                     @endforeach</span></h3>
                                      <div class="prodcut-ratting-price">
                                          <div class="prodcut-ratting"> 
                                              <a href="#" tabindex="0"><i class="fa fa-star-o"></i></a> 
                                              <a href="#" tabindex="0"><i class="fa fa-star-o"></i></a> 
                                              <a href="#" tabindex="0"><i class="fa fa-star-o"></i></a> 
                                              <a href="#" tabindex="0"><i class="fa fa-star-o"></i></a> 
                                              <a href="#" tabindex="0"><i class="fa fa-star-o"></i></a> 
                                          </div>
                                          <div class="prodcut-price">
                                              @if ($sach->GiaSale > 0)
                                              <div class="new-price"> {{number_format($sach->GiaSale,0, ",",".")}} vnđ</div>
                                              <div class="old-price"> <del>{{number_format($sach->Gia,0, ",",".")}}</del> </div>
                                              @else
                                              <div class="new-price"> {{number_format($sach->Gia,0, ",",".")}} vnđ</div>
                                              @endif
                                          </div>
                                      </div>
                                  </div>
                                  
                                  <div class="sp-des">
<p>    
DỊCH VỤ & KHUYẾN MÃI LIÊN QUAN<br>
- Hoàn tiền cho thành viên BookStoreNOW (tối đa 100k/tháng), 1% (590đ) - Xem chi tiết <br>
- Mở thẻ HSBC - nhận ngay BookStore E-voucher lên đến 3 triệu đồng - Đăng ký ngay tại đây <a>Chi tiết</a><br>
- Thẻ SHB - Giảm 100k cho đơn hàng từ 500k - Thứ 3 hằng tuần (05/11/2019 - 10/03/2020) <a>Chi tiết</a><br>
- Đăng ký dịch vụ BookCare để được bọc plastic đến 99% sách tại BookStore
</p>
                                  </div>
                                  <div class="sp-bottom-des">
                                  <div class="single-product-option">
                                      
                                  </div>
                                  <div class="quantity-area">
                                      <label>Qty :</label>
                                      <div class="cart-quantity">
                                          <form action="#" method="POST" id="myform">
                                              <div class="product-qty">
                                                  <div class="cart-quantity">
                                                      <div class="cart-plus-minus">
                                                          <div class="dec qtybutton">-</div>
                                                              <input type="text" value="01" name="qtybutton" class="cart-plus-minus-box">
                                                          <div class="inc qtybutton">+</div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </form>
                                      </div>
                                  </div>
                                  <div class="social-icon socile-icon-style-1">
                                        <ul>
                                            <li><a href="#" data-tooltip="Add To Cart" class="add-cart add-cart-text" data-placement="left" tabindex="0">Add To Cart<i class="fa fa-cart-plus"></i></a></li>
                                            <li><a href="
                                                @if(!Auth::check())
                                                login
                                            @else
                                                xu-ly-thich/{{$sach->id}}
                                            @endif
                                                " data-tooltip="Wishlist" class="w-list" tabindex="0"><i class="fa fa-heart-o"></i></a></li>
                                            <div class="social-tag">
                                                <a href="#"><i class="zmdi zmdi-share"></i></a>
                                            </div>
                                        </ul>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>  
              </div>
              </div>
              <!--single-protfolio-area are start-->
              
              <!--descripton-area start -->
              <div class="descripton-area">
                  <div class="container">
                      <div class="row">
                          <div class="product-area tab-cars-style">
                              <div class="title-tab-product-category">
                                  <div class="col-md-12 text-center">
                                      <ul class="nav mb-40 heading-style-2" role="tablist">
                                          <li role="presentation" class="active"><a href="#mota" aria-controls="mota" role="tab" data-toggle="tab">Mô tả</a></li>
                                          <li role="presentation"><a href="#binhluan" aria-controls="binhlan" role="tab" data-toggle="tab">Bình luận</a></li>
                                          <li role="presentation"><a href="#tags" aria-controls="tags" role="tab" data-toggle="tab">Tags</a></li>
                                      </ul>
                                  </div>
                              </div>
                              <div class="clearfix"></div>
                              <div class="col-sm-12">
                                  <div class="content-tab-product-category">
                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                      <div role="tabpanel" class="tab-pane fix fade in active" id="mota">
                                        <div class="review-wraper">
                                            {!!$sach->GioiThieu!!}
                                        </div>
                                      </div>
                                      <div role="tabpanel" class="tab-pane fix fade in" id="binhluan">
                                        <div class="review-wraper">
                                            @comments([
                                                'model' => $sach,
                                                'approved' => true
                                            ])
                                        </div>
                                      </div>
                                      <div role="tabpanel" class="tab-pane fix fade in" id="tags">
                                          <ul class="tag-filter">
                                              <li><a href="#">Sách</a></li>
                                          </ul>
                                      </div>
                                  </div>
                              </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>    
              <!--descripton-area end--> 
              
            @include('Page.Subpage.block-san-pham-lien-quan')
@endsection