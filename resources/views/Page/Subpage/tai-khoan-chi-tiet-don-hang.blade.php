@extends('Page.master')
@section('content-container')

<!--service idea area are start -->
<div class="idea-area  ptb-80">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <h5>Chi tiết đơn hàng ID: {{$id}}</h5>
                <h6>Trạng thái đơn hàng:
                @switch($thongtindonhang["TrangThai"])
                    @case(0)
                        Đơn hàng đang chờ xử lý
                        @break
                    @case(1)
                        Đơn hàng đang được chuẩn bị
                        @break
                    @case(2)
                        Đơn hàng đang được vận chuyển
                        @break
                    @case(3)
                        Đơn hàng hoàn tất
                        @break
                    @case(4)
                        Đơn hàng bị huỷ
                        @break
                    @default
                        @break
                @endswitch
                </h6>
                <h6>Người nhận: {{$thongtindonhang["TenNguoiNhan"]}}</h6>
                <h6>SĐT người nhận: {{$thongtindonhang["SDT"]}}</h6>
                <h6>Địa chỉ giao hàng: {{$thongtindonhang["DiaChiNhanHang"]}}</h6>
                <h6>Ghi chú: {{$thongtindonhang["GhiChu"]}}</h6>
            </div>
            <div class="col-md-9 col-sm-8 col-xs-12">
                <!-- Chi tiết đơn hàng-->
                <form action="huyDonHang" method="post" id="form-don-hang">
                    {{ csrf_field() }}
                    <input type="hidden" name="id" value="{{$id}}">
                    <table class="table table-striped table-bordered table-hover">
                        <thead>
                            <tr align="center">
                                <th>Tên sách</th>
                                <th>Số lượng</th>
                                <th>Giá</th>
                                <th>Tổng Tiền</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($donhang['chitiet'] as $item)
                            <tr>
                                <td> {{$item['sach']['TenSach']}} </td>
                                <td> {{$item['SoLuong']}} </td>
                                <td> {{number_format($item['Gia'],0, ",",".")}} vnđ</td>
                                <td> {{number_format($item['Gia'] * $item['SoLuong'],0, ",",".")}} vnđ</td>
                            </tr>
                            @endforeach
                            @if ($thongtindonhang["TrangThai"] == 0 || $thongtindonhang["TrangThai"] == 1)
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td align="center"> <a class="btn-def btn2 danger"
                                    onclick="document.getElementById('form-don-hang').submit()">Huỷ đơn hàng</a>
                                </td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>
            <!--service idea area are end -->

            @endsection

            @section('script')
            <!-- DataTables -->
            <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
            @endsection
