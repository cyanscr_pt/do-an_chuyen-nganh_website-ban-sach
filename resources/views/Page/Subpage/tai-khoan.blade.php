@extends('Page.master')
@section('content-container')
    
        <!--service idea area are start -->
        <div class="idea-area  ptb-80">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-4 col-xs-12">
                        <div class="idea-tab-menu">
                            <ul class="nav idea-tab" role="tablist">
                                <li role="presentation" class="active"><a href="#creativity" aria-controls="creativity" role="tab" data-toggle="tab">Thông tin cá nhân</a></li>
                                {{-- <li role="presentation"><a href="#ideas" aria-controls="ideas" role="tab" data-toggle="tab">Địa chỉ nhận hàng</a></li> --}}
                                {{-- <li role="presentation"><a href="#design" aria-controls="design" role="tab" data-toggle="tab">Chi tiết thanh toán</a></li> --}}
                                <li role="presentation"><a href="#devlopment" aria-controls="devlopment" role="tab" data-toggle="tab">Đơn hàng của tôi</a></li>
                                {{-- <li role="presentation"><a href="#markenting" aria-controls="markenting" role="tab" data-toggle="tab">Phương thức thanh toán</a></li> --}}
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-9 col-sm-8 col-xs-12">
                        <div class="idea-tab-content">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                @include('Page.Subpage.tai-khoan-thong-tin-ca-nhan')
                                {{-- @include('Page.Subpage.tai-khoan-dia-chi-nhan-hang') --}}
                                {{-- @include('Page.Subpage.tai-khoan-chi-tiet-thanh-toan') --}}
                                @include('Page.Subpage.tai-khoan-don-hang')
                                {{-- @include('Page.Subpage.tai-khoan-phuong-thuc-thanh-toan') --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--service idea area are end -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
@endsection