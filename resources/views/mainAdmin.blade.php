<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="">

    <title>Admin - Books Store</title>
    <base href="{{asset('')}}">

    <!-- Bootstrap Core CSS -->
    <link href="admin/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="admin/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="admin/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="admin/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- DataTables CSS -->
    <link href="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css"
        rel="stylesheet">

    <link href="admin/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">


    <!-- Custom CSS -->


    <!-- Morris Charts CSS -->
    <link href="admin/css/plugins/morris.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="admin/bower_components/datatables-responsive/css/dataTables.responsive.css" rel="stylesheet">
    <script type="text/javascript" language="javascript" src="admin/ckeditor/ckeditor.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script> -->
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Admin - Books Store</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        {{ Auth::guard('QuanTri')->user()->TenNV }}
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>

                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="logout-add"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <!-- Menu -->
                @include('AdPage.menu')
                <!-- Menu -->

            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Main -->
                @yield('AdMain')
                <!-- End Main -->

                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <!-- <script src="admin/bower_components/jquery/dist/jquery.min.js"></script> -->

    <!-- Bootstrap Core JavaScript -->
    <script src="admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="admin/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="admin/dist/js/sb-admin-2.js"></script>

    <!-- DataTables JavaScript -->
    <script src="admin/bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="admin/bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js">
    </script>


    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>
    <script>
        $(document).ready(function () {
            $('#dataTables-example').DataTable({
                responsive: true
            });
        });

    </script>


    {{-- Ajax Thêm tac giả trong thêm sách --}}
    <script>
        $(document).ready(function () {
            $("#themtg").click(function () {
                var tentg = $(this).parents("div").find("#tentacgianew").val(); //lay ten tac gia
                //   alert(tentg);
                $.get("admin/sach/xuLyAddTgInSach/" + tentg, function (data) {
                    if (data == 1) {
                        alert('Đã thêm tác giả');
                        location.href = "admin/sach/add";
                    } else {
                        alert('Chưa thêm được tác giả');
                        location.href = "admin/sach/add";
                    }
                });
            });

        });

    </script>
    {{-- Ajax Thêm tac giả trong thêm sách --}}
    {{-- Ajax thêm thể loại trong thêm sách --}}
    <script>
        $(document).ready(function () {
            $("#themtl").click(function () {
                var tentl = $(this).parents("div").find("#tentheloainew").val(); //lay ten the loai
                //   alert(tentg);
                $.get("admin/sach/xuLyAddTlInSach/" + tentl, function (data) {
                    if (data == 1) {
                        alert('Đã thêm thể loại');
                        location.href = "admin/sach/add";
                    } else {
                        alert('Chưa thêm được thể loại');
                        location.href = "admin/sach/add";
                    }
                });
            });

        });

    </script>
    {{-- Ajax thêm thể loại trong thêm sách --}}

    {{-- Ajax thêm nhà xuất bản trong thêm sách --}}
    <script>
        $(document).ready(function () {
            $("#themnxb").click(function () {
                var tennxb = $(this).parents("div").find("#tennxbnew").val(); //lay ten the loai
                //   alert(tentg);
                $.get("admin/sach/xuLyAddNxbInSach/" + tennxb, function (data) {
                    if (data == 1) {
                        alert('Đã thêm nhà xuất bản');
                        location.href = "admin/sach/add";
                    } else {
                        alert('Chưa thêm được nhà xuất bản');
                        location.href = "admin/sach/add";
                    }
                });
            });

        });

    </script>
    {{-- Ajax thêm nhà xuất bản trong thêm sách --}}

    @yield('script')

</body>

</html>
