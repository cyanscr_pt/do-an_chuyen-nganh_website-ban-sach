<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route trang chủ
Route::get('/','frontendController@getIndex');

//Route trang chủ
Route::get('trang-chu','frontendController@getIndex');

//Route Trang tất cả sản phẩm
Route::get('tat-ca-sach','frontendController@getPageTatCaSach');

//Route Chi tiết 1 sách
Route::get('chi-tiet/{id}','frontendController@getChiTietSach')->name('chi-tiet');

//Router Trang Blog - Tất cả blog - Tất cả tin tức
Route::get('tin-tuc','frontendController@getAllBlog');

//Router Trang Blog - Chi tiết 1 blog - chi tiết Tin
Route::get('/tin/{id}','frontendController@getBlogDetail')->name('tin');

//Route Trang báo lỗi 404
Route::get('404','frontendController@get404');

//Route Trang Liên hệ
Route::get('lien-he','frontendController@getPageLienHe');

//Route Trang About

//Route Trang login
Route::get('login','frontendController@getPageLogin')->name('login');
Route::post('login','frontendController@postPageLogin')->name('login');
//Route Trang Đăng kí
Route::get('dang-ki','frontendController@getPageDangKi');
Route::post('dang-ki','frontendController@postPageDangKi');
//Route Đăng xuất
Route::get('logout','frontendController@getDangxuat');

//Route Trang giỏ hàng
Route::get('gio-hang','frontendController@getPageGioHang')->name('gio-hang');

//Thêm vào giỏ hàng
Route::get('add-to-cart/{id}','frontendController@getAddToCart')->name('themvaogiohang');

//xu ly thich sann pham
Route::get('xu-ly-thich/{id}','frontendController@xulylike');


//Cập nhật giỏ hàng
Route::get('update-to-cart/{id}/{Qty}','frontendController@getUpdatetocart');
//Thêm số lượng nhiều vào giỏ hàng
Route::get('add-amount-to-cart/{id}/{amount}','frontendController@getAddAmountToCart')->name('themnhieuvaogiohang');
//Bỏ ra khỏi giỏ hàng
Route::get('remove-from-card/{id}','frontendController@getRemoveFromCart')->name('borakhoigiohang');
//Xoá ra khỏi giỏ hàng
Route::get('delete-from-card/{id}','frontendController@getDeleteFromCart')->name('xoakhoigiohang');

//Đặt hàng
Route::get('checkout','frontendController@getCheckout')->name('checkout');
//Đặt hàng
Route::post('checkout','frontendController@postCheckout')->name('checkout');
//Xác nhận đặt hàng
Route::get('complete-order','frontendController@getCompleteOrder')->name('complete-order');
Route::post('complete-order','frontendController@postCompleteOrder')->name('complete-order');

//Route Tìm kiếm
Route::get('search','frontendController@getSearch');

//Route view Nhà xuất bản
 Route::get('view-NXB/{id}','frontendController@getviewNXB');
 
//sach theo the loai
Route::get('the-loai/{id}','frontendController@getSachofTL');

//Route Trang tài khoản
Route::get('tai-khoan','frontendController@getPageTaiKhoan')->name('tai-khoan');
Route::post('updateThongTin','frontendController@postPageTaiKhoan')->name('updateThongTin');
//Route Chi tiết đơn hàng
Route::get('chi-tiet-don-hang/{id}','frontendController@getChiTietDonHang')->name('chi-tiet-don-hang');
//Route Trang tài khoản - Huỷ đơn hàng
Route::post('huyDonHang','frontendController@postHuyDonHang')->name('huyDonHang');