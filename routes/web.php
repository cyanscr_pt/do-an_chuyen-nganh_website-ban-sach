<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get("test","AdPageController@test");

Route::get("login-admin","LoginAdController@loginAd");
Route::post("login-admin","LoginAdController@postloginAd");
Route::get('logout-add',"LoginAdController@getlogOutAd");

Route::group(["prefix"=>"admin","middleware"=>"CheckLoginAd"],function(){

// index
	Route::group(["prefix"=>"sach","middleware"=>"checkloginadqt"], function(){
		Route::get('danh-sach','SachController@getDsSach');

		Route::get('add','SachController@getAddSach');
		Route::post('add','SachController@postAddSach');

		Route::get('xuLyAddTgInSach/{tentg}','SachController@addTg');
		Route::get('xuLyAddTlInSach/{tentl}','SachController@addTl');
		Route::get('xuLyAddNxbInSach/{tennxb}','SachController@addNxb');

		Route::get('update/{id}','SachController@getUpdateSach');
		Route::post('update/{id}','SachController@postUpdateSach');

		Route::get("delete/{id}","SachController@getXoaSach");
	});
	// SÁCH

	Route::group(["prefix"=>"the-loai","middleware"=>"checkloginadqt"], function(){
		Route::get('danh-sach','AdPageController@getDsLoai');

		Route::get('add','AdPageController@getAddLoai');
		Route::post('add','AdPageController@postAddLoai');

		Route::get('update/{id}','AdPageController@getUpdateLoai');
		Route::post('update/{id}','AdPageController@postUpdateLoai');

		Route::get("delete/{id}","AdPageController@getXoaLoai");
	});
	//THỂ LOẠI

	Route::group(["prefix"=>"tac-gia","middleware"=>"checkloginadqt"], function(){
		Route::get('danh-sach','AdPageController@getDsTacGia');

		Route::get('add','AdPageController@getAddTG');
		Route::post('add','AdPageController@postAddTG');

		Route::get('update/{id}','AdPageController@getUpdateTG');
		Route::post('update/{id}','AdPageController@postUpdateTG');

		Route::get('delete/{id}','AdPageController@getDeleteTG');
	});
	//TÁC GIẢ

	Route::group(["prefix"=>"nha-xuat-ban","middleware"=>"checkloginadqt"], function(){
		Route::get('danh-sach','AdPageController@getDsNhaXuatBan');

		Route::get("add","AdPageController@getAddNhaXuatBan");
		Route::post("add","AdPageController@postAddNhaXuatBan");

		Route::get("update/{id}","AdPageController@getUpdateNhaXuatBan");
		Route::post("update/{id}","AdPageController@postUpdateNhaXuatBan");

		Route::get("delete/{id}","AdPageController@getDeleteNhaXuatBan");
	//end Nhà Xuất Bản
	});
	//NHÀ XUẤT BẢN
	Route::group(["prefix"=>"don-hang","middleware"=>"checkloginadqt"], function(){
		Route::get('danh-sach','DonHangController@getDonHangDs');
		Route::get('xu-ly/{id}/{idnv}','DonHangController@getXuLy')->where(['id' => '[0-9]+', 'idnv' => '[0-9]+']);
		Route::get('xu-ly-huy/{id}','DonHangController@getXuLyHuy');

		// Route::get('xu-ly-huy/{id}','DonHangController@getDonHangDs');

	});
	//ĐƠN HÀNG
	Route::group(["prefix"=>"khuyen-mai","middleware"=>"checkloginadqt"], function(){
		Route::get("danh-sach","TinKhuyenMaiController@getKhuyenMaiDs");

		Route::get("add","TinKhuyenMaiController@getAddKm");
		Route::post("add","TinKhuyenMaiController@postAddKm");

		Route::get("update/{id}","TinKhuyenMaiController@getUpdateKm");
		Route::post("update/{id}","TinKhuyenMaiController@postUpdateKm");

	});
	//KHUYẾN MÃI
	
	Route::group(["prefix"=>"tin","middleware"=>"checkloginadqt"], function(){
		Route::get("danh-sach","TinKhuyenMaiController@getDsTin");

		Route::get('add','TinKhuyenMaiController@getAddTin');
		Route::post('add','TinKhuyenMaiController@postAddTin');

		Route::get('update/{id}','TinKhuyenMaiController@getUpdateTin');
		Route::post('update/{id}','TinKhuyenMaiController@postUpdateTin');

		Route::get('xuly/{id}','TinKhuyenMaiController@getxuly');

	});
	//TIN
	Route::group(["prefix"=>"binh-luan","middleware"=>"checkloginadqt"], function(){
		Route::get('danh-sach','AdPageController@getBinhLuan');
		Route::get('xu-ly/{id}','AdPageController@getXuLy');

	});
	//BÌNH LUẬN

	Route::group(["prefix"=>"nhan-vien","middleware"=>"checkloginadmain"], function(){
		Route::get('danh-sach','NVController@getDsNhanVien');

		Route::get('xuly/{id}','NVController@xuly');

		Route::get('add','NVController@getaddNV');
		Route::post('add','NVController@postaddNV');

	});
	//Nhan Vien
	Route::group(["prefix"=>"user","middleware"=>"checkloginadmain"], function(){
		Route::get('getdsuser','UserController@getDsUser');
		Route::get('xulyUser/{id}','UserController@xulyUser');
	});
	

	//User
	Route::get("index","pageController@getIndex");
	//end index

	Route::get('thong-ke','AdPageController@getThongKe');
	//Thong ke
	

	
});
