###### loc's memo  

# Trang frontend  
resources >> views >> Page >> Subpage

Page.Subpage.*  
	header.blade.php thành phần trang header chứa navbar và các thanh phần khác của header  
	footer.blade.php thành phần trang footer chứa các thành phần ở footer  
	trang-chu.blade.php trang chứa body nội dung trang index  
	shop.blade.php trang tất cả sách  

# Route  
resources >> routes >> frontend

# Controller
app >> Http >> Controllers >> frontendControllers
