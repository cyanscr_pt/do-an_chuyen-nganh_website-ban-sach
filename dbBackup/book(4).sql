-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th12 14, 2019 lúc 05:25 AM
-- Phiên bản máy phục vụ: 5.7.26-0ubuntu0.18.04.1-log
-- Phiên bản PHP: 7.3.6-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `book`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `binhluan`
--

CREATE TABLE `binhluan` (
  `id` int(11) NOT NULL,
  `id_Sach` int(11) NOT NULL,
  `id_User` int(10) NOT NULL,
  `id_NV` int(11) DEFAULT '1',
  `NoiDung` text COLLATE utf8_unicode_ci NOT NULL,
  `Ngay` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TrangThai` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitietdonhang`
--

CREATE TABLE `chitietdonhang` (
  `id_DH` int(11) DEFAULT NULL,
  `id_Sach` int(11) DEFAULT NULL,
  `SoLuong` tinyint(11) DEFAULT NULL,
  `Gia` int(11) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chitietdonhang`
--

INSERT INTO `chitietdonhang` (`id_DH`, `id_Sach`, `SoLuong`, `Gia`, `updated_at`, `created_at`) VALUES
(1, 2, 2, 110000, '2019-12-05 09:46:29', '2019-12-05 09:46:29'),
(1, 13, 2, 30000, '2019-12-05 09:46:29', '2019-12-05 09:46:29'),
(2, 13, 2, 150000, '2019-12-05 09:46:29', '2019-12-05 09:46:29'),
(2, 12, 1, 85000, '2019-12-05 09:46:29', '2019-12-05 09:46:29'),
(4, 6, 3, 540000, '2019-12-05 16:46:42', '2019-12-05 16:46:42'),
(5, 6, 3, 540000, '2019-12-05 16:47:56', '2019-12-05 16:47:56'),
(6, 6, 3, 540000, '2019-12-05 16:49:54', '2019-12-05 16:49:54'),
(7, 4, 3, 195000, '2019-12-05 16:52:01', '2019-12-05 16:52:01'),
(7, 6, 1, 180000, '2019-12-05 16:52:01', '2019-12-05 16:52:01'),
(7, 9, 5, 850000, '2019-12-05 16:52:02', '2019-12-05 16:52:02'),
(8, 14, 1, 209000, '2019-12-05 20:11:25', '2019-12-05 20:11:25'),
(8, 13, 1, 150000, '2019-12-05 20:11:25', '2019-12-05 20:11:25'),
(8, 12, 1, 85000, '2019-12-05 20:11:25', '2019-12-05 20:11:25'),
(8, 15, 1, 95000, '2019-12-05 20:11:25', '2019-12-05 20:11:25'),
(8, 7, 1, 109000, '2019-12-05 20:11:25', '2019-12-05 20:11:25'),
(9, 1, 1, 110000, '2019-12-06 11:02:40', '2019-12-06 11:02:40'),
(9, 4, 1, 65000, '2019-12-06 11:02:40', '2019-12-06 11:02:40'),
(10, 14, 1, 209000, '2019-12-06 11:06:26', '2019-12-06 11:06:26'),
(11, 14, 1, 209000, '2019-12-08 14:19:00', '2019-12-08 14:19:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitietkhuyenmai`
--

CREATE TABLE `chitietkhuyenmai` (
  `id_Sach` int(11) NOT NULL,
  `id_KM` int(11) NOT NULL,
  `SoLuongKM` int(11) NOT NULL,
  `SoLuongConLai` int(11) DEFAULT NULL,
  `DieuKien` text COLLATE utf8_unicode_ci,
  `PhanTramKM` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chitietkhuyenmai`
--

INSERT INTO `chitietkhuyenmai` (`id_Sach`, `id_KM`, `SoLuongKM`, `SoLuongConLai`, `DieuKien`, `PhanTramKM`, `updated_at`, `created_at`) VALUES
(1, 8, 100, 100, '<p>abc abc</p>', 90, '2019-12-12 13:58:17', '2019-12-12 13:58:17'),
(2, 8, 100, 100, '<p>abc abc</p>', 90, '2019-12-12 13:58:17', '2019-12-12 13:58:17'),
(4, 8, 100, 100, '<p>abc abc</p>', 90, '2019-12-12 13:58:18', '2019-12-12 13:58:18'),
(1, 9, 100, 100, '<p>abc abc</p>', 90, '2019-12-12 13:59:18', '2019-12-12 13:59:18'),
(2, 9, 100, 100, '<p>abc abc</p>', 90, '2019-12-12 13:59:18', '2019-12-12 13:59:18'),
(4, 9, 100, 100, '<p>abc abc</p>', 90, '2019-12-12 13:59:18', '2019-12-12 13:59:18'),
(1, 10, 100, 100, '<p>plpa opllplpplp lub lub</p>', 90, '2019-12-13 19:10:49', '2019-12-13 19:10:49'),
(1, 11, 100, 100, '<p>plpa opllplpplp lub lub</p>', 90, '2019-12-13 19:12:22', '2019-12-13 19:12:22'),
(1, 12, 100, 100, '<p>plpa opllplpplp lub lub</p>', 90, '2019-12-13 19:13:32', '2019-12-13 19:13:32'),
(1, 13, 100, 100, '<p>them lai di cu</p>', 90, '2019-12-13 19:14:47', '2019-12-13 19:14:47'),
(1, 14, 100, 100, '<p>them lai di cu</p>', 90, '2019-12-13 19:15:26', '2019-12-13 19:15:26'),
(20, 15, 2, 2, '<p>khuyen culox</p>', 10, '2019-12-14 12:01:41', '2019-12-14 12:01:41'),
(21, 15, 2, 2, '<p>khuyen culox</p>', 10, '2019-12-14 12:01:41', '2019-12-14 12:01:41'),
(22, 15, 2, 2, '<p>khuyen culox</p>', 10, '2019-12-14 12:01:42', '2019-12-14 12:01:42'),
(20, 16, 2, 2, '<p>khuyen culox</p>', 10, '2019-12-14 12:02:13', '2019-12-14 12:02:13'),
(21, 16, 2, 2, '<p>khuyen culox</p>', 10, '2019-12-14 12:02:13', '2019-12-14 12:02:13'),
(22, 16, 2, 2, '<p>khuyen culox</p>', 10, '2019-12-14 12:02:13', '2019-12-14 12:02:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitietloai`
--

CREATE TABLE `chitietloai` (
  `id_Loai` int(11) NOT NULL,
  `id_Sach` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chitietloai`
--

INSERT INTO `chitietloai` (`id_Loai`, `id_Sach`, `updated_at`, `created_at`) VALUES
(2, 5, '2019-11-27 17:05:27', '2019-11-27 17:05:27'),
(2, 2, '2019-11-27 21:50:24', '2019-11-27 21:50:24'),
(5, 4, '2019-11-27 21:56:41', '2019-11-27 21:56:41'),
(7, 4, '2019-11-27 21:56:41', '2019-11-27 21:56:41'),
(3, 7, '2019-12-04 16:17:22', '2019-12-04 16:17:22'),
(3, 8, '2019-12-04 16:17:44', '2019-12-04 16:17:44'),
(3, 13, '2019-12-04 16:20:22', '2019-12-04 16:20:22'),
(5, 1, '2019-12-13 19:15:51', '2019-12-13 19:15:51'),
(2, 19, '2019-12-13 20:37:58', '2019-12-13 20:37:58'),
(3, 19, '2019-12-13 20:37:58', '2019-12-13 20:37:58');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `chitiettacgia`
--

CREATE TABLE `chitiettacgia` (
  `id_TG` int(11) NOT NULL,
  `id_Sach` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `chitiettacgia`
--

INSERT INTO `chitiettacgia` (`id_TG`, `id_Sach`, `updated_at`, `created_at`) VALUES
(4, 5, '2019-11-27 17:05:26', '2019-11-27 17:05:26'),
(5, 5, '2019-11-27 17:05:27', '2019-11-27 17:05:27'),
(12, 5, '2019-11-27 17:05:27', '2019-11-27 17:05:27'),
(3, 2, '2019-11-27 21:50:24', '2019-11-27 21:50:24'),
(10, 4, '2019-11-27 21:56:41', '2019-11-27 21:56:41'),
(11, 4, '2019-11-27 21:56:41', '2019-11-27 21:56:41'),
(12, 7, '2019-12-04 16:17:22', '2019-12-04 16:17:22'),
(12, 8, '2019-12-04 16:17:44', '2019-12-04 16:17:44'),
(13, 13, '2019-12-04 16:20:22', '2019-12-04 16:20:22'),
(3, 1, '2019-12-13 19:15:50', '2019-12-13 19:15:50'),
(14, 19, '2019-12-13 20:37:58', '2019-12-13 20:37:58'),
(15, 22, '2019-12-14 11:34:22', '2019-12-14 11:34:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `donhang`
--

CREATE TABLE `donhang` (
  `id` int(11) NOT NULL,
  `TenNguoiNhan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TrangThai` tinyint(1) NOT NULL,
  `NgayLap` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `TongTien` int(11) NOT NULL,
  `GhiChu` text COLLATE utf8_unicode_ci,
  `DiaChiNhanHang` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` int(11) NOT NULL,
  `id_User` int(11) NOT NULL,
  `id_NV` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `donhang`
--

INSERT INTO `donhang` (`id`, `TenNguoiNhan`, `TrangThai`, `NgayLap`, `TongTien`, `GhiChu`, `DiaChiNhanHang`, `SDT`, `id_User`, `id_NV`, `updated_at`, `created_at`) VALUES
(1, 'lox', 4, '2019-12-11 04:01:14', 140000, '123 <br/> Hủy: Không Giao Được Hàng', '1223', 258369, 2, 1, '2019-12-11 11:01:15', '2019-11-29 07:34:10'),
(2, 'hoang tesst', 5, '2019-12-07 05:36:56', 235000, 'test d <br/> Hủy: Không Giao Được Hàng', '1223/4', 988755656, 1, 1, '2019-12-07 12:36:57', '2019-12-03 17:00:00'),
(6, 'loc', 1, '2019-12-12 05:45:52', 540000, '123123', 'tnloc 123 sg', 906828619, 6, 1, '2019-12-12 12:45:52', '2019-12-05 16:49:54'),
(7, 'Hoàng DŨng', 0, '2019-12-07 05:26:52', 1225000, 'Hủy: Không Giao Được Hàng', '776ND SG', 9060102, 6, NULL, '2019-12-07 05:26:52', '2019-12-05 16:52:00'),
(8, 'tanloc', 0, '2019-12-07 05:26:54', 648000, 'hêloo giao pủi tối nka', '775/9 NF SG', 906123456, 6, NULL, '2019-12-07 05:26:54', '2019-12-05 20:11:24'),
(9, 'culoc', 0, '2019-12-07 05:26:56', 175000, '124543534345345 <br/> Hủy: Không Giao Được Hàng', '123 gghh', 12313, 6, NULL, '2019-12-07 05:26:56', '2019-12-06 11:02:39'),
(10, 'loc', 0, '2019-12-06 04:06:28', 209000, 'loc test null nv', '1123213pyh', 123456, 6, NULL, '2019-12-06 11:06:24', '2019-12-06 11:06:24'),
(11, 'loc', 0, '2019-12-08 07:19:00', 209000, 'ffsdfsdfsdf', '234312312', 11233123, 6, NULL, '2019-12-08 14:19:00', '2019-12-08 14:19:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `khuyenmai`
--

CREATE TABLE `khuyenmai` (
  `id` int(11) NOT NULL,
  `Ten_KM` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TrangThai` tinyint(2) NOT NULL,
  `NgayBatDau` date NOT NULL,
  `NgayKetThuc` date NOT NULL,
  `id_NV` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `khuyenmai`
--

INSERT INTO `khuyenmai` (`id`, `Ten_KM`, `TrangThai`, `NgayBatDau`, `NgayKetThuc`, `id_NV`, `updated_at`, `created_at`) VALUES
(5, 'culoxtet', 1, '1998-02-07', '2020-12-12', 1, '2019-11-30 19:25:54', '2019-11-30 19:25:54'),
(6, 'Lorem Ipsum', 1, '2019-12-01', '2019-12-31', 1, '2019-11-30 19:25:54', '2019-11-30 19:25:54'),
(7, 'sale Giáng Sinh', 1, '2019-12-12', '2019-12-30', 1, '2019-12-04 08:27:49', '2019-12-04 08:27:49'),
(8, 'KM 12 12 19', 1, '2019-12-12', '2019-12-24', 1, '2019-12-12 13:58:17', '2019-12-12 13:58:17'),
(9, 'KM 12 12 19', 1, '2019-12-12', '2019-12-24', 1, '2019-12-12 13:59:17', '2019-12-12 13:59:17'),
(10, 'test db km', 1, '2019-12-13', '2019-12-27', 1, '2019-12-13 19:10:48', '2019-12-13 19:10:48'),
(11, 'test db km', 1, '2019-12-13', '2019-12-27', 1, '2019-12-13 19:12:21', '2019-12-13 19:12:21'),
(12, 'test db km', 1, '2019-12-13', '2019-12-27', 1, '2019-12-13 19:13:31', '2019-12-13 19:13:31'),
(13, 'tesst db giasale', 1, '2019-12-13', '2019-12-30', 1, '2019-12-13 19:14:47', '2019-12-13 19:14:47'),
(14, 'tesst db giasale', 1, '2019-12-13', '2019-12-30', 1, '2019-12-13 19:15:26', '2019-12-13 19:15:26'),
(15, 'Khuyen mai 1412', 1, '2019-12-14', '2019-12-16', 1, '2019-12-14 12:01:41', '2019-12-14 12:01:41'),
(16, 'Khuyen mai 1412', 1, '2019-12-14', '2019-12-16', 1, '2019-12-14 12:02:13', '2019-12-14 12:02:13');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `like`
--

CREATE TABLE `like` (
  `id` int(11) NOT NULL,
  `id_Sach` int(11) NOT NULL,
  `id_User` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `like`
--

INSERT INTO `like` (`id`, `id_Sach`, `id_User`, `updated_at`, `created_at`) VALUES
(1, 7, 6, '2019-12-05 04:55:08', '2019-12-05 04:55:08'),
(6, 15, 1, '2019-12-05 05:01:56', '2019-12-05 05:01:56'),
(7, 14, 1, '2019-12-05 05:02:00', '2019-12-05 05:02:00'),
(8, 12, 6, '2019-12-05 18:14:47', '2019-12-05 18:14:47'),
(9, 1, 6, '2019-12-14 01:57:59', '2019-12-14 01:57:59');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_11_29_125012_create_user_activations_table', 2),
(5, '2019_11_29_125134_alter_users_table', 2);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhanvien`
--

CREATE TABLE `nhanvien` (
  `id` int(11) NOT NULL,
  `TenNV` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ChucVu` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` int(11) NOT NULL,
  `DiaChi` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TrangThai` tinyint(1) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nhanvien`
--

INSERT INTO `nhanvien` (`id`, `TenNV`, `password`, `ChucVu`, `SDT`, `DiaChi`, `TrangThai`, `email`, `updated_at`, `created_at`) VALUES
(1, 'lox', '$2y$10$iex38CqjqmaFCnKrjRW.T.5pJvLrhwqR4k0gme3IZHcQ69pNdCuJy', 'qtv', 22222, '123 duong 456', 1, 'hoangdung2k19@gmail.com', '2019-12-03 13:40:41', '2019-12-03 13:40:41'),
(2, 'hoangdung', '$2y$10$iex38CqjqmaFCnKrjRW.T.5pJvLrhwqR4k0gme3IZHcQ69pNdCuJy', 'qttk', 1555, '4455t', 1, 'tnydung@gmail.com', '2019-12-03 13:40:41', '2019-12-03 13:40:41'),
(3, 'Hoàng Dũng', '$2y$10$iex38CqjqmaFCnKrjRW.T.5pJvLrhwqR4k0gme3IZHcQ69pNdCuJy', 'qtv', 917733663, '14442', 1, 'hoang123@gmail.com', '2019-12-03 20:41:30', '2019-12-03 13:40:41');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `nhaxuatban`
--

CREATE TABLE `nhaxuatban` (
  `id` int(11) NOT NULL,
  `TenNXB` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `TenNXB_KhongDau` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `DiaChi` text COLLATE utf8_unicode_ci,
  `SDT` int(11) DEFAULT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `nhaxuatban`
--

INSERT INTO `nhaxuatban` (`id`, `TenNXB`, `TenNXB_KhongDau`, `DiaChi`, `SDT`, `logo`, `updated_at`, `created_at`) VALUES
(1, 'NXB Trẻ', 'NXB-Tre', 'asdfadsf', 556562222, '0yrP_top-7-thiet-ke-logo-nha-xuat-ban-sach-dac-sac-001.png', '2019-12-06 06:39:49', '2019-11-20 07:05:26'),
(2, ' NXB Văn Học ', 'NXB-Van-Hoc', NULL, NULL, 'lcOM_nha-xuat-ban-van-hoc-115190.jpg', '2019-12-06 06:40:07', '2019-11-20 07:05:26'),
(3, 'NXB Hội Nhà Văn', 'NXB-Hoi-Nha-Van', NULL, NULL, 'R3Mc_hoivanhoc.png', '2019-12-06 06:40:12', '2019-11-20 07:05:26'),
(4, 'NXB Kim Đồng', 'NXB-Kim-Dong', NULL, NULL, 'N5TK_20141104064637!Logo_nxb_Kim_Đồng.png', '2019-12-06 06:40:26', '2019-11-20 07:05:26'),
(5, 'NXB Thế Giới', 'NXB-The-Gioi', NULL, NULL, '0tHx_logo-nxbtg.jpg', '2019-12-06 06:40:35', '2019-11-20 07:05:26'),
(6, 'NXB Thời Đại', 'NXB-Thoi-Dai', NULL, NULL, NULL, '2019-11-20 07:05:26', '2019-11-20 07:05:26'),
(7, 'NXB Hồng Đức', 'NXB-Hong-Duc', NULL, NULL, NULL, '2019-11-20 07:05:26', '2019-11-20 07:05:26'),
(8, 'NXB Tổng hợp TP.Hồ Chí Minh', 'NXB-tong-hop-tp-ho-chi-minh', NULL, NULL, NULL, '2019-11-20 07:05:26', '2019-11-20 07:05:26'),
(9, 'NXB Lao động xã hội', 'NXB-lao-dong-xa-hoi', NULL, NULL, NULL, '2019-11-20 07:05:26', '2019-11-20 07:05:26'),
(12, 'Nhã Nam', 'nha-nam', '123 đường 456', 1234435, NULL, '2019-11-21 01:06:39', '2019-11-21 01:06:39'),
(13, 'NXB Lao Động', 'NXB-Lao-Dong', NULL, NULL, NULL, '2019-11-23 17:35:05', '2019-11-23 17:35:05'),
(14, 'NXB Tri Thức', 'NXB-tri-thuc', NULL, NULL, NULL, '2019-11-23 17:57:21', '2019-11-23 17:57:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sach`
--

CREATE TABLE `sach` (
  `id` int(11) NOT NULL,
  `TenSach` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TenSach_KhongDau` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `DichGia` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lang` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `KichThuoc` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SoTrang` int(11) DEFAULT NULL,
  `LoaiBia` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `SKU` int(11) DEFAULT NULL,
  `urlHinh` varchar(225) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Gia` int(11) NOT NULL,
  `TrangThai` tinyint(2) NOT NULL,
  `SoLuong` int(11) NOT NULL,
  `GioiThieu` text COLLATE utf8_unicode_ci,
  `id_NXB` int(11) NOT NULL,
  `GiaSale` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sach`
--

INSERT INTO `sach` (`id`, `TenSach`, `TenSach_KhongDau`, `DichGia`, `lang`, `KichThuoc`, `SoTrang`, `LoaiBia`, `SKU`, `urlHinh`, `Gia`, `TrangThai`, `SoLuong`, `GioiThieu`, `id_NXB`, `GiaSale`, `updated_at`, `created_at`) VALUES
(1, 'Điệp Viên Hoàn Hảo X6', 'Diep-vien-hoan-hao-x6-tai-ban', 'Không', 'vi', '16 x 24 cm', 392, 'Chọn loại bìa', 1123123, 'diep-vien-hoan-hao-x6-a_1_2_2.jpg', 110000, 1, 496, '<p>Phạm Xuân Ẩn, một cái tên chứa biết bao điều bí ẩn, không chỉ với người Việt Nam, mà đối với cả rất nhiều chính khách và báo chí thế giới, đặc biệt là báo chí Mỹ. Người ta đã bình luận, tranh cãi, phỏng đoán và viết rất nhiều về ông, thậm chí còn làm phim tài liệu nhiều tập về ông, nhưng dường như còn có quá nhiều điều vẫn chưa được làm sáng tỏ. Cho đến khi một người Mỹ bắt tay vào cuộc… Đó chính là Larry Berman, nhà sử học, giáo sư chính trị học thuộc đại học California, một chuyên gia xuất sắc chuyên nghiên cứu về cuộc chiến tranh xâm lược Việt Nam của Mỹ. Năm 2007, quyển sách Perfect Spy - The Incredible Double Life of PHAM XUAN AN Time Magazine Reporter &amp; Vietnamese Communist Agent (Điệp viên hoàn hảo – Hai cuộc đời khó tin (hay cuộc đời hai mặt theo cách gọi của Phương Tây về Phạm Xuân Ẩn, phóng viên tạp chí Reuters, Time và tướng tình báo Cộng sản Việt Nam)) của Larry Berman được xuất bản lần đầu tiên tại Mỹ đã gây chấn động dư luận Mỹ, kiều bào Việt Nam ở Mỹ và cả ở Việt Nam. Vì trước khi quyển sách này ra đời, rất ít người biết và hiểu về tướng tình báo Phạm Xuân Ẩn, mặc dù ông là một nhân vật huyền thoại của chiến tranh Việt Nam. Có lẽ bởi vì cuộc đời của Phạm Xuân Ẩn, cũng giống như tên của ông, chứa đựng nhiều bí ẩn về con người và sự nghiệp của một nhà tình báo vĩ đại, tài năng và bản lĩnh nhưng lại vô cùng khiêm nhường, bình dị.</p>\r\n\r\n<p>Phạm Xuân Ẩn (sinh ngày 12/9/1927, mất ngày 20/9/2006) tham gia hoạt động cách mạng từ đầu thập niên 1950, năm 1953 được kết nạp Đảng và được giao nhiệm vụ hoạt động điệp báo. Nhằm tạo vỏ bọc tốt hơn để có thể thâm nhập sâu hơn vào giới chức chính quyền và quân đội Sài Gòn, năm 1957 ông được cấp trên bố trí cho sang Mỹ học ngành báo chí. Năm 1959, Phạm Xuân Ẩn về nước, làm việc cho Hãng tin Reuters và sau đó là tạp chí hàng đầu Time, New York Herald Tribune của Mỹ. Với kiến thức uyên bác, hiểu biết rộng, cương trực và tài năng giao tiếp, ngoại giao khác biệt, độc đáo theo kiểu lãng tử, hào hoa, ngang tàng, “chửi thề như bắp rang”, xuất hiện với phong cách thượng lưu, thừa hưởng văn hóa được đào tạo chính qui từ Mỹ, ông đã thâm nhập và là bạn tri kỷ với các tướng lĩnh, trùm an ninh mật vụ cả Mỹ và Sài Gòn, giới báo chí Việt Nam Cộng Hòa và Mỹ cũng như các chính khách chóp bu của chính quyền Sài Gòn để khai thác những thông tin tuyệt mật mang tầm chiến lược cho cuộc đối đầu của Bắc Việt Nam với Mỹ và chính quyền Sài Gòn. Cuộc đời hoạt động tình báo đầy ly kỳ hấp dẫn nhưng cũng vô cùng hiểm nguy của Phạm Xuân Ẩn với bí danh X6 thuộc cụm tình báo H63 được thể hiện một cách trung thực, sinh động trong cuốn Điệp viên hoàn hảo X6 được bổ sung cập nhật mới.</p>\r\n\r\n<p>Sau 6 năm, cuốn sách với những thông tin có giá trị lịch sử giờ đây lại được ra mắt bạn đọc qua một bản dịch đầy đủ và cập nhật nhiều thông tin chưa từng được công bố do chính Phạm Xuân Ẩn tiết lộ với tác giả Larry Berman có ghi âm. Với lời hứa chỉ được công bố những thông tin này từng giai đoạn sau khi Phạm Xuân Ẩn mất, Larry Berman đã mất 6 năm để cân nhắc để từng bước công bố thông tin của người đã khuất.</p>', 7, 11000, '2019-12-13 19:15:50', '2019-11-24 22:42:37'),
(2, 'Khởi hành', 'khoi-hanh', NULL, 'vi', '14.5 x 20.5 cm', 2, 'Chọn loại bìa', NULL, 'khoihanh.jpg', 38400, 0, 200, '<p>Khởi Hành \"Trong thế giới toàn cầu hóa hiện nay, mức độ cạnh tranh càng lúc càng dữ dội. Sinh viên cần phải chủ động tìm hiểu và chuẩn bị những hành trang cần thiết vì nghề nghiệp và cuộc sống tương lai của chính mình. Bạn không thể phụ thuộc vào trường học, bạn không thể phụ thuộc vào xã hội. Bạn không thể chờ đợi và kỳ vọng vào người khác. Bạn chỉ có thể dựa vào chính bạn!\" (Giáo sư John Vu) Khởi hành là cuốn sách được hình thành từ những bài viết hướng dẫn cho sinh viên trên trang blog của Giáo sư John Vu (science-technology.vn). Giáo sư hiện đang giảng dạy tại Đại học Carnegie Mellon, Mỹ. Quyển sách ra đời với mong muốn giới thiệu những phương pháp học tiến bộ, cách tư duy khoa học, cũng như các kỹ năng cần thiết cho sinh viên trong quá trình học tập, từ lúc chuẩn bị vào đại học cho đến lúc ra trường tìm việc làm một cách thuận lợi. Giáo sư John Vu viết trang blog này dành cho sinh viên từ nhiều nước mà ông có tham gia giảng dạy (Trung Quốc, Nhật Bản, Hàn Quốc, Ấn Độ, Thổ Nhĩ Kỳ). Tuy nhiên, khi tổ chức cuốn sách này, Ban biên tập đã chọn lọc và sắp xếp các bài viết dựa trên những vấn đề và thắc mắc mà sinh viên Việt Nam thường gặp phải. Mục đích là để giúp sinh viên mở rộng tầm nhìn ra thế giới, ý thức được sự thay đổi mang tính toàn cầu, nhận thức được nhiều cơ hội sẵn có và chủ động tự trui rèn phẩm chất cũng như kỹ năng để có thể đáp ứng được nhu cầu lao động trên thị trường trong nước và thị trường quốc tế. Học sinh cấp 3, học sinh cuối cấp chuẩn bị tốt nghiệp trung học phổ thông và sinh viên đại học có thể sử dụng cuốn sách này như một cẩm nang để tự mình định hướng, lên kế hoạch và quyết định tương lai của chính bản thân mình. Khởi hành được bố cục theo từng chủ đề, học sinh và sinh viên có thể đọc theo từng phần với tư cách là một tập hợp những bài viết chia sẻ kinh nghiệm; hoặc có thể đọc một mạch theo hệ thống để có cái nhìn rõ ràng hơn về thị trường lao động, về việc giáo dục trong hiện tại và tự giáo dục trong tương lai, thông qua đó chủ động nắm bắt cách học sao cho đạt hiệu quả cao nhất về kiến thức và biết ứng dụng vào thực hành.Từng đề mục của cuốn sách được sắp xếp như một kiểu đối thoại với sinh viên, học sinh, giúp các bạn dễ dàng tiếp nhận, nắm bắt những thông tin quan trọng. Quyển sách bao gồm:</p>\r\n\r\n<p>• Phần I:Tốt nghiệp trung học xong, ta nên làm gì? Được viết dành cho các bạn học sinh cấp 3, hoặc cuối cấp 3 đang băn khoăn với con đường học đại học phía trước</p>\r\n\r\n<p>• Phần II: Vào đại học rồi, ta phải làm gì? dành cho các bạn sinh viên. Với các bài viết về phương pháp học tập như Học có mục đích, Học để lấy kỹ năng, Phương pháp học tích cực và Lời khuyên dành cho người sắp tốt nghiệp</p>\r\n\r\n<p>• Phần phụ lục với các bài viết về giáo dục như: Giáo dục: hôm nay và ngày mai, Gian lận trong trường học, Lập kế hoạch nghề nghiệp: Đam mê và Thực tế, Việc làm tốt trong ngành toán học và Cơ hội vô tận</p>', 8, NULL, '2019-12-11 11:00:23', '2019-11-24 22:42:37'),
(4, 'Cha Giàu Cha Nghèo', 'cha-giau-cha-ngheo', NULL, 'vi', '13 x 19 x 2 cm', 375, 'Chọn loại bìa', NULL, '9636056f8d74235e864a62a76f5ed784.jpg', 65000, 1, 150, '<p>Cuốn sách là một câu chuyện, chủ yếu nói về sự giáo dục mà Kiyosaki đã nhận được tại <a href=\"https://vi.wikipedia.org/wiki/Hawaii\" title=\"Hawaii\">Hawaii</a>.</p>\r\n\r\n<p><strong>Người cha nghèo</strong> trong câu chuyện là cha thật của Kiyosaki, có bằng <a href=\"https://vi.wikipedia.org/wiki/Ti%E1%BA%BFn_s%C4%A9\" title=\"Tiến sĩ\">PhD</a>, tốt nghiệp từ <a href=\"https://vi.wikipedia.org/wiki/%C4%90%E1%BA%A1i_h%E1%BB%8Dc_Stanford\" title=\"Đại học Stanford\">Stanford</a>, <a href=\"https://vi.wikipedia.org/wiki/Chicago\" title=\"Chicago\">Chicago</a> và đại học Northwestern, với tất cả sự uyên thâm đó, ông trở thành người đứng đầu ngành giáo dục bang Hawaii. Theo cuốn sách, ông được mọi người rất tôn trọng cho tới khi, giai đoạn cuối sự nghiệp, ông quyết định chống lại thống đốc bang Hawaii. Điều đó trực tiếp khiến <em>người cha nghèo</em> mất việc, và không bao giờ còn khả năng tìm lại được công việc có vị trí như vậy nữa. Bởi vì ông ta chưa bao giờ được học về cách tự do về tài chính, thay vì phụ thuộc hoàn toàn vào trợ cấp chính phủ (một người làm thuê), ông ta chìm trong nợ nần chồng chất.</p>\r\n\r\n<p>Đối lập với nhân vật đó là <strong>người cha giàu</strong>, bố của người bạn thân nhất, Michael. <em>Người cha giàu</em> bỏ học từ lớp 8, nhưng lại trở thành một triệu phú. Ông ta dạy Kiyosako và Michael rất nhiều bài học về tài chính, và luôn nói rằng các cậu phải học để tiền làm việc cho họ chứ đừng tiêu hết tiền kiếm được cho cuộc sống hàng ngày, giống như những nhân công của người cha giàu, cũng như <em>người cha nghèo</em>, và hầu hết mọi người trên thế giới.</p>\r\n\r\n<p>Cuốn sách đã nêu bật vị trí khác nhau của đồng tiền, sự nghiệp và cuộc đời hai người đàn ông, và họ đã làm thế nào để thay đổi các quyết định trong cuộc đời của Kiyosaki.</p>', 9, NULL, '2019-12-07 12:24:14', '2019-11-24 22:42:37'),
(5, 'Đừng Lựa Chọn An Nhàn Khi Còn Trẻ', 'Dung-lua-chon-an-nhan-khi-con-tre', 'Đặng Quân', 'vi', '13.5 x 20 cm', 316, 'Chọn loại bìa', NULL, '0e56b45bddc01b57277484865818ab9b.jpg', 59000, 1, 50, '<p>Trong độ xuân xanh phơi phới ngày ấy, bạn không dám mạo hiểm, không dám nỗ lực để kiếm học bổng, không chịu tìm tòi những thử thách trong công việc, không phấn đấu hướng đến ước mơ của mình. Bạn mơ mộng rằng làm việc xong sẽ vào làm ở một công ty nổi tiếng, làm một thời gian sẽ thăng quan tiến chức. Mơ mộng rằng khởi nghiệp xong sẽ lập tức nhận được tiền đầu tư, cầm được tiền đầu tư là sẽ niêm yết trên sàn chứng khoán. Mơ mộng rằng muốn gì sẽ có đó, không thiếu tiền cũng chẳng thiếu tình, an hưởng những năm tháng êm đềm trong cuộc đời mình. Nhưng vì sao bạn lại nghĩ rằng bạn chẳng cần bỏ ra chút công sức nào, cuộc sống sẽ dâng đến tận miệng những thứ bạn muốn? Bạn cần phải hiểu rằng: Hấp tấp muốn mau chóng thành công rất dễ khiến chúng ta đi vào mê lộ. Thanh xuân là khoảng thời gian đẹp đẽ nhất trong đời, cũng là những năm tháng then chốt có thể quyết định tương lai của một người. Nếu bạn lựa chọn an nhàn trong 10 năm, tương lai sẽ buộc bạn phải vất vả trong 50 năm để bù đắp lại. Nếu bạn bươn chải vất vả trong 10 năm, thứ mà bạn chắc chắn có được là 50 năm hạnh phúc. Điều quý giá nhất không phải là tiền mà là tiền bạc. Thế nên, bạn à, đừng lựa chọn an nhàn khi còn trẻ.</p>', 5, NULL, '2019-11-27 17:05:26', '2019-11-24 22:42:37'),
(6, 'Sự kết thúc của thời đại giả kim', 'su-ket-thuc-cua-nha-gia-kim', 'Ngô Thế Vinh ', 'vi', '16 x 24 cm ', 368, 'Bìa mềm', NULL, 'SuKetThucCuaThoiDaiGiaKim.jpg', 180000, 1, 97, 'Giấy không thể biến thành vàng, và thuật giả kim thời trung cổ đã bị chứng minh đa phần chỉ là trò lừa đảo không hơn không kém. Ấy thế mà suốt một thời gian dài, hệ thống tiền tệ và tài chính của con người đã lấy đó làm nền tảng: biến giấy (tiền) thành vàng.\r\nTác giả Mervyn King, cựu Thống đốc Ngân hàng Anh quốc, chính là một trong những người đầu tiên cảm nhận về vấn đề trầm kha của hệ thống tiền tệ và tài chính hiện hữu. Cuộc Cách mạng Công nghiệp (thế kỷ XVIII-XIX) đã tạo nên nền tảng của thời đại tư bản hiện đại. Tuy nhiên, sự bùng nổ của những đổi mới công nghệ của thời hiện đại đang thách thức hai ý niệm cũ về hệ thống tài chính và tiền tệ: đó là việc sử dụng tiền giấy và sự ra đời các ngân hàng tín dụng. Ngày nay, chúng ta vẫn coi những hệ thống này như điều hiển nhiên, dẫu cho cốt lõi của những ý niệm đó có bất thường và “màu nhiệm” (như thuật giả kim) ra sao đi nữa. Những tờ giấy có in chữ bình thường bỗng trở nên quý giá chẳng kém gì vàng, và những khoản vay dài hạn đầy rủi ro được chuyển thành các khoản tiền gửi ngắn hạn an toàn. Như tác giả gọi, đây chính là “thuật giả kim tài chính” – sự tạo ra những sức mạnh tài chính khổng lồ có thể phớt lờ thực tế và lý lẽ tự nhiên. Niềm tin vào sức mạnh này mang lại những lợi ích to lớn; tính thanh khoản mà nó tạo ra đã giúp thúc đẩy sự tăng trưởng kinh tế trong suốt hai thế kỷ đã qua. Tuy nhiên, nó cũng tạo ra một chuỗi thảm họa kinh tế không có hồi kết, từ siêu lạm phát cho đến sự sụp đổ của ngành ngân hàng, từ những cuộc suy thoái toàn cầu cho đến tình trạng trì trệ hiện tại.\r\nLàm thế nào chúng ta có thể dung hòa giữa điểm mạnh và điểm yếu của những ý niệm trên? Từ kinh nghiệm của bản thân trong lĩnh vực tài chính, tác giả đã đưa ra những kiến giải mới mẻ về các lực lượng kinh tế này và chỉ ra con đường hướng về phía trước cho nền kinh tế thế giới. Những giải pháp táo bạo của Mervyn King đánh thẳng vào những yếu tố phức tạp, không cần thiết và thừa thãi của hệ thống lập pháp để đề ra một con đường sáng sủa hơn cho sự phát triển kinh tế và chấm dứt sự phụ thuộc thái quá vào “thuật giả kim” của chúng ta. ', 5, NULL, '2019-12-07 12:22:27', '2019-11-24 22:42:37'),
(7, 'Khi Hơi Thở Hóa Thinh Không', 'khi-hoi-tho-hoa-thinh-khong', 'Trần Thanh Hương', 'vi', '14 x 20.5 cm', 236, 'Chọn loại bìa', NULL, 'khi-hoi-tho-hoa-thinh-khong.gif', 109000, 1, 250, '<p>Khi Hơi Thở Hóa Thinh Không là tự truyện của một bác sĩ bị mắc bệnh ung thư phổi. Trong cuốn sách này, tác giả đã chia sẻ những trải nghiệm từ khi mới bắt đầu học ngành y, tiếp xúc với bệnh nhân cho tới khi phát hiện ra mình bị ung thư và phải điều trị lâu dài. Kalanithi rất yêu thích văn chương nên câu chuyện của anh đã được thuật lại theo một phong cách mượt mà, dung dị và đầy cảm xúc. Độc giả cũng được hiểu thêm về triết lý sống, triết lý nghề y của Kalanithi, thông qua ký ức về những ngày anh còn là sinh viên, rồi thực tập, cho đến khi chính thức hành nghề phẫu thuật thần kinh. “Đối với bệnh nhân và gia đình, phẫu thuật não là sự kiện bi thảm nhất mà họ từng phải đối mặt và nó có tác động như bất kỳ một biến cố lớn lao trong đời. Trong những thời điểm nguy cấp đó, câu hỏi không chỉ đơn thuần là sống hay chết mà còn là cuộc sống nào đáng sống.” – Kalanithi luôn biết cách đưa vào câu chuyện những suy nghĩ sâu sắc và đầy sự đồng cảm như thế. Bạn bè và gia đình đã dành tặng những lời trìu mến nhất cho con người đáng kính trọng cả về tài năng lẫn nhân cách này. Dù không thể vượt qua cơn bệnh nan y, nhưng thông điệp của tác giả sẽ còn khiến người đọc nhớ mãi.</p>', 13, NULL, '2019-12-04 16:17:22', '2019-11-24 22:42:37'),
(8, 'Vọng Sài Gòn', 'vong-Sai-Gon', NULL, 'vi', '14 x 20.5 cm', 300, 'Chọn loại bìa', NULL, 'af487ae43ff73b502b854fc356b156a7.jpg', 108000, 1, 8, '<p>“Đọc sách để thư giãn, nhưng không phải với cuốn này. Miêu viết là để bạn đọc đấu vật với tiềm thức của chính mình, cào xới đến xây xát cả tàng thức để tìm cho ra những hạt đậu tốt/xấu mà mảnh đất này để lại. Để biết mình đang yêu một thành phố như thế nào.” - Liêu Hà Trinh, MC, diễn viên “Người viết cuốn sách có tình yêu nồng nàn đến dữ dội đối với vùng đất Sài Gòn khiến cho bất cứ ai, dù sống ở đây chưa lâu hay có gốc gác nhiều đời ở vùng đất này, vừa cảm thấy gần gũi nhiều điều cuốn sách đề cập đến, vừa cảm thấy tình không đủ nặng, yêu chưa da diết và còn nhiều thờ ơ với nó, khi đọc những trang viết của Trác Thúy Miêu.” - Phạm Công Luận</p>', 3, NULL, '2019-12-04 16:17:44', '2019-11-24 22:42:37'),
(9, 'Ông Trăm Tuổi Trèo Qua Cửa Sổ Và Biến Mất (Tái Bản)', 'ong-tram-tuoi-treo-qua-cua-so-va-bien-mat-tai-ban', 'Phạm Hải Anh', 'vi', '13 x 20 cm ', 516, 'Bìa mềm', NULL, '26b6f3e1029bad38317073706098398c.jpg', 170000, 1, 36, '\r\n\r\nVào ngày sinh nhật lần thứ 100 của mình, cụ ông Allan Karlsson đột nhiên trèo qua của sổ ngôi nhà dưỡng lão – Nhà Già – và biến mất. Ở cái tuổi 100 hiếm ai đạt tới thì cụ có thể đi đâu được? Một cuộc truy tìm trên khắp nước Thụy Điển diễn ra từ phía những người có trách nhiệm chăm nom cụ cũng như chính quyền sở tại. Song song với cuộc truy tìm nhân đạo ấy, một cuộc truy tìm đuổi bắt khác gay cấn hơn, xảy đến từ một tên tội phạm, kẻ đã ngớ ngẩn hoặc đúng hơn, bất cẩn trao vali 50 triệu crown vào tay cụ già này.\r\n\r\nNhưng một người đã sống qua một thế kỷ thì không dễ gì tóm cụ ta được. Cuốn tiểu thuyết hồi tưởng lại cuộc đời phiêu lưu của Allan Karlsson, người đã đi khắp thế giới từ những năm trước Đại chiến thế giới thứ nhất đến cuộc Thế chiến thứ hai, từ nước Nga Xô Viết tới nước Mỹ siêu cường và nước Trung Quốc con rồng đang lên ở Viễn Đông. Cuốn tiểu thuyết với giọng điệu hóm hỉnh trào lộng, dẫn dắt người đọc chu du cùng Allan qua những tình huống giả tưởng làm bật lên cái nhìn tưng tửng về thế giới này. Những xung đột văn hóa, ý thức hệ và những nét khác thường của các vùng đất xa xôi, càng chứng tỏ sự đa dạng của nhân loại trong thế giới có vẻ phẳng này.\r\n\r\nCuốn tiểu thuyết Ông trăm tuổi bốc hơi qua cửa sổ đã trở thành hiện tượng quốc gia ở Thụy Điển, đem lại cho người đọc một cái nhìn hài hước kín đáo của văn hóa Bắc Âu, nơi có truyền thống tôn quý văn học lâu đời.\r\n', 1, NULL, '2019-12-07 12:22:28', '2019-11-24 22:42:37'),
(10, 'Tôi tự học', 'toi-tu-hoc', NULL, 'vi', '13 x 19 cm', 262, 'Bìa mềm', NULL, 'toi-tu-hoc.gif', 60000, 1, 30, '\r\n\r\nTôi Tự Học\r\n\r\nSách “Tôi tự học” của tác giả Nguyễn Duy Cần đề cập đến khái niệm, mục đích của học vấn đối với con người đồng thời nêu lên một số phương pháp học tập đúng đắn và hiệu quả. Tác giả cho rằng giá trị của học vấn nằm ở sự lĩnh hội và mở mang tri thức của con người chứ không đơn thuần thể hiện trên bằng cấp. Trong xã hội ngày nay, không ít người quên đi ý nghĩa đích thực của học vấn, biến việc học của mình thành công cụ để kiếm tiền nhưng thực ra nó chỉ là phương tiện để  đưa con người đến thành công mà thôi. Bởi vì học không phải để lấy bằng mà học còn để “biết mình” và để “đối nhân xử thế”.\r\n\r\nCuốn sách này tuy đã được xuất bản từ rất lâu nhưng giá trị của sách vẫn còn nguyên vẹn. Những tư tưởng, chủ đề của sách vẫn phù hợp và có thể áp dụng trong đời sống hiện nay. Thiết nghĩ, cuốn sách này rất cần cho mọi đối tượng bạn đọc vì không có giới hạn nào cho việc truy tầm kiến thức, việc học là sự nghiệp lâu dài của mỗi con người. Đặc biệt, cuốn sách là một tài liệu quý để các bạn học sinh – sinh viên tham khảo, tổ chức lại việc học của mình một cách hợp lý và khoa học. Các bậc phụ huynh cũng cần tham khảo sách này để định hướng và tư vấn cho con em mình trong quá trình học tập.\r\n', 1, NULL, '2019-11-24 22:42:37', '2019-11-24 22:42:37'),
(11, 'Tôi, Tương Lai Và Thế Giới', 'toi-tuong-lai-va-the-gioi', NULL, 'vi', '14 x 20.5 cm', 280, 'Bìa mềm', NULL, 'toi-tuong-lai-va-the-gioi.jpg', 160000, 1, 15, '\r\n\r\nTôi, Tương lai & Thế giới ra đời đúng vào thời điểm cần thiết nhất đối với Việt Nam. Môi trường sống của chúng ta, kể cả ở Việt Nam đang thay đổi ở một tốc độ chưa từng có do Cách mạng công nghiệp 4.0 đem lại. Trong sự thay đổi ấy, luật tiến hóa của vạn vật sẽ là tấm lưới chọn lọc những ai tồn tại. Đó không phải người thông minh nhất, cũng không phải người mạnh nhất hay nhanh nhất mà là người có khả năng thích nghi cao nhất.\r\n\r\nQua những trải nghiệm trong cuộc sống, tác giả Nguyễn Phi Vân giúp người đọc vượt thời gian và không gian để có một cảm nhận cho những thay đổi hằng ngày trong cuộc sống tương lai mà người đọc có thể không tưởng tượng được, và từ đó rút ra những kinh nghiệm sống và những bài học quý giá mà chúng ta cần phải chuẩn bị để không bị đào thải bởi luật tiến hóa.\r\n\r\nNhư nhà khoa học gia, chính trị gia Benjamin Franklin từng nói: “Thất bại trong chuẩn bị, bạn đang chuẩn bị cho sự thất bại”. Tôi, Tương lai & Thế giới nên là cuốn sách đầu giường cho các bạn trẻ, các nhà giáo dục, kể cả những nhà khởi nghiệp và những người làm chính sách để có sự chuẩn bị và sẵn sàng nắm bắt cơ hội khi cơn sóng thần Cách mạng công nghiệp 4.0 tràn đến.\r\n', 5, NULL, '2019-11-24 22:42:37', '2019-11-24 22:42:37'),
(12, 'Có Hai Con Mèo Ngồi Bên Cửa Sổ', 'co-hai-con-meo-ngoi-ben-cua-so', NULL, 'vi', '0x0', 210, 'Bìa mềm', NULL, '93351eded0a9ff29538401f76f76c2c1.jpg', 85000, 1, 150, '\r\n\r\nCó Hai Con Mèo Ngồi Bên Cửa Sổ\r\n\r\nNhân vật chính thứ nhất tên là Gấu.\r\n\r\nNhân vật thứ hai là Tí Hon.\r\n\r\nNhân vật thứ ba, tên là…; còn nữa, nhân vật thứ tư, tên là…\r\n\r\nĐể biết tại sao Gấu lại chơi thân với Tí Hon, thì mời bạn hãy mở sách ra.\r\n\r\nGấu và Tí Hon thân nhau đến mức có thể chia sẻ từng chuyện vui buồn trong những phút giây mềm yếu, lo lắng và chăm sóc, giúp nhau từ miếng ăn đến “chiến lược” để tồn tại lâu dài.Tình bạn là gì? Bạn gái là gì? Tình yêu là gì?\r\nBọn mèo chuột kể với chúng ta nhiều câu chuyện nhỏ, gửi thông điệp rằng, tình yêu có sức mạnh tuyệt diệu, có thể làm nên mọi điều phi thường trong cuộc sống muôn loài.\r\n\r\nCuốn truyện có độ dầy vừa phải, 67 hình vẽ của họa sĩ Đỗ Hoàng Tường sinh động đến từng nét nũng nịu hay kiêu căng của nàng mèo người yêu mèo Gấu, câu chuyện thì hấp dẫn duyên dáng điểm những bài thơ tình lãng mạn nao lòng song đọc to lên thì khiến cười hinh hích…\r\n\r\nBạn hãy đọc nhé, để thấy, Nguyễn Nhật Ánh đã viết truyện mèo chuột theo cái cách của riêng mình độc đáo như thế nào.\r\n\r\n* Giá sản phẩm trên Tiki đã bao gồm thuế theo luật hiện hành. Tuy nhiên tuỳ vào từng loại sản phẩm hoặc phương thức, địa chỉ giao hàng mà có thể phát sinh thêm chi phí khác như phí vận chuyển, phụ phí hàng cồng kềnh, ...', 1, NULL, '2019-12-07 12:36:57', '2019-11-24 22:42:37'),
(13, 'Bố Già (Bìa Cứng - Tái Bản 2017) - Mario Puzo', 'bo-gia-bia-cung-tai-ban-2017-mario-puzo', 'Ngọc Thứ Lan', 'vi', '16 x 24 cm', 534, 'Chọn loại bìa', NULL, 'bo-gia_1_1.jpg', 150000, 1, 109, '<p>Bố Già Thế giới ngầm được phản ánh trong tiểu thuyết Bố già là sự gặp gỡ giữa một bên là ý chí cương cường và nền tảng gia tộc chặt chẽ theo truyền thống Mafia xứ Sicily với một bên là xã hội Mỹ nhập nhằng đen trắng, mảnh đất màu mỡ cho những cơ hội làm ăn bất chính hứa hẹn những món lợi kếch xù. Trong thế giới ấy, hình tượng Bố già được tác giả dày công khắc họa đã trở thành bức chân dung bất hủ trong lòng người đọc. Từ một kẻ nhập cư tay trắng đến ông trùm tột đỉnh quyền uy, Don Vito Corleone là con rắn hổ mang thâm trầm, nguy hiểm khiến kẻ thù phải kiềng nể, e dè, nhưng cũng được bạn bè, thân quyến xem như một đấng toàn năng đầy nghĩa khí. Nhân vật trung tâm ấy đồng thời cũng là hiện thân của một pho triết lí rất “đời” được nhào nặn từ vốn sống của hàng chục năm lăn lộn giữa chốn giang hồ bao phen vào sinh ra tử. Với kết cấu hoàn hảo, cốt truyện không thiếu những pha hành động gay cấn, tình tiết bất ngờ và không khí kình địch đến nghẹt thở, Bố già xứng đáng là đỉnh cao trong sự nghiệp văn chương của Mario Puzo. Nhận định “Bố già là sự tổng hòa của mọi hiểu biết. Bố già là đáp án cho mọi câu hỏi.” (Diễn viên Tom Hanks) “Bạn không thể dừng đọc nó và khó lòng ngừng mơ về nó.” (New York Times Magazine) “Một tác phẩm kinh điển về mafia… Tự bản thân nó đã tạo ra một thứ bùa mê hoặc độc giả.” (The Times)</p>', 2, NULL, '2019-12-11 11:00:23', '2019-11-24 22:42:37'),
(14, 'Sapiens: Lược Sử Loài Người (Tái Bản Có Chỉnh Sửa)', 'sapiens-luoc-su-loai-nguoi-tai-ban-co-chinh-sua', 'Nguyễn Thủy Chung', 'vi', '16 x 24 cm', 554, 'Bìa mềm', NULL, 'f4e6621e69775643d22604bccef281bf.jpg', 209000, 1, 300, '\r\n\r\nSapiens: Lược Sử Loài Người\r\n\r\nSapiens, đưa chúng ta vào một chuyến đi kinh ngạc qua toàn bộ lịch sử loài người, từ những gốc rễ tiến hóa của nó đến thời đại của chủ nghĩa tư bản và kỹ thuật di truyền, để khám phá tại sao chúng ta đang trong những điều kiện sinh sống hiện tại.\r\n\r\nSapiens tập trung vào các quá trình quan trọng đã định hình loài người và thế giới quanh nó, chẳng hạn như sự ra đời của sản xuất nông nghiệp, việc tạo ra tiền, sự lan truyền của những tôn giáo, và sự nổi lên của những nhà nước quốc gia. Không giống như những quyển sách khác cùng loại, Sapiens đã có một lối tiếp cận liên ngành học, bắc cầu qua những khoảng cách giữa lịch sử, sinh học, triết học và kinh tế theo một lối trước đây chưa từng có. Hơn nữa, lấy cả quan điểm vĩ mô và vi mô, Sapiens không chỉ đề cập đến những gì đã xảy ra và tại sao, mà còn đi sâu vào việc những cá nhân trong lịch sử đó đã cảm nhận nó như thế nào.\r\n\r\nCâu hỏi lớn và sâu sắc của Harari là: chúng ta thực sự muốn gì? Có cách nào để đạt được hạnh phúc cho con người chúng ta, hoặc thậm chí liệu chúng ta có biết được nó là gì hay không? Trong cốt lõi của nó, Sapiens biện luận rằng chúng ta không biết về bản thân chúng ta, huống chi biết được những nhu cầu của những loài sinh vật khác. Chúng ta đã quá thường xuyên bị những tưởng tượng hư cấu của chúng ta lừa dối. Lịch sử cũng là một hư cấu, nhưng một hư cấu đã được kiềm chế bởi thực tại và biện luận: một hình thức của huyền thoại – một hư cấu hữu ích – khiến nó có thể mang lại sự giác ngộ của sự tự biết chính mình.\r\n\r\n“Tôi khuyến khích tất cả chúng ta, dẫu có những tin tưởng tín ngưỡng nào, để đặt câu hỏi về những thuật kể cơ bản về thế giới chúng ta, để nối những phát triển ngày xưa với những quan tâm ngày nay, và để không sợ hãi những vấn đề tranh luận” (Yuval Noah Harari)\r\n', 14, NULL, '2019-11-24 22:42:37', '2019-11-24 22:42:37'),
(15, 'Để Con Được Ốm (Tái Bản 2018)', 'de-con-duoc-om-tai-ban-2018', NULL, 'vi', '14 x 20.5 cm', 300, 'Bìa mềm', NULL, 'c9c14e9e05b605d40ea29249240264f9.jpg', 95000, 1, 5, 'Để Con Được Ốm\r\n\r\nĐể con được ốm có thể coi là một cuốn nhật ký học làm mẹ thông qua những câu chuyện từ trải nghiệm thực tế mà chị Uyên Bùi đã trải qua từ khi mang thai đến khi em bé chào đời và trở thành một cô bé khỏe mạnh, vui vẻ. Cùng với những câu chuyện nhỏ thú vị của người mẹ là lời khuyên mang tính chuyên môn, giải đáp cụ thể từ bác sỹ Nguyễn Trí Đoàn, giúp hóa giải những hiểu lầm từ kinh nghiệm dân gian được truyền lại, cũng như lý giải một cách khoa học những thông tin chưa đúng đắn đang được lưu truyền hiện nay, mang đến góc nhìn đúng đắn nhất cho mỗi hiện tượng, sự việc với những kiến thức y khoa hiện đại được cập nhật liên tục. Cuốn sách sẽ giúp các bậc phụ huynh trang bị một số kiến thức cơ bản trong việc chăm sóc trẻ một cách khoa học và góp phần giúp các mẹ và những-người-sẽ-là-mẹ trở nên tự tin hơn trong việc chăm con, xua tan đi những lo lắng, để mỗi em bé ra đời đều được hưởng sự chăm sóc tốt nhất.\r\n\r\nTrên thị trường có nhiều đầu sách về chăm sóc trẻ, tuy nhiên đa phần đều là nội dung được dịch từ các sách nước ngoài nên phần nào đó không phù hợp với các gia đình Việt bởi sự khác biệt về suy nghĩ và ý thức hệ cùng những quan niệm sống. Đặc biệt là các cuốn sách ấy không bao giờ nhắc tới cách xử lý các vấn đề đời thường, những kinh nghiệm dân gian hay phương pháp chăm sóc trẻ từ “kinh nghiệm cá nhân” của các mẹ tràn lan trên các diễn đàn và mạng xã hội - những điều đang tồn tại trong xã hội Việt ta hiện nay. Do đó, Để con được ốm trở thành một tác phẩm độc đáo khi đi sâu vào vấn đề chăm sóc con trẻ cùng những điều bất cập trong xã hội Việt, trong những gia đình Việt, giữa các thành viên trong gia đình. Để con được ốm không phải là cuốn “bách khoa toàn thư” chữa bách bệnh ở trẻ em, nó đơn giản là lời sẻ chia và động viên gửi đến các bà mẹ trẻ nói riêng và các gia đình nói chung.\r\n\r\nHi vọng, cuốn sách sẽ khiến các bậc phụ huynh cảm thấy như một luồng gió mát giúp “cởi phăng”, “cuốn bay” những suy nghĩ, những định kiến không đúng như những lớp áo giáp vững chắc về hiều biết, về cách chăm sóc trẻ khi bệnh cũng như khi khỏe.\r\n\r\nNhận xét\r\n\r\n“Khi cầm cuốn sách trên tay, tôi cảm thấy hết sức thú vị vì nội dung của nó đã gạt bỏ hầu hết mọi quan niệm về chăm sóc trẻ theo “kinh nghiệm” truyền tai của các bà mẹ - nuôi con nhìn sang nhà hàng xóm. Đặc biệt hơn, nó được tổng kết từ thực tế chăm sóc con của một bà mẹ trẻ. Những rắc rối hết sức đời thường tưởng như nhỏ nhặt ấy đã được giải đáp và phân tích một cách cặn kẽ bằng các chứng cứ khoa học và những lời khuyên cũng hết sức đời thường.”\r\n\r\n(Bs. Ngô Đức Hùng, Bệnh viện Bạch Mai)\r\n\r\n\"Từ giây phút đầu tiên cầm quyển sách trên tay, tôi biết rằng quyển sách này sẽ tạo ra sự thay đổi trong nhận thức của cha mẹ Việt về cách chữa trị bằng trái tim yêu thương, thay vì việc chữa trị thông thường thông qua các biểu hiện bệnh lý.\"\r\n\r\n(Mrs. Jeannie Chan-Ho, Viện trưởng Viện Giáo dục Shichida Việt Nam)\r\n\r\n\"Sức khỏe của con luôn là mối quan tâm hàng đầu của mẹ. Chỉ cần con hắt hơi thôi là lo lắng có phải bệnh không? Khi con bệnh, mình chăm sóc sao đây? Sao tháng này con tăng ký ít vậy? Phát triển vậy có bình thường không? Có suy dinh dưỡng không? Hôm nay con ăn có vài muỗng, sao đủ chất? Bao nhiêu lo lắng, từ lớn đến nhỏ đều được Bác sĩ Trí Đoàn giải thích rõ ràng, dễ hiểu và mang tính khoa học. Nhiều kinh nghiệm dân gian chưa đúng cũng được mổ xẻ, phân tích giúp các mẹ hiểu cái gì nên giữ lại, cái gì nên điều chỉnh cho đúng. Thậm chí, những người trong ngành y như tôi cũng được cập nhật những kiến thức cũ thành mới. Có thể nói, chưa quyển sách nào làm đươc điều này. Tôi tin Để con được ốm là một quyển sách đáng để gối đầu giường cho các bà mẹ, giúp các mẹ tự tin chăm sóc bé yêu một cách tốt nhất!\"\r\n\r\n(Bs. Anh Thy - Chuyên viên Tư vấn Sữa mẹ Quốc tế tại Việt Nam)\r\n\r\n\"Cái hay của sách là giúp chúng ta - những học trò bỡ ngỡ trong lớp học “làm cha mẹ” - giảm bớt áp lực trong quá trình nuôi con. Nuôi con không bao giờ nhàn nhã, nhưng nên mang đến niềm vui, thay vì sự căng thẳng và những chuỗi ngày dài băn khoăn lo lắng, rồi dẫn tới bao điều không hay khác.\"\r\n\r\n(Bs. Đinh Huỳnh Linh, Bệnh viện Tim mạch Quốc gia)\r\n', 5, NULL, '2019-11-24 22:42:37', '2019-11-24 22:42:37'),
(19, 'Cảm Xúc Là Kẻ Thù Số Một Của Thành Công (Tái Bản 2018)', 'Cam-xuc-la-ke-thu-so-mot-cua-thanh-cong', 'none', 'vi', '14.5 x 20.5', 50, 'cung', 1134, 'fbeG_camXucLaKeThu.png', 225000, 1, 123, '<p><strong>[Bản Đặc Biệt] Cảm Xúc Là Kẻ Thù Số Một Của Thành Công (Tái Bản 2018) - Kèm Chữ Ký Tác Giả + 8 Postcard (Số Lượng Có Hạn)</strong></p>\r\n\r\n<p style=\"text-align:justify\">Sau thành công của cuốn&nbsp;Cảm xúc là kẻ thù số một của thành công&nbsp;phiên bản đặc biệt 2017, báo&nbsp;Sinh Viên Việt Nam – Hoa Học Trò&nbsp;và&nbsp;Langmaster&nbsp;tiếp tục phát hành phiên bản đặc biệt 2018 với phần hình thức được làm mới hoàn toàn và thêm 30% nội dung mới, theo yêu cầu của đông đảo bạn đọc và người hâm mộ&nbsp;TS Lê Thẩm Dương.</p>\r\n\r\n<p style=\"text-align:justify\">Nhà báo&nbsp;Nguyễn Tuấn Anh&nbsp;– Thư ký Tòa soạn báo&nbsp;Sinh Viên Việt Nam, chủ biên cuốn sách&nbsp;Cảm xúc là kẻ thù số một của thành công&nbsp;phiên bản đặc biệt 2018 cho biết nhiều nội dung mới mang tính thời sự được&nbsp;TS Lê Thẩm Dương&nbsp;bổ sung như: Những bài học cho phụ huynh từ vụ việc sai phạm điểm thi ở Hà Giang, triết lý bóng đá trong công việc và cuộc sống từ thành tích của đội tuyển U23 và Olympic Việt Nam tại giải U23 Châu Á và ASIAD 2018, các bước để khởi nghiệp thành công, những bài học cuộc sống...</p>\r\n\r\n<p style=\"text-align:justify\">Cảm xúc là kẻ thù số một của thành công&nbsp;phiên bản đặc biệt 2018 gồm những bài nói chuyện, bài viết, bài phỏng vấn nổi tiếng của&nbsp;TS Lê Thẩm Dương&nbsp;có tác động lớn đến giới trẻ trong thời gian vừa qua liên quan đến việc hướng nghiệp, chọn nghề nghiệp, học tập, nghiên cứu, lao động, chọn vợ/chồng, thái độ sống tích cực... Sách được chia thành 7 phần: Chào bạn, Tân Sinh Viên; Thất nghiệp là tín hiệu tuyệt vời; Cái gì không mua được bằng tiền?; Chọn vợ/chồng theo hàm số hay biến số?; Hàm số của tình yêu hạnh phúc; Phân loại tính cách bằng MBTI; Bài học cuộc sống –&nbsp;TS Lê Thẩm Dương&nbsp;khuyên đọc.</p>\r\n\r\n<p style=\"text-align:justify\">TS Lê Thẩm Dương&nbsp;giảng dạy tại Học viện Ngân hàng Hà Nội từ năm 1982, sau đó chuyển vào trường Đại học Ngân hàng TP.HCM. Hiện ông đang là Trưởng khoa Tài chính, trường ĐH Ngân hàng TP.HCM. Ông cũng là giảng viên chính chương trình cấp chứng chỉ hành nghề của Uỷ ban Chứng khoán Nhà nước, giảng viên thỉnh giảng của nhiều tập đoàn, trường Đại học, chương trình đào tạo. Ông là khách mời thường xuyên của nhiều diễn đàn cấp quốc gia và khu vực, nhiều chương trình truyền hình uy tín…TS Lê Thẩm Dương&nbsp;nổi tiếng trên mạng với những bài giảng “gây bão”.</p>', 1, NULL, '2019-12-13 20:37:57', '2019-12-13 20:37:57'),
(20, 'sách test', 'erwer', 'ee', 'Chọn ngôn ngữ', '111', 11, 'cung', 111, 'HMla_camXucLaKeThu.png', 122, 1, 11, '<p>12123</p>', 1, 110, '2019-12-14 05:04:31', '2019-12-14 11:29:54'),
(21, 'sách test', 'erwer', 'ee', 'Chọn ngôn ngữ', '111', 11, 'cung', 111, 'xOyi_camXucLaKeThu.png', 122, 1, 11, '<p>12123</p>', 1, 110, '2019-12-14 05:04:34', '2019-12-14 11:31:57'),
(22, 'sách test', 'erwer', 'ee', 'Chọn ngôn ngữ', '111', 11, 'cung', 111, 'rJIl_camXucLaKeThu.png', 122, 1, 11, '<p>12123</p>', 1, 110, '2019-12-14 05:04:37', '2019-12-14 11:34:22');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tacgia`
--

CREATE TABLE `tacgia` (
  `id` int(11) NOT NULL,
  `TenTG` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TenTG_KhongDau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `MoTa` text COLLATE utf8_unicode_ci,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tacgia`
--

INSERT INTO `tacgia` (`id`, `TenTG`, `TenTG_KhongDau`, `MoTa`, `updated_at`, `created_at`) VALUES
(1, 'J. K. Rowling', 'J-K-Rowling', 'Joanne \"Jo\" Rowling, (/[invalid input: \'icon\']ˈroʊlɪŋ/,[1] sinh ngày 31 tháng 7 năm 1965[2]), bút danh là J. K. Rowling,[3] và Robert Galbraith. Cư ngụ tại thủ đô Edinburgh, Scotland là tiểu thuyết gia người Anh, tác giả bộ truyện giả tưởng nổi tiếng Harry Potter với bút danh J. K. Rowling.[4]', '2019-11-21 02:02:44', '2019-11-20 18:50:46'),
(2, 'Nguyễn Nhật Ánh', 'Nguyen-Nhat-Anh', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(3, 'Gs. John Vũ', 'Gs-John-Vu', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(4, 'F. Scott Fitzgerald', 'F-Scott-Fitzgerald', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(5, 'Malcolm Gladwell', 'Malcolm-Gladwell', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(6, 'Dan Ariely', 'Dan-Ariely', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(7, 'Thomas Harris', 'Thomas-Harris', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(8, 'Stephen Hawking', 'Stephen-Hawking', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(9, 'Larry Berman', 'Larry-Berman', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(10, 'Robert T. Kiyosaki', 'Robert-T-Kiyosaki', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(11, 'Sharon L. Lechter', 'Sharon-L-Lechter', NULL, '2019-11-20 18:50:46', '2019-11-20 18:50:46'),
(12, 'Cảnh Thiên', 'Canh-Thien', NULL, '2019-11-23 17:26:17', '2019-11-23 17:26:17'),
(13, 'Mario Puzo', 'Mario-Puzo', '<p><strong>Mario Gianluigi Puzo</strong> (<a href=\"https://vi.wikipedia.org/wiki/15_th%C3%A1ng_10\" title=\"15 tháng 10\">15 tháng 10</a> năm <a href=\"https://vi.wikipedia.org/wiki/1920\" title=\"1920\">1920</a> – <a href=\"https://vi.wikipedia.org/wiki/2_th%C3%A1ng_7\" title=\"2 tháng 7\">2 tháng 7</a> năm <a href=\"https://vi.wikipedia.org/wiki/1999\" title=\"1999\">1999</a>) là một <a href=\"https://vi.wikipedia.org/wiki/Nh%C3%A0_v%C4%83n\" title=\"Nhà văn\">nhà văn</a>, <a href=\"https://vi.wikipedia.org/wiki/Nh%C3%A0_bi%C3%AAn_k%E1%BB%8Bch\" title=\"Nhà biên kịch\">nhà biên kịch</a> người Mỹ, được biết đến với những <a href=\"https://vi.wikipedia.org/wiki/Ti%E1%BB%83u_thuy%E1%BA%BFt\" title=\"Tiểu thuyết\">tiểu thuyết</a> về <a href=\"https://vi.wikipedia.org/wiki/Mafia\" title=\"Mafia\">Mafia</a>, đặc biệt là <em><a href=\"https://vi.wikipedia.org/wiki/B%E1%BB%91_gi%C3%A0_(ti%E1%BB%83u_thuy%E1%BA%BFt)\" title=\"Bố già (tiểu thuyết)\">Bố già</a></em> (1969), mà sau này ông đồng chuyển thể thành <a href=\"https://vi.wikipedia.org/wiki/B%E1%BB%91_gi%C3%A0_(phim)\" title=\"Bố già (phim)\">một bộ phim</a> cùng với <a href=\"https://vi.wikipedia.org/wiki/Francis_Ford_Coppola\" title=\"Francis Ford Coppola\">Francis Ford Coppola</a>. Ông đã giành được <a href=\"https://vi.wikipedia.org/wiki/Gi%E1%BA%A3i_Oscar_cho_k%E1%BB%8Bch_b%E1%BA%A3n_chuy%E1%BB%83n_th%E1%BB%83_xu%E1%BA%A5t_s%E1%BA%AFc_nh%E1%BA%A5t\" title=\"Giải Oscar cho kịch bản chuyển thể xuất sắc nhất\">Giải Oscar cho kịch bản chuyển thể xuất sắc nhất</a> vào giữa những năm 1972 và 1974. Dù là một nhà văn được cưng chiều của Hollywood nhưng ông vẫn luôn cảm thấy thất vọng về kinh đô điện ảnh của Mỹ.</p>', '2019-12-04 16:19:49', '2019-12-04 16:19:49'),
(14, 'Lê Thẩm Dương', 'tesst', NULL, '2019-12-13 20:28:43', '2019-12-13 20:28:43'),
(15, 'thuan', 'thuan', NULL, '2019-12-14 11:34:22', '2019-12-14 11:34:22'),
(16, 'ddsd', 'ddsd', NULL, '2019-12-14 11:45:36', '2019-12-14 11:45:36');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `theloai`
--

CREATE TABLE `theloai` (
  `id` int(11) NOT NULL,
  `TenTL` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `TenTL_KhongDau` varchar(225) COLLATE utf8_unicode_ci NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `theloai`
--

INSERT INTO `theloai` (`id`, `TenTL`, `TenTL_KhongDau`, `updated_at`, `created_at`) VALUES
(2, 'kinh tế', 'kinh-te', '2019-11-18 08:21:01', '2019-11-18 08:23:29'),
(3, 'văn học', 'van-hocnew', '2019-11-18 16:02:25', '2019-11-18 08:23:29'),
(4, 'kiến thức tổng hợp', 'kien-thuc-tong-hop', '2019-11-18 08:21:01', '2019-11-18 08:23:29'),
(5, 'tiểu sử - hồi ký', 'tieu-su-hoi-ky', '2019-11-18 08:21:01', '2019-11-18 08:23:29'),
(6, 'qwe', 'qwe1', '2019-11-18 16:03:18', '2019-11-18 15:24:14'),
(7, 'hoang', 'hoang-dun', '2019-11-21 01:05:35', '2019-11-18 16:10:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tin`
--

CREATE TABLE `tin` (
  `id` int(11) NOT NULL,
  `TenTin` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TenTin_KhongDau` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `TrangThai` tinyint(1) NOT NULL,
  `NoiDungTin` text COLLATE utf8_unicode_ci NOT NULL,
  `urlHinh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_KM` int(11) NOT NULL,
  `id_NV` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tin`
--

INSERT INTO `tin` (`id`, `TenTin`, `TenTin_KhongDau`, `TrangThai`, `NoiDungTin`, `urlHinh`, `id_KM`, `id_NV`, `updated_at`, `created_at`) VALUES
(15, 'sale Giáng Sinh', 'sale-giang-sinh', 1, '<p>sale test</p>', 'fbMS_Banner-1-dummy.jpg', 7, 1, '2019-12-04 08:30:21', '2019-12-04 08:30:21'),
(16, 'sale Giáng Sinh 1', 'sale-giang-sinh-1', 1, '<p>test 2</p>', '5Ou7_Banner-5-1.jpg', 8, 1, '2019-12-14 02:38:27', '2019-12-04 08:37:39'),
(17, 'Tin KM 12 12', 'tin-km-12-12', 1, '<p>1234568</p>', '1920px-Great_Wave_off_Kanagawa2.jpg', 8, 1, '2019-12-13 18:54:21', '2019-12-12 14:01:52');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tenUser` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `SDT` int(11) DEFAULT NULL,
  `DiaChi` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `NgaySinh` date DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `TrangThai` tinyint(1) DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `user`
--

INSERT INTO `user` (`id`, `email`, `tenUser`, `SDT`, `DiaChi`, `NgaySinh`, `password`, `TrangThai`, `remember_token`, `updated_at`, `created_at`, `active`, `email_verified_at`) VALUES
(1, 'thuan@gmail.com', 'thuan', 1, '1', '2019-11-29', '$2y$10$iex38CqjqmaFCnKrjRW.T.5pJvLrhwqR4k0gme3IZHcQ69pNdCuJy', 1, NULL, '2019-11-29 04:29:42', '2019-11-29 04:29:42', 0, NULL),
(2, 'lox@gmail.com', 'lox', 1, '1', '2019-12-03', '$2y$10$iex38CqjqmaFCnKrjRW.T.5pJvLrhwqR4k0gme3IZHcQ69pNdCuJy', 1, NULL, '2019-12-03 20:17:31', '2019-11-29 04:29:42', 0, NULL),
(4, 'tnloc@outlook.com', 'Tan Loc', NULL, NULL, NULL, '$2y$10$5mgqA.AJvJIuT2ejJ7iHauZ5jS8v3tInBiJKnQbyNcxxdg5ELCXIu', 1, NULL, '2019-11-29 16:04:37', '2019-11-29 16:04:37', 0, NULL),
(5, 'tnloc@Outlook.com', 'Tan Loc', 906828619, '112233 ND P12 Q8', '1998-05-27', '$2y$10$jP1KEawbEibvobu3icKgq.z7Phj0PxYWWaCFJH4ELYVpX9LBdVWXm', 1, NULL, '2019-11-30 15:06:28', '2019-11-30 15:06:28', 0, NULL),
(6, 'hoangdung2k19@gmail.com', 'dung hoang', 917733664, '123/4accc', '2019-01-09', '$2y$10$w/9aYZpWM7vJX6o.cHheIuOmilOwzSIONBRBRLRBlFVyWcqsE89C6', 1, NULL, '2019-12-09 12:52:05', '2019-12-04 12:38:41', 0, NULL);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_Sach` (`id_Sach`),
  ADD KEY `id_User` (`id_User`),
  ADD KEY `id_NV` (`id_NV`);

--
-- Chỉ mục cho bảng `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  ADD KEY `id_DH` (`id_DH`),
  ADD KEY `id_Sach` (`id_Sach`);

--
-- Chỉ mục cho bảng `chitietkhuyenmai`
--
ALTER TABLE `chitietkhuyenmai`
  ADD KEY `id_Sach` (`id_Sach`),
  ADD KEY `id_KM` (`id_KM`);

--
-- Chỉ mục cho bảng `chitietloai`
--
ALTER TABLE `chitietloai`
  ADD KEY `id_Sach` (`id_Sach`),
  ADD KEY `id_loai` (`id_Loai`);

--
-- Chỉ mục cho bảng `chitiettacgia`
--
ALTER TABLE `chitiettacgia`
  ADD KEY `id_TG` (`id_TG`),
  ADD KEY `id_Sach` (`id_Sach`);

--
-- Chỉ mục cho bảng `donhang`
--
ALTER TABLE `donhang`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_User` (`id_User`),
  ADD KEY `id_NV` (`id_NV`);

--
-- Chỉ mục cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `khuyenmai`
--
ALTER TABLE `khuyenmai`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_NV` (`id_NV`);

--
-- Chỉ mục cho bảng `like`
--
ALTER TABLE `like`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_Sach` (`id_Sach`),
  ADD KEY `id_User` (`id_User`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `nhanvien`
--
ALTER TABLE `nhanvien`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `nhaxuatban`
--
ALTER TABLE `nhaxuatban`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `sach`
--
ALTER TABLE `sach`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_NXB` (`id_NXB`);

--
-- Chỉ mục cho bảng `tacgia`
--
ALTER TABLE `tacgia`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `theloai`
--
ALTER TABLE `theloai`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tin`
--
ALTER TABLE `tin`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_NV` (`id_NV`),
  ADD KEY `id_KM` (`id_KM`);

--
-- Chỉ mục cho bảng `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `donhang`
--
ALTER TABLE `donhang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT cho bảng `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `khuyenmai`
--
ALTER TABLE `khuyenmai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `like`
--
ALTER TABLE `like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `nhanvien`
--
ALTER TABLE `nhanvien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `nhaxuatban`
--
ALTER TABLE `nhaxuatban`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT cho bảng `sach`
--
ALTER TABLE `sach`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT cho bảng `tacgia`
--
ALTER TABLE `tacgia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `theloai`
--
ALTER TABLE `theloai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `tin`
--
ALTER TABLE `tin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `binhluan`
--
ALTER TABLE `binhluan`
  ADD CONSTRAINT `binhluan_ibfk_1` FOREIGN KEY (`id_NV`) REFERENCES `nhanvien` (`id`),
  ADD CONSTRAINT `binhluan_ibfk_3` FOREIGN KEY (`id_Sach`) REFERENCES `sach` (`id`),
  ADD CONSTRAINT `binhluan_ibfk_4` FOREIGN KEY (`id_User`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `chitietdonhang`
--
ALTER TABLE `chitietdonhang`
  ADD CONSTRAINT `chitietdonhang_ibfk_1` FOREIGN KEY (`id_Sach`) REFERENCES `sach` (`id`),
  ADD CONSTRAINT `chitietdonhang_ibfk_2` FOREIGN KEY (`id_DH`) REFERENCES `donhang` (`id`);

--
-- Các ràng buộc cho bảng `chitietkhuyenmai`
--
ALTER TABLE `chitietkhuyenmai`
  ADD CONSTRAINT `chitietkhuyenmai_ibfk_1` FOREIGN KEY (`id_Sach`) REFERENCES `sach` (`id`),
  ADD CONSTRAINT `chitietkhuyenmai_ibfk_2` FOREIGN KEY (`id_KM`) REFERENCES `khuyenmai` (`id`);

--
-- Các ràng buộc cho bảng `chitietloai`
--
ALTER TABLE `chitietloai`
  ADD CONSTRAINT `chitietloai_ibfk_1` FOREIGN KEY (`id_Loai`) REFERENCES `theloai` (`id`),
  ADD CONSTRAINT `chitietloai_ibfk_2` FOREIGN KEY (`id_Sach`) REFERENCES `sach` (`id`);

--
-- Các ràng buộc cho bảng `chitiettacgia`
--
ALTER TABLE `chitiettacgia`
  ADD CONSTRAINT `chitiettacgia_ibfk_1` FOREIGN KEY (`id_TG`) REFERENCES `tacgia` (`id`),
  ADD CONSTRAINT `chitiettacgia_ibfk_2` FOREIGN KEY (`id_Sach`) REFERENCES `sach` (`id`);

--
-- Các ràng buộc cho bảng `donhang`
--
ALTER TABLE `donhang`
  ADD CONSTRAINT `donhang_ibfk_1` FOREIGN KEY (`id_NV`) REFERENCES `nhanvien` (`id`),
  ADD CONSTRAINT `donhang_ibfk_2` FOREIGN KEY (`id_User`) REFERENCES `user` (`id`);

--
-- Các ràng buộc cho bảng `khuyenmai`
--
ALTER TABLE `khuyenmai`
  ADD CONSTRAINT `khuyenmai_ibfk_1` FOREIGN KEY (`id_NV`) REFERENCES `nhanvien` (`id`);

--
-- Các ràng buộc cho bảng `like`
--
ALTER TABLE `like`
  ADD CONSTRAINT `like_ibfk_1` FOREIGN KEY (`id_User`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `like_ibfk_2` FOREIGN KEY (`id_Sach`) REFERENCES `sach` (`id`);

--
-- Các ràng buộc cho bảng `sach`
--
ALTER TABLE `sach`
  ADD CONSTRAINT `sach_ibfk_1` FOREIGN KEY (`id_NXB`) REFERENCES `nhaxuatban` (`id`);

--
-- Các ràng buộc cho bảng `tin`
--
ALTER TABLE `tin`
  ADD CONSTRAINT `tin_ibfk_1` FOREIGN KEY (`id_NV`) REFERENCES `nhanvien` (`id`),
  ADD CONSTRAINT `tin_ibfk_2` FOREIGN KEY (`id_KM`) REFERENCES `khuyenmai` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
