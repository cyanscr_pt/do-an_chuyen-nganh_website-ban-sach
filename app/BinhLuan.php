<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BinhLuan extends Model
{
    protected $table = "binhluan";

  	public function Sach(){
  		return $this->belongsTo('App\Sach','id_Sach','id');
  	}
  	public function user(){
  		return $this->belongsTo('App\User','id_User','id');
  	}
  	public function NhanVien(){
  		return $this->belongsTo('App\NhanVien','id_NV','id');
  	}
}
