<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NhaXuatBan extends Model
{
    protected $table = "nhaxuatban";
 	 public function Sach(){
 	 	return $this->hasMany('App\Sach','id_NXB','id');
 	 }   
}
