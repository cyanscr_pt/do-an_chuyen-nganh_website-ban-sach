<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravelista\Comments\Commenter;
use Illuminate\Contracts\Auth\CanResetPassword;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable,Commenter;
    
    protected $table = "user";
    
    public function User(){
        return $this->hasMany('App\DonHang','id_User','id');//
    }
    public function BinhLuan(){
        return $this->hasMany('App\BinhLuan','id_User','id');
    }

    public function Like(){
        return $this->hasMany('App\Like','id_User','id');
    }
    // public function Like(){
    //     return $this->belongsToMany('App\Sach','Like','id_Sach','id_User');
    // }
}
