<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    protected $table = "like";
    
    public function Sach(){
        return $this->belongsTo('App\Sach','id_Sach','id');
    }
    public function User(){
        return $this->belongsTo('App\User','id_User','id');
    }
    // public function Sach(){
    //     return $this->belongsToMany('Sach');
    // }
    // public function User(){
    //     return $this->belongsToMany('User');
    // }
}
