<?php

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;

class NhanVien extends Authenticatable
{
	use Notifiable;
	protected $table= "nhanvien";

	
	public function BinhLuan(){
		return $this->hasMany('App\BinhLuan','id_NV','id');
	}
	public function DonHang(){
		return $this->hasMany('App\DonHang','id_NV','id');
	}
	public function KhuyenMai(){
		return $this->hasMany('App\KhuyenMai','id_NV','id');
	}
	public function Tin(){
		return $this->hasMany('App\Tin','id_NV','id');
	}
}
