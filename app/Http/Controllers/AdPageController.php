<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\TheLoai;
use App\Sach;
use App\ChiTietLoai;
use App\TacGia;
use App\ChiTietTacGia;
use App\NhaXuatBan;
use App\BinhLuan;
use App\NhanVien;
use App\DonHang;
use App\ChiTietDonHang;
use Illuminate\Support\Str;

class AdPageController extends Controller
{
    function test(){
        $loai= Sach::find(4)->theloai;
        dd($loai);
    }


    function getThongKe(){
        $DH = DonHang::where("TrangThai","=","0")->get();
        $sach = Sach::select('id','TenSach')->get();
        // $banChay = ChiTietDonHang::select('id_Sach','sum(SoLuong) as TongSL')->groupBy('id_Sach')->get();
        $banChay = DB::select('SELECT id_Sach, sum(SoLuong) as TongSL FROM chitietdonhang GROUP BY id_Sach ORDER BY TongSL DESC LIMIT 0,10');
    
               $databanchay= [];
        foreach ($banChay as $value) {
            $soluong = $value->TongSL;
            $idsach= $value->id_Sach;
            $sp= Sach::where('id',$idsach)->get();
            foreach ($sp as $value) {
                $databanchay[]= [
                    "name" => $value->TenSach,
                    "y"    => (int) $soluong
                ];
            }
        }
        return view('AdPage.thongke',[
            'databanchay' => json_encode($databanchay),
            'DH'            => $DH
        ]);
    }

    

    function getDsTacGia(){
        $TacGia = TacGia::all();
        return view('AdPage.TacGia.DsTacGia',compact('TacGia'));
    }// get danh sách tác giả
    
    function getAddTG(){
        return view('AdPage.TacGia.ThemTacGia');
    }
    function postAddTG(){
        $this->validate($this->request,
            [
                'TenTG'             =>'required',
            ],
            [
                'TenTG.required'    =>'Bạn chưa nhập tên tác giả',
            ]);
        $this->validate($this->request,
            [
                'gioithieu'             =>'required',
            ],
            [
                'gioithieu.required'    =>'Bạn chưa nhập giới thiệu tác giả',
            ]);
        $tg= new TacGia;

        $tg->TenTG          =$this->request->TenTG;
        $tg->TenTG_KhongDau =$this->request->TenTG_KhongDau;
        $tg->MoTa           =$this->request->gioithieu;

        $tg->save();
        return redirect('admin/tac-gia/danh-sach')->with('ThongBao','Thêm thành công.');
    }
    function getUpdateTG($id){
        $tg= TacGia::find($id);
        return view('AdPage.TacGia.updateTacGia',compact('tg'));
    }
    function postUpdateTG($id){
        $this->validate($this->request,
            [
                'TenTG'             =>'required',
            ],
            [
                'TenTG.required'    =>'Bạn chưa nhập tên tác giả',
            ]);
        $this->validate($this->request,
            [
                'gioithieu'             =>'required',
            ],
            [
                'gioithieu.required'    =>'Bạn chưa nhập giới thiệu tác giả',
            ]);
        $tg= TacGia::find($id);

        $tg->TenTG          =$this->request->TenTG;
        $tg->TenTG_KhongDau =$this->request->TenTG_KhongDau;
        $tg->MoTa           =$this->request->gioithieu;

        $tg->save();
        return redirect('admin/tac-gia/danh-sach')->with('ThongBao','Sửa thành công.');
    }
    function getDeleteTG($id){
        $tg= TacGia::find($id);
        $tg->delete();
        return redirect('admin/tac-gia/danh-sach')->with('ThongBao','Xóa thành công.');
    }

    function getDsNhaXuatBan(){
        $NXB= NhaXuatBan::all();
        return view('AdPage.DsNhaXuatBan',compact('NXB'));
    }// danh sách nhà xuất bản
    function getAddNhaXuatBan(){
        return view('AdPage.ThemNXB');
    }// thêm nhà xuất bản get

    function postAddNhaXuatBan(){
        $this->validate($this->request,
            [
                'TenNxb'            =>'required',
            ],
            [
                'TenNxb.reqired'    =>'Ban chua nhap ten NXB',
            ]);
        $this->validate($this->request,
            [
                'diachi'            =>'required',
            ],
            [
                'diachi.reqired'    =>'Ban chua nhap dia chi NXB',
            ]);
        $this->validate($this->request,
            [
                'phone'            =>'required',
            ],
            [
                'phone.reqired'    =>'Ban chua nhap SDT NXB',
            ]);
        $nxb= new NhaXuatBan;

        if($this->request->hasFile('image'))
        {
            $file= $this->request->file('image');
            $name= $file->getClientOriginalName();
            $hinh=Str::random(4)."_".$name;
            // while(file_exists('upload/biasach/'.$hinh)){
            //     $hinh=Str::random(4)."_".$name;
            // }
            $file->move('upload/logoNXB/',$hinh);
            $nxb->logo              = $hinh;
            // dd($hinh);
        }else{
            $nxb->logo              = "";
        }

        $nxb->TenNXB          = $this->request->TenNxb;
        $nxb->TenNXB_KhongDau = $this->request->TenNxb_KhongDau;
        $nxb->diachi          = $this->request->diachi;
        $nxb->SDT          = $this->request->phone;

        $nxb->save();

        return redirect('admin/nha-xuat-ban/danh-sach')->with('ThongBao','Them thanh cong');
    }// thêm nhà xuất bản post
    function getUpdateNhaXuatBan($id){
        $nxb= NhaXuatBan::find($id);
        return view('AdPage.updateNXB',compact('nxb'));
    }// update nhà xuất bản
    function postUpdateNhaXuatBan($id){
        $this->validate($this->request,
            [
                'TenNxb'            =>'required',
            ],
            [
                'TenNxb.reqired'    =>'Ban chua nhap ten NXB',
            ]);
        $this->validate($this->request,
            [
                'diachi'            =>'required',
            ],
            [
                'diachi.reqired'    =>'Ban chua nhap dia chi NXB',
            ]);
        $this->validate($this->request,
            [
                'phone'            =>'required',
            ],
            [
                'phone.reqired'    =>'Ban chua nhap SDT NXB',
            ]);
        $nxb= NhaXuatBan::find($id);
        if($this->request->hasFile('image'))
        {
            $file= $this->request->file('image');
            $name= $file->getClientOriginalName();
            $hinh=Str::random(4)."_".$name;
            // while(file_exists('upload/biasach/'.$hinh)){
            //     $hinh=Str::random(4)."_".$name;
            // }
            $file->move('upload/logoNXB/',$hinh);
            $nxb->logo              = $hinh;
            // dd($hinh);
        }else{
            $nxb->logo              = $nxb->logo;
        }
        $nxb->TenNXB          = $this->request->TenNxb;
        $nxb->TenNXB_KhongDau = $this->request->TenNxb_KhongDau;
        $nxb->diachi          = $this->request->diachi;
        $nxb->SDT          = $this->request->phone;

        $nxb->save();

        return redirect('admin/nha-xuat-ban/danh-sach')->with('ThongBao','Sua Thanh Cong');
    }
    function getDeleteNhaXuatBan($id){
        $nxb= NhaXuatBan::find($id);
        $nxb->delete();
        return redirect('admin/nha-xuat-ban/danh-sach')->with('ThongBao','xoa Thanh Cong');
    }
    //
    function getDsLoai(){
        $Loai= TheLoai::all();
        return view('AdPage.DsLoai',compact('Loai'));
    }
    function getAddLoai(){
        return view('AdPage.ThemLoai');
    }
    function postAddLoai(){
        $this->validate($this->request,
            [
                'TenLoai'           =>'required',
            ],
            [
                'TenLoai.required'  =>'Ban chua nhap ten loai',
            ]);
        $theloai=new TheLoai;
        $theloai->TenTL        =$this->request->TenLoai;
        $theloai->TenTL_KhongDau=$this->request->TenLoai_KhongDau;
        $theloai->save();

        return redirect('admin/the-loai/danh-sach')->with('ThongBao','Them thanh cong');
    }// them loai

    function getUpdateLoai($id){
        $Loai= TheLoai::find($id);
        return view('AdPage.updateLoai',compact('Loai'));
    }
    function postUpdateLoai($id){
        $this->validate($this->request,
            [
                'TenLoai'           =>'required',
            ],
            [
                'TenLoai.required'  =>'Ban chua nhap ten loai',
            ]);
        $theloai=TheLoai::find($id);
        $theloai->TenTL        =$this->request->TenLoai;
        $theloai->TenTL_KhongDau=$this->request->TenLoai_KhongDau;
        $theloai->save();

        return redirect('admin/the-loai/danh-sach')->with('ThongBao','update thanh cong');
    }
    function getTenTacGia($id_Sach="4"){
        $ChiTiet= ChiTietTacGia::where('id_Sach',"$id_Sach")->get();
        foreach ($ChiTiet as  $chitiet) {
            $id= $chitiet->id_TG;
            $TacGia = TacGia::where("id_TG","$id")->get();
            echo "<pre>";
            print_r($TacGia);
            foreach ($TacGia as $value) {
                echo $value->TenTG."<br>";
            }
        }
    }
    function getXoaLoai($id){
        $Loai = TheLoai::find($id);
        $Loai->delete();
        return redirect('admin/the-loai/danh-sach')->with('ThongBao','Xoa Thanh cong');
    }

    function getBinhLuan(){
        $BL= BinhLuan::all();
        return view('AdPage.BinhLuan.DsBinhLuan',compact('BL'));
    }
    function getXuLy($id){
        $binhluan=BinhLuan::find($id);

        if($binhluan->TrangThai==1){
           $binhluan->TrangThai=0;
       }else{
        $binhluan->TrangThai=1;
    }
    $binhluan->save();
    return redirect('admin/binh-luan/danh-sach')->with('ThongBao','Cap nhap thanh cong');

}
}
