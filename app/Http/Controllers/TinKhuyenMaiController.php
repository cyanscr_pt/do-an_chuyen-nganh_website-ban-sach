<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\KhuyenMai;
use App\Tin;
use App\Sach;
use Illuminate\Support\Str;

use App\ChiTietKhuyenMai;
class TinKhuyenMaiController extends Controller
{
	public function getKhuyenMaiDs()
	{
        $KM= KhuyenMai::all();
		return view('AdPage.KhuyenMaiTinTuc.KhuyenMaiDs',compact("KM"));
    }
    // Danh sách khuyến mại

	public function getAddKm()
	{
		$Sach= Sach::all();
		return view('AdPage.KhuyenMaiTinTuc.ThemKM',compact('Sach'));
    }
    //get add KM

   
	public function postAddKm()
	{
		$this->validate($this->request,
            [
                'tenKM'             =>'required',
            ],
            [
                'tenKM.required'    =>'Bạn chưa nhập tên chương trình.',
            ]);
		$this->validate($this->request,
            [
                'soluongKM'             =>'required',
            ],
            [
                'soluongKM.required'    =>'Bạn chưa nhập số lượng KM',
            ]);
		$this->validate($this->request,
            [
                'ngaybatdauKM'             =>'required',
            ],
            [
                'ngaybatdauKM.required'    =>'Bạn chưa chọn ngày bắt đầu',
            ]);
		$this->validate($this->request,
            [
                'ngayketthucKM'             =>'required',
            ],
            [
                'ngayketthucKM.required'    =>'Bạn chưa chọn ngày kết thúc',
            ]);
		$this->validate($this->request,
            [
                'sach'             =>'required',
            ],
            [
                'sach.required'    =>'Bạn chưa chọn sách được khuyến mãi',
            ]);
		$this->validate($this->request,
            [
                'phantramKm'             =>'required',
            ],
            [
                'phantramKm.required'    =>'Bạn chưa nhập phần trăm khuyến mãi',
            ]);
		$km= new KhuyenMai;
		$km->Ten_KM						=$this->request->tenKM;
		$km->TrangThai					=$this->request->trangthai;
		$km->NgayBatDau					=$this->request->ngaybatdauKM;
		$km->NgayKetThuc				=$this->request->ngayketthucKM;
		$km->id_NV						=$this->request->id_NV;
		$km->save();

		$idkm= $km->id;

		foreach ($this->request->sach as $value) {
           
			$ctkm= new ChiTietKhuyenMai;

			$ctkm->id_Sach			=$value;
			$ctkm->id_KM			=$idkm;
			$ctkm->SoLuongKM		=$this->request->soluongKM;
			$ctkm->SoLuongConLai	=$this->request->soluongKM;
			$ctkm->DieuKien			=$this->request->ghichu;
			$ctkm->PhanTramKM		=$this->request->phantramKm;

            $ctkm->save();
            
            $sach = Sach::find($value);
            $sach->GiaSale = $sach->Gia * (100- $this->request->phantramKm) /100;
            $sach->save();
		}
		return redirect("admin/khuyen-mai/danh-sach")->with("ThongBao","Đã thêm khuyen mai mã: $idkm");
    } 
    //them khuyen mai
	public function getUpdateKm($id){
		$khuyenMai = KhuyenMai::find($id);
		$Sach= Sach::all();
		return view("AdPage.KhuyenMaiTinTuc.updateKM", compact("khuyenMai","Sach"));

    }
    public function postUpdateKm($id)
    {
        $this->validate($this->request,
        [
            'tenKM'             =>'required',
        ],
        [
            'tenKM.required'    =>'Bạn chưa nhập tên chương trình.',
        ]);
        $this->validate($this->request,
        [
            'soluongKM'             =>'required',
        ],
        [
            'soluongKM.required'    =>'Bạn chưa nhập số lượng KM',
        ]);
        $this->validate($this->request,
        [
            'ngaybatdauKM'             =>'required',
        ],
        [
            'ngaybatdauKM.required'    =>'Bạn chưa chọn ngày bắt đầu',
        ]);
        $this->validate($this->request,
        [
            'ngayketthucKM'             =>'required',
        ],
        [
            'ngayketthucKM.required'    =>'Bạn chưa chọn ngày kết thúc',
        ]);
        $this->validate($this->request,
        [
            'sach'             =>'required',
        ],
        [
            'sach.required'    =>'Bạn chưa chọn sách được khuyến mãi',
        ]);
        $this->validate($this->request,
        [
            'phantramKm'             =>'required',
        ],
        [
            'phantramKm.required'    =>'Bạn chưa nhập phần trăm khuyến mãi',
        ]);
        $km= KhuyenMai::find($id);
        $km->Ten_KM						=$this->request->tenKM;
        $km->TrangThai					=$this->request->trangthai;
        $km->NgayBatDau					=$this->request->ngaybatdauKM;
        $km->NgayKetThuc				=$this->request->ngayketthucKM;
        $km->id_NV						=$this->request->id_NV;
        $km->save();

        $idkm= $km->id;

        foreach ($km->Sach as $value) {

            $sach = Sach::find($value->id);
            $sach->GiaSale = "0";
            $sach->save();
        }

        $km->Sach()->detach();

        foreach ($this->request->sach as $value) {
        
            $ctkm= new ChiTietKhuyenMai;

            $ctkm->id_Sach			=$value;
            $ctkm->id_KM			=$idkm;
            $ctkm->SoLuongKM		=$this->request->soluongKM;
            $ctkm->SoLuongConLai	=$this->request->soluongKM;
            $ctkm->DieuKien			=$this->request->ghichu;
            $ctkm->PhanTramKM		=$this->request->phantramKm;

            $ctkm->save();
            
            $sach = Sach::find($value);
            $sach->GiaSale = $sach->Gia * (100- $this->request->phantramKm) /100;
            $sach->save();
        }
        return redirect("admin/khuyen-mai/danh-sach")->with("ThongBao","Đã Sửa Khuyến Mãi mã: $idkm");
    }




    //Tin Tuc
	public function getDsTin()
	{
		$Tin= Tin::all();
		return view("AdPage.KhuyenMaiTinTuc.TinTucDS",compact("Tin"));
	}
	public function getAddTin(){
		$KhuyenMai= KhuyenMai::select('id','Ten_KM')->get();
		// dd($KhuyenMai);
		return view("AdPage.KhuyenMaiTinTuc.ThemTin",compact("KhuyenMai"));
	}
	public function postAddTin(){

		$this->validate($this->request,
            [
                'title'             =>'required',
            ],
            [
                'title.required'    =>'Bạn chưa nhập tiêu đề tin.',
			]);
			$this->validate($this->request,
            [
                'Title_KhongDau'             =>'required',
            ],
            [
                'Title_KhongDau.required'    =>'Bạn chưa nhập nội dung tin',
            ]);
			$this->validate($this->request,
            [
                'id_KM'             =>'required',
            ],
            [
                'id_KM.required'    =>'Bạn chưa chọn khuyến mãi.',
			]);
			$this->validate($this->request,
            [
                'image'             =>'required',
            ],
            [
                'image.required'    =>'Bạn chưa chọn hình ảnh tiêu đề',
			]);
			$this->validate($this->request,
            [
                'NoiDung'             =>'required',
            ],
            [
                'NoiDung.required'    =>'Bạn chưa nhập nội dung tin',
            ]);
		$tin = new Tin;

		if($this->request->hasFile('image'))
        {
            $file= $this->request->file('image');
            $name= $file->getClientOriginalName();
            $hinh=Str::random(4)."_".$name;
            // while(file_exists('upload/biasach/'.$hinh)){
            //     $hinh=Str::random(4)."_".$name;
            // }
            $file->move('upload/image_tin/',$hinh);
            $tin->urlHinh              = $hinh;
            // dd($hinh);
        }else{
            $tin->urlHinh              = "";
		}
		$tin->TenTin			=$this->request->title;
		$tin->TenTin_KhongDau	=$this->request->Title_KhongDau;
		$tin->TrangThai			="1";
		$tin->NoiDungTin		=$this->request->NoiDung;
		$tin->id_KM				=$this->request->id_KM;
		$tin->id_NV				=$this->request->id_NV;

		$tin->save();
		return redirect('admin/tin/danh-sach')->with('ThongBao','Đã thêm tin tưc.');
    }
    
    public function getUpdateTin($id){
        $tin= Tin::find($id);
        $KhuyenMai= KhuyenMai::select('id','Ten_KM')->get();
        return view('AdPage.KhuyenMaiTinTuc.updatetin',compact('tin','KhuyenMai'));
    }

    public function postUpdateTin($id){
        $this->validate($this->request,
            [
                'title'             =>'required',
            ],
            [
                'title.required'    =>'Bạn chưa nhập tiêu đề tin.',
			]);
			$this->validate($this->request,
            [
                'Title_KhongDau'             =>'required',
            ],
            [
                'Title_KhongDau.required'    =>'Bạn chưa nhập nội dung tin',
            ]);
			$this->validate($this->request,
            [
                'id_KM'             =>'required',
            ],
            [
                'id_KM.required'    =>'Bạn chưa chọn khuyến mãi.',
			]);
			$this->validate($this->request,
            [
                'NoiDung'             =>'required',
            ],
            [
                'NoiDung.required'    =>'Bạn chưa nhập nội dung tin',
            ]);
		$tin = Tin::find($id);

		if($this->request->hasFile('image'))
        {
            $file= $this->request->file('image');
            $name= $file->getClientOriginalName();
            $hinh=Str::random(4)."_".$name;
            // while(file_exists('upload/biasach/'.$hinh)){
            //     $hinh=Str::random(4)."_".$name;
            // }
            $file->move('upload/image_tin/',$hinh);
            $tin->urlHinh              = $hinh;
            // dd($hinh);
        }else{
            $tin->urlHinh              = $tin->urlHinh;
		}
		$tin->TenTin			=$this->request->title;
		$tin->TenTin_KhongDau	=$this->request->Title_KhongDau;
		$tin->TrangThai			=$tin->TrangThai	;
		$tin->NoiDungTin		=$this->request->NoiDung;
		$tin->id_KM				=$this->request->id_KM;
		$tin->id_NV				=$tin->id_NV;

		$tin->save();
		return redirect('admin/tin/danh-sach')->with('ThongBao','Sửa thành công');
    }

    public function getxuly($id){
        $Tin = Tin::find($id);
        if($Tin->TrangThai==1){
            $Tin->TrangThai =0;
        }elseif ($Tin->TrangThai==0) {
            $Tin->TrangThai =1;
        }

        $Tin->save();
        return redirect('admin/tin/danh-sach')->with('ThongBao',"Đã update trạng thái tin: $Tin->id");
    }

}
