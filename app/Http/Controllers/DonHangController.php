<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TheLoai;
use App\Sach;
use App\ChiTietLoai;
use App\TacGia;
use App\ChiTietTacGia;
use App\NhaXuatBan;
use App\BinhLuan;
use App\NhanVien;
use App\DonHang;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;


class DonHangController extends Controller
{
	public function getDonHangDs(){
		$DH= DonHang::all();
		return view("AdPage.DonHang.donhang",compact("DH"));
    }
    public function getXuLy($id, $idnv){
    	$DH= DonHang::find($id);
    	if($DH->TrangThai==0){
			$DH->TrangThai=1;
			$DH->id_NV= $idnv;
    	}elseif ($DH->TrangThai==1) {
			$DH->TrangThai=2;
			
            foreach ($DH->sach as $value) {
				$sl =$value->pivot->SoLuong;
                $id_s = $value->id;
                $S= Sach::find($id_s);
                $S->SoLuong = $S->SoLuong - $sl;
                $S->save();
            }
    	}elseif ($DH->TrangThai==2) {
    		$DH->TrangThai=3;
    	}
    	$DH->save();
    	return redirect('admin/don-hang/danh-sach')->with("ThongBao","Đã cập nhật trạng thái của đơn hàng có mã: $DH->id");
    }
    public function getXuLyHuy($id){
    	$DH= DonHang::find($id);
    	if($DH->TrangThai==0||$DH->TrangThai==1 ||$DH->TrangThai==1){
    		$DH->TrangThai=4;
		}elseif($DH->TrangThai!=0 || $DH->TrangThai!=4 || $DH->TrangThai!=1){
			$DH->TrangThai=5;
			$DH->GhiChu ="$DH->GhiChu <br/> Hủy: Không Giao Được Hàng";
			foreach ($DH->sach as $value) {
                $sl =$value->pivot->SoLuong;
                $id_s = $value->id;
                $S= Sach::find($id_s);
                $S->SoLuong = $S->SoLuong + $sl;
                $S->save();
            }
		}
		
    	$DH->save();
    	return redirect('admin/don-hang/danh-sach')->with("ThongBao","Đã hủy đơn hàngđơn hàng có mã: $DH->id.");
	}
	
}
