<?php

namespace App\Http\Controllers;
use App\NhanVien;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class NVController extends Controller
{
    public function getDsNhanVien(){
        $NhanVien= NhanVien::all();
        return view('AdPage.NhanVien.DsNhanVien', compact("NhanVien"));
    }
    public function xuly($id){
        $nhanvien = NhanVien::find($id);
        if($nhanvien->TrangThai ==0){
            $nhanvien->TrangThai =1;
            $nhanvien->save();
            return redirect("admin/nhan-vien/danh-sach")->with("ThongBao","Đã kích hoạt tài khoản: $nhanvien->email");
        }else{
            $nhanvien->TrangThai =0;
            $nhanvien->save();
            return redirect("admin/nhan-vien/danh-sach")->with("ThongBao","Đã khóa tài khoản: $nhanvien->email");
        }
    }
    public function getaddNV()
    {
        return view('AdPage.NhanVien.addNv');
    }
    public function postaddNV()
    {
        $this->validate($this->request,
        [
            'Ten'             =>'required',
        ],
        [
            'Ten.required'    =>'Bạn chưa nhập tên.',
        ]);
        $this->validate($this->request,
        [
            'email'             =>'required',
        ],
        [
            'email.required'    =>'Bạn chưa nhập email.',
        ]);   
        $this->validate($this->request,
        [
            'SDT'             =>'required',
        ],
        [
            'SDT.required'    =>'Bạn chưa nhập số điện thoại.',
        ]);   
        $this->validate($this->request,
        [
            'ngaysinh'             =>'required',
        ],
        [
            'ngaysinh.required'    =>'Bạn chưa nhập ngày sinh',
        ]);
        $this->validate($this->request,
        [
            'diachi'             =>'required',
        ],
        [
            'diachi.required'    =>'Bạn chưa nhập địa chỉ.',
        ]);
        $this->validate($this->request,
        [
            'chucvu'             =>'required',
        ],
        [
            'chucvu.required'    =>'Bạn chưa chọn chức vụ',
        ]);
        $this->validate($this->request,
        [
            'matkhau'             =>'required',
        ],
        [
            'matkhau.required'    =>'Bạn chưa nhập mật khẩu',
        ]);

        $nhanvien= new NhanVien;
        
        $nhanvien->TenNV            = $this->request->Ten;
        $nhanvien->email            = $this->request->email;
        $nhanvien->password         = $this->request->matkhau;
        $nhanvien->ChucVu           = $this->request->chucvu;
        $nhanvien->SDT           = $this->request->SDT;
        $nhanvien->DiaChi           = $this->request->diachi;
        $nhanvien->NgaySinh           = $this->request->ngaysinh;
        $nhanvien->TrangThai           = 1;
        
        $nhanvien->save();
        return redirect("admin/nhan-vien/danh-sach")->with("ThongBao","Thêm Thành Công");
    }
}
