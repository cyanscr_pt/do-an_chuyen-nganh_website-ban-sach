<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\TheLoai;
use App\Sach;
use App\ChiTietLoai;
use App\TacGia;
use App\ChiTietTacGia;
use App\NhaXuatBan;
use App\BinhLuan;
use App\NhanVien;
use Illuminate\Support\Str;

class SachController extends Controller
{
	function addTg($tentg){
		
			$tg= new TacGia;
			$tg->TenTG 			= $tentg;
			$tg->TenTG_KhongDau		= stripUnicode($tentg);
			$tg->save();
			echo 1;
	}
	function addTl($tentl)
	{
		$tl= new TheLoai;
		$tl->TenTL 			= $tentl;
		$tl->TenTL_KhongDau =stripUnicode($tentl);
		$tl->save();
		echo 1;
	}
	function addNxb($tennxb)
	{
		$nxb= new NhaXuatBan;
		$nxb->TenNXB 			=$tennxb;
		$nxb->TenNXB_KhongDau 	=stripUnicode($tennxb);
		$nxb->save();
		echo 1;
	}
	function getXoaSach($id){
        $sach = Sach::find($id);
        $sach->TacGia()->detach();
        $sach->TheLoai()->detach();
        $sach->delete();
        return redirect('admin/sach/danh-sach')->with('ThongBao','Xóa thành công.');
    }
    function getDsSach(){
        $Sach= Sach::all();
       // dd($Sach);

        return view('AdPage.DsSach',['Sach' => $Sach]);
    }//sach

    function getAddSach(){
        $TacGia= TacGia::all();
        $NXB= NhaXuatBan::all();
        $TL= TheLoai::all();
        // dd($TacGia);
        return view("AdPage.Sach.ThemSach",compact('TacGia','NXB','TL'));
    }
    public function postAddSach(){
		
        $this->validate($this->request,
            [
                'TenSach'             =>'required',
            ],
            [
                'TenSach.required'    =>'Bạn chưa nhập tên sách.',
            ]);
        $this->validate($this->request,
            [
                'NXB'             =>'required',
            ],
            [
                'NXB.required'    =>'Bạn chưa chưa chọn nhà xuất bản.',
            ]);
        $this->validate($this->request,
            [
                'lang'             =>'required',
            ],
            [
                'lang.required'    =>'Bạn chưa chon ngôn ngữ.',
            ]);
        $this->validate($this->request,
            [
                'gia'             =>'required',
            ],
            [
                'gia.required'    =>'Bạn chưa nhập giá bán.',
            ]);
        $this->validate($this->request,
            [
                'soluong'             =>'required',
            ],
            [
                'soluong.required'    =>'Bạn chưa nhập số lượng sách.',
			]);
			$this->validate($this->request,
            [
                'tacgia'             =>'required',
            ],
            [
                'tacgia.required'    =>'Bạn chưa chọn tác giả.',
			]);
			$this->validate($this->request,
            [
                'theloai'             =>'required',
            ],
            [
                'theloai.required'    =>'Bạn chưa chọn số lượng',
            ]);
        $sach= new Sach;
        if($this->request->hasFile('image'))
        {
            $file= $this->request->file('image');
            $name= $file->getClientOriginalName();
            $hinh=Str::random(4)."_".$name;
            // while(file_exists('upload/biasach/'.$hinh)){
            //     $hinh=Str::random(4)."_".$name;
            // }
            $file->move('upload/biasach/',$hinh);
            $sach->urlHinh              = $hinh;
            // dd($hinh);
        }else{
            $sach->urlHinh              = "";
        }
        $sach->TenSach              = $this->request->TenSach;
        $sach->TenSach_KhongDau     = ($this->request->TenSach_KhongDau == "")?stripUnicode($this->request->TenSach):$this->request->TenSach_KhongDau;
        $sach->DichGia              = $this->request->dichgia;
        $sach->lang                 = $this->request->lang;
        $sach->KichThuoc            = $this->request->kichthuoc;
        $sach->SoTrang              = $this->request->sotrang;
        $sach->LoaiBia              = $this->request->bia;
        $sach->SKU                  = $this->request->sku;
        $sach->Gia                  = $this->request->gia;
        $sach->TrangThai            = $this->request->trangthai;
        $sach->SoLuong              = $this->request->soluong;
        $sach->GioiThieu            = $this->request->gioithieusach;
        $sach->id_NXB               = $this->request->NXB;

        $sach->save();
        $idsach= $sach->id;
        $tg=$this->request->tacgia;
        $tl=$this->request->theloai;

		if($tg !=null){
			foreach ($tg as $value) {
			$chitietTG = new ChiTietTacGia;
			$chitietTG->id_TG = $value;
			$chitietTG->id_Sach= $idsach;
			$chitietTG->save();
			}
		}
		if($tl !=null){
			foreach ($tl as $value) {
				$chitietloai = new ChiTietLoai;
				$chitietloai->id_Loai = $value;
				$chitietloai->id_Sach= $idsach;
				$chitietloai->save();
			}
		}
		// if($this->request->tentacgianew !=""){
		// 	$idtgnew;
		// 	//them tac gia moi
		// 	$tg= new TacGia;
		// 	$tg->TenTG 			= $this->request->tentacgianew;
		// 	$tg->TenTg_KhongDau	= stripUnicode($this->request->tentacgianew);
		// 	$tg->save();
		// 	$idtgnew= $tg->id;
			
		// 	//them sach vao bang chi tiet tac gia
		// 	$CTTG = new ChiTietTacGia;
		// 	$CTTG->id_TG = $idtgnew;
		// 	$CTTG->id_Sach= $idsach;
		// 	$CTTG->save();
		// }
		// if($this->request->tentheloainew !=""){
		// 	$idtlnew;
		// 	//them tac the loai moi
		// 	$tl= new TheLoai;
		// 	$tl->TenTL 			= $this->request->tentheloainew;
		// 	$tl->TenTL_KhongDau	="tesst";
		// 	$tl->save();
		// 	$idtlnew= $tl->id;
			
		// 	//them sach vao bang chi tiet the loai
		// 	$CTTL = new ChiTietLoai;
		// 	$CTTL->id_Loai = $idtlnew;
		// 	$CTTL->id_Sach= $idsach;
		// 	$CTTL->save();
		// }
     return redirect('admin/sach/danh-sach')->with('ThongBao','Thêm thành công.');
    }// them sach post
	function getUpdateSach($id){
		$Sach= Sach::find($id);
		
		$TacGia= TacGia::all();
		$NXB= NhaXuatBan::all();
		$TL= TheLoai::all();
        // dd($TacGia);
		return view("AdPage.Sach.SuaSach",compact('Sach','TacGia','NXB','TL'));
	}
	public function postUpdateSach(Request $request, $id){
		$this->validate($this->request,
			[
				'TenSach'             =>'required',
			],
			[
				'TenSach.required'    =>'Bạn chưa nhập tên sách.',
			]);
		$this->validate($this->request,
			[
				'TenSach_KhongDau'             =>'required',
			],
			[
				'TenSach_KhongDau.required'    =>'Bạn chưa nhập tên sách không dấu.',
			]);
		$this->validate($this->request,
			[
				'tacgia'             =>'required',
			],
			[
				'tacgia.required'    =>'Bạn chưa chọn tác giả.',
			]);
		$this->validate($this->request,
			[
				'theloai'             =>'required',
			],
			[
				'theloai.required'    =>'Bạn chưa chưa chọn thể loại.',
			]);
		$this->validate($this->request,
			[
				'NXB'             =>'required',
			],
			[
				'NXB.required'    =>'Bạn chưa chưa chọn nhà xuất bản.',
			]);
		$this->validate($this->request,
			[
				'lang'             =>'required',
			],
			[
				'lang.required'    =>'Bạn chưa chon ngôn ngữ.',
			]);
		$this->validate($this->request,
			[
				'gia'             =>'required',
			],
			[
				'gia.required'    =>'Bạn chưa nhập giá bán.',
			]);
		$this->validate($this->request,
			[
				'soluong'             =>'required',
			],
			[
				'soluong.required'    =>'Bạn chưa nhập số lượng sách.',
			]);


		$sach=Sach::find($id);
		$hinhold= $sach->urlHinh;
		if($this->request->hasFile('image'))
		{
			$file= $this->request->file('image');
			$name= $file->getClientOriginalName();
			$hinh=Str::random(4)."_".$name;
            // while(file_exists('upload/biasach/'.$hinh)){
            //     $hinh=Str::random(4)."_".$name;
            // }
			$file->move('upload/biasach/',$hinh);
			$sach->urlHinh              = $hinh;
            // dd($hinh);
		}else{
			$sach->urlHinh              = $hinhold;
		}
		$sach->TenSach              = $this->request->TenSach;
		$sach->TenSach_KhongDau     = $this->request->TenSach_KhongDau;
		$sach->DichGia              = $this->request->dichgia;
		$sach->lang                 = $this->request->lang;
		$sach->KichThuoc            = $this->request->kichthuoc;
		$sach->SoTrang              = $this->request->sotrang;
		$sach->LoaiBia              = $this->request->bia;
		$sach->SKU                  = $this->request->sku;
		$sach->Gia                  = $this->request->gia;
		$sach->TrangThai            = $this->request->trangthai;
		$sach->SoLuong              = $this->request->soluong;
		$sach->GioiThieu            = $this->request->gioithieusach;
		$sach->id_NXB               = $this->request->NXB;

		$sach->save();
		$idsach= $sach->id;
		$tg=$this->request->tacgia;
		$tl=$this->request->theloai;

		$sach->TacGia()->detach();
		$sach->TheLoai()->detach();

		// $sach->theloai->detach();
		foreach ($tg as $value) {

			$chitietTG = new ChiTietTacGia;
         // dd($chitietTG);
			$chitietTG->id_TG = $value;
			$chitietTG->id_Sach= $idsach;
			$chitietTG->save();
		}
		foreach ($tl as $value) {
			$chitietloai = new ChiTietLoai;
			$chitietloai->id_Loai = $value;
			$chitietloai->id_Sach= $idsach;
			$chitietloai->save();
		}

		return redirect('admin/sach/danh-sach')->with('ThongBao','Sửa thành công.');
    }// them sach post
}
