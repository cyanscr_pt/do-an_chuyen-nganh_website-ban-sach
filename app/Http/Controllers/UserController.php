<?php

namespace App\Http\Controllers;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getDsUser(){
        $User = User::all();
        return view('AdPage.User.DsUser', compact('User'));
    }
    public function xulyUser($id)
    {
        $User = User::find($id);
        if($User->active ==0){
            $User->active =1;
            $User->save();
            return redirect("admin/user/getdsuser")->with("ThongBao","Đã kích hoạt User $User->email");
        }else{
            $User->active =0;
            $User->save();
            return redirect("admin/user/getdsuser")->with("ThongBao","Đã khóa User $User->email");
        }
    }
}
