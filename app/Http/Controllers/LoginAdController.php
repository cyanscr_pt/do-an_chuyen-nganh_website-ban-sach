<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\NhanVien;
class LoginAdController extends Controller
{
    public function loginAd(){
    	return view("AdPage.login");
    }
    public function postloginAd(){
    	

    	if(Auth::guard('QuanTri')->attempt(['email'=> $this->request->email, 
    		'password'=> $this->request->password]))
    	{
    		return redirect('admin/thong-ke');
    	}else {
    		return redirect('login-admin');
    	}
    }
    public function getlogOutAd(){
        Auth::guard('QuanTri')->logout();
        return redirect('login-admin');
    }
}
