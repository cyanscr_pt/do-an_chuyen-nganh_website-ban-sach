<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\Tin;
use App\TheLoai;
use App\Sach;
use App\ChiTietLoai;
use App\TacGia;
use App\ChiTietTacGia;
use App\NhaXuatBan;
use App\BinhLuan;
use App\KhuyenMai;
use App\Cart;
use App\User;
use App\Like;
use App\DonHang;
use App\ChiTietDonHang;
use Session;
use Carbon\Carbon;

class frontendController extends Controller
{
    public function getPageAbout(){
        return view('Page.Subpage.about');
    }
    //
    public function getIndex(){
        $SachNew = Sach::select('id','TenSach','urlHinh','Gia','GiaSale')
                                ->orderby('id','desc')
                                ->limit(10)
                                ->get();
        $Banner= Tin::select('id','urlHinh','TenTin')
                        ->where('TrangThai',1)
                        ->get();
        $KhuyenMai= KhuyenMai::where('TrangThai','1')->get();
        // foreach ($KhuyenMai as $value) {
        //     print_r($value->Tin);
        // }
        $Sach = Sach::select('id','TenSach','urlHinh','Gia','GiaSale')->get();
        // 
        $Like= like::all();
        $NXB= NhaXuatBan::select('id','logo')->get();
        return view('Page.Subpage.trang-chu', compact('SachNew','Banner','KhuyenMai','Sach','Like','NXB'));
    }

    public function getPageTatCaSach(){
        $Sach = DB::table('sach')->where('TrangThai','=', 1)->paginate(12);
        return view('Page.Subpage.tat-ca-sach',compact('Sach'));
    }

    public function getChiTietSach(Request $req){
        $sach = Sach::where('id',$req->id)->first();
        return view('Page.Subpage.chi-tiet-sach',compact('sach'));
    }

    public function getAllBlog(){
        $Blogs = DB::table('tin')->where('TrangThai','=', 1)->paginate(12);
        return view('Page.Subpage.blog',compact('Blogs'));
    }

    public function getBlogDetail(Request $req){
        $blog = Tin::where('id',$req->id)->first();
        $DanhSachIDsKhuyenMai = DB::Table('chitietkhuyenmai')->where('id_KM',$blog->id_KM)->get();
        $DanhSachSPKhuyenMai = [];
        foreach($DanhSachIDsKhuyenMai as $sach){
            //dd($sach->id_Sach);
            $DanhSachSPKhuyenMai[] = DB::Table('sach')->where('id',$sach->id_Sach)->get()->toArray();
        }
        //dd($DanhSachSPKhuyenMai);
        return view('Page.Subpage.blog-detail',compact('blog','DanhSachSPKhuyenMai'));
    }

    public function get404(){
        return view('Page.Subpage.404');
    }

    public function getPageLienHe(){
        return view('Page.Subpage.lien-he');
    }


    public function getPageLogin(){
        return view('Page.Subpage.login');
    }

    public function postPageLogin(Request $req){
        // if (Auth::viaRemember()) {
        //     //TODO xử ký nhớ người dùng
        //     return redirect('/')->with('ThongBao','Đăng nhập thành công');
        // } else {
        //
        // }
        
        $this->validate($req,
            [
                'email'=> 'required|email',
                'password' => 'required'
            ],[
                'email.required'=> 'Vui lòng nhập email để đăng nhập',
                'password' => 'Vui lòng nhập password để đăng nhập'
            ]
        );
        $credential = [
            'email'=>$req->email,
            'password'=>$req->password,
            // 'TrangThai' => 1,
        ];
        $remember = $req->remember;
        if(Auth::attempt($credential, $remember)){
            // dd(Auth::user()->tenUser);
            return redirect('/')->with('ThongBao','Đăng nhập thành công');
        }
         else {
            if(Auth::attempt(['email'=>$req->email,'password'=>$req->password])){
                return redirect()->back()->with('ThongBao','Tài khoản chưa kích hoạt');
            } else {
                return redirect()->back()->with('ThongBao','Đăng nhập thất bại');
            }
        }
    }

    public function getDangxuat(){
        $user = Auth::user();
        $user->remember_token = null;
        $user->save();
        Auth::logout();
        return redirect('/');
    }

    public function getPageDangKi(){
        return view('Page.Subpage.dang-ki');
    }

    public function postPageDangKi(Request $req){
        //Validate form
        //TODO xử lý checkbox
        $req->validate([
            'email'=>'required|email|unique:user,id',
            'password'=>'required|min:6',
            'tenUser'=>'required',
            'repassword'=>'required|same:password',
            'agree' => 'required',
            'NgaySinh' =>'before: 13 years ago',
        ],[
            'email.required'=>'Vui lòng nhập email',
            'email.email'=>'Vui lòng nhập email đúng định dạng',
            'email.unique'=>'email này đã đăng ký tài khoản',
            'password.required'=>'Vui lòng nhập password',
            'password.min'=>'Password cần sử dụng ít nhất 6 kí tự',
            'tenUser.required'=>'Vui lòng nhập tên người dùng',
            'repassword.require'=>'Vui lòng nhập lại mật khẩu một lần nữa',
            'password.same'=>'Mật khẩu không giống nhau',
            'agree.required' => 'Bạn phải đồng ý với điều khoản sử dụng',
            'NgaySinh.before'=> 'bạn phải trên 13 tuổi mới được sử dụng',
        ]);

        if( User::where('email',$req->email)->count() > 0){
            return redirect('dang-ki')->with('ThongBao','email đã tồn tại');
        } else{
            $user = new User;
            $user->email = $req->email;    
            $user->tenUser =$req->tenUser;
            $user->password= Hash::make($req->password);
            $user->SDT = $req->SDT;
            $user->DiaChi = $req->DiaChi;
            $user->NgaySinh = date($req->NgaySinh);
            $user->save();
            
            return redirect('login')->with('ThongBao','Đã tạo tài khoản thành công!');
        }
    }
    public function postHuyDonHang(Request $req){
        if(Auth::check()){
            $id = $req->id;
            //dd($id);
            $donhang = DonHang::find($id);
            $donhang->TrangThai = 4;
            $donhang->save();
            //dd($donhang);
            return redirect('tai-khoan');
        }
    }
    public function layDanhSachDonHang(){
        if(Auth::check()){
            $user = Auth::user();
            $result = array();
            $DanhSachDonHang = DonHang::where('id_User',$user->id)->get();
            foreach($DanhSachDonHang as $donhang){
                $chitietdh = ChiTietDonHang::where('id_DH',$donhang->id)->get();
                $donhang["chitiet"] = $chitietdh;
                foreach($donhang["chitiet"] as $chitiet){
                    $chitiet["sach"] = Sach::find($chitiet['id_Sach']);
                }
            }
            //dd($DanhSachDonHang);
            return $DanhSachDonHang;
        }
        return null;
    }

    public function getPageTaiKhoan(){
        if(Auth::check()){
            $user = Auth::user();
            $lichSuMuaHang = self::layDanhSachDonHang();
            return view('Page.Subpage.tai-khoan',compact('user','lichSuMuaHang'));
        }
        else 
            return view('Page.Subpage.login');
    }

    public function getChiTietDonHang($id){
        if(Auth::check()){
            $chitietdh = ChiTietDonHang::where('id_DH',$id)->get();
            $donhang["chitiet"] = $chitietdh;
            $thongtindonhang = DonHang::where('id',$id)->get();
            $thongtindonhang =$thongtindonhang[0];
            foreach($donhang["chitiet"] as $chitiet){
                $chitiet["sach"] = Sach::find($chitiet['id_Sach']);
            }
            return view('Page.Subpage.tai-khoan-chi-tiet-don-hang',compact('donhang','id','thongtindonhang'));
        }
        else
        return redirect('/');
    }

    public function postPageTaiKhoan(Request $req){
        $user = Auth::user();
        $current_password = Auth::User()->password; 

        if( Hash::check($req->old_password,$current_password)){
            $user->tenUser = $req->tenUser;
            $user->SDT = $req->SDT;
            $user->DiaChi = $req->DiaChi;
            $user->NgaySinh = $req->NgaySinh;
            
            //Đổi mật khẩu
            if($req->new_password != ""){
                $req->validate([
                    'new_password'=>'min:6',
                    'confirm_new_password'=>'same:new_password'
                ],[
                    'new_password.min' => 'Password cần sử dụng ít nhất 6 kí tự',
                    'confirm_new_password.same' => 'Xác nhận Password không trùng nhau',
                ]);
                $user->password = Hash::make($req->new_password); 
            }

            $user->save();
            return redirect('tai-khoan')->with('ThongBao','Đã lưu thông tin tài khoản!');
        }
        return redirect('tai-khoan')->with('ThongBao','Lưu thông tin tài khoản thất bại!');
    }

    public function getAddToCart(Request $req, $id){
        $sach = Sach::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->add($sach,$id);
        $req->session()->put('cart',$cart);
        return redirect()->back();
    }

    public function getAddAmountToCart(Request $req, $id, $amount){
        $sach = Sach::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->addamount($sach,$id,$amount);
        $req->session()->put('cart',$cart);
        return redirect()->back();
    }

    public function getUpdatetocart(Request $req, $id, $Qty){
        $sach = Sach::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->update($id, $Qty);
        $req->session()->put('cart',$cart);
        echo 1;
        //return redirect('gio-hang');
    }

    public function getRemoveFromCart(Request $req, $id){
        $sach = Sach::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->reduceByOne($id);
        if (count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else {
            Session::forget('cart');
        }
        return redirect()->back();
    }

    public function getDeleteFromCart($id){
        $sach = Sach::find($id);
        $oldCart = Session('cart')?Session::get('cart'):null;
        $cart = new Cart($oldCart);
        $cart->removeItem($id);
        if (count($cart->items)>0){
            Session::put('cart',$cart);
        }
        else {
            Session::forget('cart');
        }
        return redirect()->back();
    }

    public function getSearch(Request $req){
        $tukhoa = $req->key;
        Session::put('key',$tukhoa);
        $Sach = DB::table('sach')->where(
            'TenSach',
            'like',
            '%'.$req->key.'%')->paginate(12);
        return view('Page.Subpage.tat-ca-sach',compact('Sach'));
    }
    //view Nha xuat ban trang chu
    public function getviewNXB($id){
        $Sach = DB::table('sach')->where('id_NXB','=',$id)->paginate(12);
        // $Sach= Sach::where('id_NXB','=',$id);
        return view('Page.Subpage.tat-ca-sach',compact('Sach'));
    }

    public function getSachofTL($id){
        $tl=  TheLoai::find($id);
        $Sach= $tl->Sach()->paginate(12);
        return view('Page.Subpage.tat-ca-sach',compact('Sach'));
    }

    public function xulylike($id){
        $id_u =Auth::user()->id;
        $Like = Like::where('id_Sach','=',$id,'and','id_User','=',$id_u)->count();

        // dd($Like);
        if($Like == 0){
            $like= new Like;
            $like->id_Sach = $id;
            $like->id_User = $id_u;
            $like->save();
            return redirect()->back()->with('ThongBao','Đã Thích');

        }else{
            $L = Like::where('id_Sach','=',$id,'and','id_User','=',$id_u)->delete();
            return redirect()->back()->with('ThongBao','Đã Bỏ Thích');
        }
    }

    public function getPageGioHang(){
        $oldCart = Session('cart');
        $cart = new Cart($oldCart);
        //dd($cart);
        
        return view('Page.Subpage.gio-hang')->with([
            'cart'=>Session::get('cart'),
            'product_cart'=>$cart->items,
            'totalPrice'=>$cart->totalPrice,
            'totalQty'=>$cart->totalQty
            ]);
    }
    
    public function getCheckout(){
        if(Auth::check()){
            $user = Auth::user();
            return view('Page.Subpage.gio-hang-checkout',compact('user'));
        }
        return redirect('login');
    }

    public function postCheckout(Request $req){
        //Validate form
        //TODO xử lý checkbox
        $req->validate([
            'name'=>'required',
            'sdt'=>'required',
            'diachi'=>'required'
        ],[
            'name.required'=>'Vui lòng nhập tên người nhận',
            'sdt.required'=>'Vui lòng nhập số điện thoại người nhận',
            'diachi.required'=>'Vui lòng nhập địa chỉ để nhận hàng'
        ]);

        //TODO xử lý nếu thông tin user trống thì bổ sung
        //nếu có rồi thì hiện ra form
        //if
        $user = Auth::user();
        $user->DiaChi = $req->diachi;
        $user->SDT = $req->sdt;
        $hoten = $req->name;
        $ghichu = $req->ghichu;

        //TODO thêm thông tin nhận hàng vào session
        Session::put('donhang',[
            'user'=> $user,
            'hoten'=> $hoten,
            'ghichu'=> $ghichu,
            'cart'=> Session::get('cart')
        ]);
        return redirect('complete-order');
    }

    public function getCompleteOrder(){
        if( Session::has('cart')){
            $oldCart = Session('cart')?Session::get('cart'):null;
            $cart = new Cart($oldCart);
            return view('Page.Subpage.gio-hang-complete-order')->with([
                'cart'=>Session::get('cart'),
                'product_cart'=>$cart->items,
                'totalPrice'=>$cart->totalPrice,
                'totalQty'=>$cart->totalQty
                ]);
        } else {
            return redirect('gio-hang');
        }
    }

    public function postCompleteOrder(){
        $donhang = Session::get('donhang');

        //dd($donhang);

        $DH = new DonHang;
        $DH->id_User = $donhang['user']['id'];
        $DH->TenNguoiNhan = $donhang['hoten'];
        $DH->TongTien = $donhang['cart']->totalPrice;
        $DH->GhiChu = $donhang['ghichu'];
        $DH->DiaChiNhanHang = $donhang['user']['DiaChi'];
        $DH->SDT = $donhang['user']['SDT'];
        $DH->TrangThai = 0;
        $DH->save();

        foreach($donhang['cart']->items as $item){
            $chitietdh = new ChiTietDonHang;
            $chitietdh->id_DH = $DH->id;
            $chitietdh->id_Sach = $item['item']->id;
            $chitietdh->SoLuong = $item['Qty'];
            $chitietdh->Gia = $item['price'];
            $chitietdh->save();
        }
        Session::forget('cart');
        return redirect('/');
    }

    
}