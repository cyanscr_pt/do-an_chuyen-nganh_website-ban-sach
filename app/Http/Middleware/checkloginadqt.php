<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class checkloginadqt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::guard('QuanTri')->User()->ChucVu=="qtv")
        {
            
             return $next($request);
           
        }
        else
        {
            return redirect('admin/thong-ke')->with("ThongBao","Bạn có quyền vào trang này");
        }
    }  
}