<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonHang extends Model
{
    protected $table = "donhang";

    public function Sach(){
        return $this->belongsToMany('App\Sach','chitietdonhang','id_DH','id_Sach')->withPivot('SoLuong','Gia');
    }
    
    public function User(){
    	return $this->belongsTo('App\User','id_User','id');//
    }
    public function NhanVien(){
    	return $this->belongsTo('App\NhanVien','id_NV','id');//
    }
}
