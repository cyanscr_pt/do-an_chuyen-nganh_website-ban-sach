<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

use Session;

use App\TheLoai;
use App\Cart;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('Page.Subpage.header', function($view){
            $TheLoai = TheLoai::all();
            foreach ($TheLoai as $theloai){

                $theloai["soluong"] = DB::table('chitietloai')
                                    ->where('id_Loai', $theloai->id)->count();
            }

            if(Session('cart')){
                $oldCart = Session('cart');
                $cart = new Cart($oldCart);
                $view->with([ 
                'TheLoai'=>$TheLoai,  
                'cart'=>Session::get('cart'),
                'product_cart'=>$cart->items,
                'totalPrice'=>$cart->totalPrice,
                'totalQty'=>$cart->totalQty
                ]);
            } else 
                $view->with('TheLoai',$TheLoai);
        });
    }
}
