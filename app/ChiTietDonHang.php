<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChiTietDonHang extends Model
{
    protected $table = "chitietdonhang";
  
    public function chitietdonhang(){
        return $this->belongsToMany('Sach')->withPivot('SoLuong', 'Gia');
    }
}
