<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ChiTietKhuyenMai extends Model
{
    protected $table = "chitietkhuyenmai";

    public function Sach(){
        return $this->belongsToMany('Sach')->withPivot('SoLuongKM','SoLuongConLai','DieuKien','PhanTramKM');
    }

    public function KhuyenMai(){
        return $this->belongsToMany('KhuyenMai')->withPivot('SoLuongKM','SoLuongConLai','DieuKien','PhanTramKM');
    }
    
}
