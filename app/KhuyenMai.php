<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KhuyenMai extends Model
{
    protected $table = "khuyenmai";
    
    public function Sach(){
        return $this->belongsToMany('App\Sach','chitietkhuyenmai','id_KM','id_Sach')->withPivot('SoLuongKM','SoLuongConLai','DieuKien','PhanTramKM');
    }

    public function Tin(){
        return $this->hasMany('App\Tin','id_KM','id');
    }

}
