<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravelista\Comments\Commentable;

class Sach extends Model
{
	protected $table = "sach";
    use Commentable;
	
    public function NhaXuatBan(){
    	return $this->belongsTo('App\NhaXuatBan','id_NXB','id');
    }

    public function Like(){
        return $this->hasMany('App\Like','id_Sach','id');
    }
    public function BinhLuan(){
    	return $this->hasMany('App\BinhLuan','id_BL','id');
    }

    public function DonHang(){
        return $this->belongsToMany('App\DonHang','chitietdonhang','id_DH','id_Sach');
    }

    public function TheLoai(){
    	return $this->belongsToMany('App\TheLoai','chitietloai','id_Sach','id_Loai');
    }

    public function TacGia(){
        return $this->belongsToMany('App\TacGia','chitiettacgia','id_Sach','id_TG');
    }

    public function KhuyenMai(){
        return $this->belongsToMany('App\KhuyenMai','chitietkhuyenmai','id_KM','id_Sach')->withPivot('SoLuongKM','SoLuongConLai','DieuKien','PhanTramKM');
    }

    // public function Like()
    // {
    //     return $this->belongsToMany('App\User','Like','id_User','id_Sach');
    // }

    
}
