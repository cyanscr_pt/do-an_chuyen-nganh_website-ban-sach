<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tin extends Model
{
    protected $table = "tin";
    public function KhuyenMai(){
        return $this->belongsTo('App\KhuyenMai','id_KM','id');
    }
    public function NhanVien(){
        return $this->belongsTo('App\NhanVien','id_NV','id');
    }
}